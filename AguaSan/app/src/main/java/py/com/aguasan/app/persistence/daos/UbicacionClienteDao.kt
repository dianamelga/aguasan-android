package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import py.com.aguasan.app.persistence.entities.UbicacionCliente

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface UbicacionClienteDao {

    @Query("SELECT a.*" +
            " FROM ubicaciones_clientes a JOIN ubicaciones_tipos b ON a.cod_tipo_ubicacion = b.cod_tipo_ubicacion " +
            " JOIN clientes c ON a.cod_cliente = c.cod_cliente " +
            " WHERE c.cod_cliente = :codCliente")
    fun getUbicacionesFromCliente(codCliente: Int): Observable<List<UbicacionCliente>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(ubicaciones: List<UbicacionCliente>): Single<List<Long>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewUbicacionCliente(ubicacion: UbicacionCliente): Completable

    @Update
    fun updateUbicacionCliente(ubicacion: UbicacionCliente): Completable

    @Delete
    fun deleteUbicacionCliente(ubicacion: UbicacionCliente): Completable

    @Query("DELETE FROM ubicaciones_clientes WHERE cod_ubicacion_local = :codUbicacionLocal")
    fun deleteUbicacionCliente(codUbicacionLocal: Int): Completable

    @Query("DELETE FROM ubicaciones_clientes")
    fun deleteAll(): Completable
}