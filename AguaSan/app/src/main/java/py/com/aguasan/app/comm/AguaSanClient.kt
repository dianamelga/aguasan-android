package py.com.aguasan.app.comm

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import py.com.aguasan.app.BuildConfig
import py.com.aguasan.app.comm.interceptors.LoggingInterceptor
import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class AguaSanClient : OkHttpClient() {
    companion object {
        fun provideClient(pair: Pair<String?, String?>?): OkHttpClient {
            val builder = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    var request = chain.request()
                    val builder = request.newBuilder()
                    pair?.second?.let {
                        builder.addHeader("Authorization", "Bearer $it")
                    }
                    builder.addHeader("Accept", "application/json")
                    builder.addHeader("Content-Type", "application/json; charset=UTF-8")
                    request = builder.build()
                    chain.proceed(request)
                }
                .readTimeout(380, TimeUnit.SECONDS)
                .connectTimeout(380, TimeUnit.SECONDS)

            //TODO: Trust certificates temporarily. Delete on production
            try {
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>,
                                                    authType: String) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>,
                                                    authType: String) {
                    }

                    override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                        return emptyArray()
                    }
                })

                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, java.security.SecureRandom())
                val sslSocketFactory = sslContext.socketFactory

                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                builder.hostnameVerifier(HostnameVerifier { hostname, session -> true })

            } catch (e: Exception) {
                if (BuildConfig.DEBUG) { e.printStackTrace() }
            }


            if (BuildConfig.DEBUG) {
                val logging = LoggingInterceptor()
                builder.addInterceptor(logging)
            }

            return builder.build()
        }
    }
}