package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.Visita
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface AguaSanVisitaApi {

    @POST("visita")
    fun createVisita(
        @Body visita: Visita
    ): Observable<Visita>

    fun getVisitas(
        @Path("codCliente") codCliente: Int
    ): Observable<List<Visita>>

    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanVisitaApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanVisitaApi::class.java)
        }
    }
}