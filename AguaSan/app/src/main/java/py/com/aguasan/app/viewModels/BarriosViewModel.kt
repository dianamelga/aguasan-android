package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.BarriosRepository
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.models.GenericApiResponse
import java.util.concurrent.TimeUnit

class BarriosViewModel(var application: AguaSan, context: Context?) : AguaSanViewModel(context) {

    fun listBarrios(): Observable<List<Barrio>> {
        return BarriosRepository(application).listBarrios()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(3)
    }

    fun createBarrio(barrio: Barrio): Observable<Barrio>{
        return BarriosRepository(application).createBarrio(barrio)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun updateBarrio(barrio: Barrio): Observable<Barrio> {
        return BarriosRepository(application).updateBarrio(barrio)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteBarrio(codBarrio: String, codCiudad: String): Observable<GenericApiResponse> {
        return BarriosRepository(application).deleteBarrio(codBarrio, codCiudad)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun getBarrio(codBarrio: String, codCiudad: String): Observable<Barrio> {
        return BarriosRepository(application).getBarrio(codBarrio, codCiudad)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(2)
    }

    fun insertBarrioToDatabase(barrio: Barrio): Completable {
        return BarriosRepository(application).insertBarrioToDatabase(barrio)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertAllBarriosToDatabase(barrios: List<Barrio>): Completable {
        return BarriosRepository(application).insertAllBarriosToDatabase(barrios)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteBarrioFromDatabase(codBarrio: String, codCiudad: String): Completable  {
        return BarriosRepository(application).deleteBarrioFromDatabase(codBarrio, codCiudad)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }

    fun deleteAllFromDatabase(): Completable {
        return BarriosRepository(application).deleteAllFromDatabase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun getBarriosFromDatabase(ciudad: Ciudad): Observable<List<Barrio>> {
        return BarriosRepository(application).getBarriosFromDatabase(ciudad)
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(2)
    }

    fun getAllBarriosFromDatabase(): Observable<List<Barrio>> {
        return BarriosRepository(application).getAllBarriosFromDatabase()
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(2)
    }
}