package py.com.aguasan.app.test

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import androidx.core.app.ActivityCompat
import android.view.View
import android.widget.Toast
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.activity_test_location.*
import py.com.aguasan.app.R
import py.com.aguasan.app.views.activities.MapsActivity

class TestLocationActivity : AppCompatActivity() {

    private val REQUEST_CODE: Int = 1000
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallback: LocationCallback


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            REQUEST_CODE -> {
                if(grantResults.size > 0) {
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this@TestLocationActivity,
                            "Permission granted", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this@TestLocationActivity,
                            "Permission denied", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_location)


        //Check permission
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),  REQUEST_CODE)
        }else {
            buildLocationRequest()
            buildLocationCallback()

            //Create FusedProviderClient
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

            //set event

            btn_start_updates.setOnClickListener (View.OnClickListener {

                if(ActivityCompat.checkSelfPermission(this@TestLocationActivity,  Manifest.permission.ACCESS_FINE_LOCATION)  != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this@TestLocationActivity,  Manifest.permission.ACCESS_COARSE_LOCATION)  != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(this@TestLocationActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE)
                    return@OnClickListener
                }
                fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                    locationCallback, Looper.myLooper())

                //change state of button
                btn_start_updates.isEnabled = !btn_start_updates.isEnabled
                btn_stop_updates.isEnabled = !btn_stop_updates.isEnabled
            })

            btn_stop_updates.setOnClickListener(View.OnClickListener {
                if(ActivityCompat.checkSelfPermission(this@TestLocationActivity,  Manifest.permission.ACCESS_FINE_LOCATION)  != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this@TestLocationActivity,  Manifest.permission.ACCESS_COARSE_LOCATION)  != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(this@TestLocationActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE)
                    return@OnClickListener
                }
                fusedLocationProviderClient.removeLocationUpdates(locationCallback)

                //change state of button
                btn_start_updates.isEnabled = !btn_start_updates.isEnabled
                btn_stop_updates.isEnabled = !btn_stop_updates.isEnabled
            })
        }
    }

    private fun buildLocationCallback() {
        locationCallback = object: LocationCallback() {

            override fun onLocationResult(p0: LocationResult?) {
                var location = p0?.locations?.get(p0.locations.size-1) //get last location
                txt_location.text = location?.latitude?.toString()+"/"+location?.longitude?.toString()

                btn_stop_updates.callOnClick()


                intent = Intent(this@TestLocationActivity, MapsActivity::class.java)
                intent.putExtra(MapsActivity.LATITUDE, location?.latitude)
                intent.putExtra(MapsActivity.LONGITUDE, location?.longitude)
                startActivity(intent)
            }
        }
    }

    private fun buildLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 10f

    }
}
