package py.com.aguasan.app.utils

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.text.TextUtils
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import py.com.aguasan.app.R
import java.text.ParseException
import java.text.SimpleDateFormat


class NotificationUtils(context: Context) {

    private val mContext: Context = context
    private lateinit var notificationManager: NotificationManager
    private val CHANNEL_ID = "AguaSan"

    fun showNotificationMessage(title: String, message: String, intent: Intent) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return


        // notification icon
        val icon = R.drawable.ic_push_notification

        val resultPendingIntent = PendingIntent.getActivity(
            mContext,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        notificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //Setting up Notification channels for android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            setupNotificationChannels()
        }

        val notificationBuilder = NotificationCompat.Builder(mContext, CHANNEL_ID)
        showSmallNotification(notificationBuilder, icon, title, message, resultPendingIntent)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupNotificationChannels(){
        val channelName = "AguaSan"
        val channelDescription = "AguaSan Push Notification"

        val channel: NotificationChannel
        channel = NotificationChannel(CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH)
        channel.description = channelDescription
        channel.enableLights(true)
        channel.lightColor = Color.BLUE
        channel.enableVibration(true)
        notificationManager.createNotificationChannel(channel)
    }


    private fun showSmallNotification(mBuilder: NotificationCompat.Builder, icon: Int, title: String, message: String, resultPendingIntent: PendingIntent) {

        val inboxStyle = NotificationCompat.InboxStyle()

        inboxStyle.addLine(message)

        val notification: Notification = mBuilder.build()
        mBuilder.setTicker(title)
        mBuilder.setAutoCancel(true)
        mBuilder.setContentTitle(title)
        mBuilder.setContentIntent(resultPendingIntent)
        mBuilder.setStyle(inboxStyle)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            mBuilder.color = ContextCompat.getColor(mContext, R.color.colorPrimary)
            mBuilder.setSmallIcon(R.drawable.ic_push_notification)
        }else{
            mBuilder.setSmallIcon(R.drawable.ic_push_notification)
        }
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(mContext.resources, icon))
        mBuilder.setContentText(message)

        val notificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(NOTIFICATION_ID, notification)
    }


    // Clears notification tray messages
    fun clearNotifications(context: Context) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    fun getTimeMilliSec(timeStamp: String): Long {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try {
            val date = format.parse(timeStamp)
            return date.getTime()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return 0
    }

    companion object{
        // broadcast receiver intent filters
        val REGISTRATION_COMPLETE = "registrationComplete"
        val PUSH_NOTIFICATION = "pushNotification"
        val TITLE = "title"
        val MESSAGE = "body"
        val USER_TYPE = "usertype"
        val USER_VALUE = "user_value"
        val TOKEN_2FA = "s2fa_token"

        // id to handle the notification in the notification tray
        val NOTIFICATION_ID = 100
        val NOTIFICATION_ID_BIG_IMAGE = 101

        val SHARED_PREF = "ah_firebase"
        /**
         * Method checks if the app is in background or not
         */
        fun isAppIsInBackground(context: Context): Boolean {
            var isInBackground = true
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                val runningProcesses = am.runningAppProcesses
                for (processInfo in runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (activeProcess in processInfo.pkgList) {
                            if (activeProcess == context.getPackageName()) {
                                isInBackground = false
                            }
                        }
                    }
                }
            } else {
                val taskInfo = am.getRunningTasks(1)
                val componentInfo = taskInfo[0].topActivity
                if (componentInfo?.packageName == context.getPackageName()) {
                    isInBackground = false
                }
            }

            return isInBackground
        }

    }
}