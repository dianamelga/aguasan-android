package py.com.aguasan.app.models

import java.io.Serializable

data class LoginResponse (
    val token: String?=null,
    val isAdmin: Boolean?=null,
    val usuario: Usuario?=null,
    val mail: String?= null
): Serializable