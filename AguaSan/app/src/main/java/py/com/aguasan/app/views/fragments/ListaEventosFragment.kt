package py.com.aguasan.app.views.fragments


import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_lista_eventos.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.ClienteAdapter
import py.com.aguasan.app.adapters.EventoAdapter
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.Evento
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.viewModels.EventosViewModel

class ListaEventosFragment : AguaSanFragment() {
    private var callback: (Evento) -> Unit = {}
    private var adapter: EventoAdapter? = null
    private var eventos: ArrayList<Evento> = ArrayList()
    private var eventosCached: ArrayList<Evento> = ArrayList()
    private lateinit var eventosViewModel: EventosViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        eventosViewModel = EventosViewModel(aguaSan!!, aguaSanActivity!!)
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_lista_eventos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = EventoAdapter(eventos, callback = { callback(it) }, longCallback = {
            if (it.estado == "PEND") {
                val builder = AlertDialog.Builder(aguaSanActivity!!, R.style.AppTheme_Dialog)
                builder.setTitle("Opciones")

                // add a list
                val options = arrayOf("Evento Realizado", "Evento Cancelado")
                builder.setItems(options) { dialog, which ->
                    when (which) {
                        0 -> {
                            it.estado = "FIN"
                            eventosViewModel.updateEvento(it)
                                .subscribe(object : AguaSanObserver<Evento>(aguaSanActivity!!) {
                                    override fun onNext(t: Evento) {
                                        UtilTools.showDialog(
                                            aguaSanActivity!!,
                                            "Evento Concretado exitosamente."
                                        )
                                        it.fecUltAct = t.fecUltAct
                                        it.usrUltAct = t.usrUltAct
                                        adapter?.notifyDataSetChanged()
                                    }

                                })
                        }
                        1 -> {
                            it.estado = "CANC"
                            eventosViewModel.updateEvento(it)
                                .subscribe(object : AguaSanObserver<Evento>(aguaSanActivity!!) {
                                    override fun onNext(t: Evento) {
                                        UtilTools.showDialog(aguaSanActivity!!, "Evento Cancelado.")
                                        it.fecUltAct = t.fecUltAct
                                        it.usrUltAct = t.usrUltAct
                                        adapter?.notifyDataSetChanged()
                                    }

                                })
                        }

                    }
                }

                builder.setNegativeButton("Salir", null)

                // create and show the alert dialog
                val dialog = builder.create()
                dialog.show()
            }

        })
        rvEventos.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            androidx.recyclerview.widget.RecyclerView.VERTICAL,
            false
        )
        rvEventos.adapter = adapter


        getEventos()

        slRefresh.setOnRefreshListener {
            getEventos()
        }

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_search, menu)
        (menu.findItem(R.id.action_search).actionView as SearchView).apply {
            this.queryHint = "Buscar"
            setIconifiedByDefault(false)
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    p0?.let { s ->
                        val temp = ArrayList<Evento>()
                        if (s.isNotBlank() || s.isNotEmpty()) {
                            for (item in eventos) {

                                if (item.nroEvento?.toString()?.contains(s, true) == true ||
                                    item.estado?.contains(s, true) == true ||
                                    item.cliente?.nombres?.contains(s, true) == true ||
                                    item.cliente?.apellidos?.contains(s, true) == true ||
                                    item.cliente?.nroDocumento?.contains(s, true) == true
                                ) {
                                    temp.add(item)
                                }

                            }

                        } else {
                            temp.addAll(eventosCached)
                        }
                        eventos.clear()
                        eventos.addAll(temp)
                        adapter?.notifyDataSetChanged()
                    }
                    return false
                }
            })
        }
    }

    private fun getEventos() {
        val clienteViewModel = ClientesViewModel(aguaSan!!, aguaSanActivity!!)

        eventosViewModel.getListEventos().flatMap {eventos ->
            eventos.map { e ->
                clienteViewModel.getClientesFromDatabase(e.codCliente).map { clientes -> if(clientes.isNotEmpty()) e.cliente = clientes.first() }
            }
            Observable.just(eventos)
        }
            .subscribe(object : AguaSanObserver<List<Evento>>(aguaSanActivity!!) {
                override fun onNext(ev: List<Evento>) {
                    val t = ev.filter{ e-> e.estado?.toUpperCase() == "PEND"}
                    eventos.clear()
                    eventosCached.clear()

                    eventos.addAll(t)
                    eventosCached.addAll(t)
                    adapter?.notifyDataSetChanged()
                    slRefresh.isRefreshing = false
                }


                override fun onError(e: Throwable) {
                    super.onError(e)
                    slRefresh.isRefreshing = false
                }

            })
    }


    companion object {
        fun newInstance(b: Bundle? = null, cb: (Evento) -> Unit = {}): ListaEventosFragment {
            val fragment = ListaEventosFragment()
            b?.let {
                fragment.arguments = b
            }

            fragment.callback = cb

            return fragment
        }
    }
}
