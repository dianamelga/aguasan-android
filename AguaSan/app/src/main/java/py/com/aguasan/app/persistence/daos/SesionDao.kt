package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.persistence.entities.Sesion

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Dao
interface SesionDao {

    @Query("SELECT * FROM sesion")
    fun getAll(): Observable<List<Sesion>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSesion(sesion: Sesion): Completable

    @Update
    fun updateSesion(sesion: Sesion): Completable

    @Query("UPDATE sesion SET en_sesion = 0")
    fun cerrarSesion(): Completable

}
