package py.com.aguasan.app.database


object DBContract {

    val CREATE_TABLE_SESION = "CREATE TABLE sesion (" +
            "username TEXT not null, " +
            "serial_instal INTEGER not null, " +
            "token TEXT not null, " +
            "en_sesion integer not null, " +
            "es_admin integer not null, " +
            "mail TEXT not null default '', "+
            "fec_ult_logueo text not null)"

    val CREATE_TABLE_CIUDAD = "CREATE TABLE ciudad ( " +
            "cod_ciudad text not null, " +
            "ciudad text not null)"

    val CREATE_TABLE_BARRIO = "CREATE TABLE barrio ( " +
            "cod_barrio text not null, " +
            "barrio text not null, " +
            "cod_ciudad text not null)"

    val CREATE_TABLE_TIPO_DOCUMENTO = "CREATE TABLE tipo_documento ( "+
            "cod_tipo_documento text not null, "+
            "tipo_documento text not null)"

    val CREATE_TABLE_USUARIOS = "CREATE TABLE usuarios ("+
            "username text not null, " +
            "password text not null, "+
            "nombre text not null, " +
            "cod_tipo_documento text not null, " +
            "nro_documento text not null, " +
            "email text not null, " +
            "es_admin text not null, " +
            "activo text not null, " +
            "fec_alta text not null, " +
            "fec_ult_act text not null)"

    val CREATE_TABLE_CLIENTES = "CREATE TABLE clientes (" +
            "cod_cliente_local integer PRIMARY KEY AUTOINCREMENT not null, "+
            "cod_cliente integer not null, " +
            "nombres text not null, " +
            "apellidos text not null, " +
            "celular text not null, " +
            "telefono text, "+
            "cod_tipo_documento text not null, " +
            "nro_documento text not null, " +
            "direccion text not null, "+
            "observacion text, "+
            "cod_ciudad text not null, " +
            "cod_barrio text not null, " +
            "dias_visita text not null, " +
            "activo text not null, "+
            "fec_alta text not null, " +
            "usr_alta text not null, " +
            "fec_ult_act text not null, " +
            "usr_ult_act text not null)"

    val CREATE_TABLE_UBICACIONES_TIPOS = "CREATE TABLE ubicaciones_tipos ( " +
            "cod_tipo_ubicacion text not null, " +
            "tipo_ubicacion text not null)"

    val CREATE_TABLE_UBICACIONES_CLIENTES = "CREATE TABLE ubicaciones_clientes ( " +
            "cod_ubicacion_local integer PRIMARY KEY AUTOINCREMENT NOT NULL, "+
            "cod_ubicacion integer not null, " +
            "cod_tipo_ubicacion integer not null, " +
            "cod_cliente integer not null, " +
            "latitud text not null, " +
            "longitud text not null, " +
            "path_foto text not null, " +
            "referencia text not null, "+
            "fec_alta text not null, " +
            "usr_alta text not null, " +
            "fec_ult_act text not null, " +
            "usr_ult_act text not null)"

    val CREATE_TABLE_FORMULARIO = "CREATE TABLE formulario (" +
            "nro_formulario_local integer PRIMARY KEY autoincrement NOT NULL, "+
            "nro_formulario integer not null, " +
            "cod_cliente integer not null, "+
            "bidones integer not null, " +
            "bebederos integer not null, " +
            "dispensers integer not null, " +
            "fec_alta text not null, " +
            "usr_alta text not null, " +
            "fec_ult_act text not null, " +
            "usr_ult_act text not null)"

   val CREATE_TABLE_SITUACION_VISITA = "CREATE TABLE situacion_visita (" +
           "cod_situacion integer not null," +
           "situacion text not null)"

    val CREATE_TABLE_VISITA = "CREATE TABLE visita (" +
            "nro_visita_local integer PRIMARY KEY autoincrement not null, "+
            "nro_visita integer not null," +
            "cod_cliente integer not null, "+
            "cod_situacion integer not null," +
            "bidones_canje integer not null, " +
            "bidones integer not null, " +
            "fec_alta text not null, " +
            "usr_alta text not null, " +
            "fec_ult_act text not null, " +
            "usr_ult_act text not null) "

    val CREATE_TABLE_EVENTO = "CREATE TABLE evento (" +
            "nro_evento_local integer PRIMARY KEY autoincrement not null, " +
            "nro_evento integer not null, " +
            "cod_cliente integer not null, " +
            "fec_evento text not null, " +
            "observacion text not null, " +
            "estado text not null, " +
            "fec_alta text not null, " +
            "usr_alta text not null, " +
            "fec_ult_act text not null, " +
            "usr_ult_act text not null)"
}