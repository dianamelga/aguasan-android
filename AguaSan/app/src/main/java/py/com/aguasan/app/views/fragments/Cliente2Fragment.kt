package py.com.aguasan.app.views.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.fragment_cliente2.*

import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.Formulario
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.utils.UtilTools.Companion.showDialog
import py.com.aguasan.app.utils.asFormattedDate
import py.com.aguasan.app.utils.conditionWithFocus
import py.com.aguasan.app.views.activities.ClienteActivity


class Cliente2Fragment : AguaSanFragment(), Step {
    private var diasVisita: ArrayList<String> = ArrayList()

    init {
        resourceId = R.layout.fragment_cliente2
    }

    override fun onSelected() {

    }

    override fun verifyStep(): VerificationError? {
        return if (isFormValid()) {
            val cliente = (activity as ClienteActivity).cliente
            cliente.diasVisita = diasVisita

            val formulario = Formulario(
                nroFormulario = etNroFormulario.text.toString().toLong(),
                bidones = etBidones.text.toString().toInt(),
                dispensers = etDispensers.text.toString().toInt(),
                bebederos = etBebederos.text.toString().toInt()
            )
            cliente.formulario = formulario
            cliente.activo = "S"
            cliente.userAlta = aguaSanActivity!!.aguasan!!.session!!.username
            cliente.fechaAlta = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss")
            cliente.userUltimaAct = aguaSanActivity!!.aguasan!!.session!!.username
            cliente.fechaUltimaAct = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss")

            null
        } else {
            VerificationError("Error!")
        }
    }

    override fun onError(error: VerificationError) {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_cliente2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cliente?.let {
            checkLunes.isChecked = it.diasVisita?.contains("Lunes") ?: false
            checkMartes.isChecked = it.diasVisita?.contains("Martes") ?: false
            checkMiercoles.isChecked = it.diasVisita?.contains("Miércoles") ?: false
            checkJueves.isChecked = it.diasVisita?.contains("Jueves") ?: false
            checkViernes.isChecked = it.diasVisita?.contains("Viernes") ?: false
            checkSabado.isChecked = it.diasVisita?.contains("Sábado") ?: false
            checkDomingo.isChecked = it.diasVisita?.contains("Domingo") ?: false

            etNroFormulario.setText(it.formulario?.nroFormulario.toString())
            etBidones.setText(it.formulario?.bidones.toString())
            etDispensers.setText(it.formulario?.dispensers.toString())
            etBebederos.setText(it.formulario?.bebederos.toString())

        }


    }

    override fun isFormValid(): Boolean {
        return  tilNroFormulario.conditionWithFocus(etNroFormulario?.text?.isNotEmpty() ?: false, "Ingrese número de formulario") &&
                isValidVisitDays() &&
                tilBidones.conditionWithFocus(etBidones?.text?.isNotEmpty() ?: false, "Ingrese cantidad") &&
                tilDispensers.conditionWithFocus(etDispensers?.text?.isNotEmpty() ?: false, "Ingrese cantidad") &&
                tilBebederos.conditionWithFocus(etBebederos?.text?.isNotEmpty() ?: false, "Ingrese cantidad")
    }

    private fun isValidVisitDays(): Boolean {
        diasVisita.clear()

        if (checkLunes.isChecked) diasVisita.add("Lunes")
        if (checkMartes.isChecked) diasVisita.add("Martes")
        if (checkMiercoles.isChecked) diasVisita.add("Miércoles")
        if (checkJueves.isChecked) diasVisita.add("Jueves")
        if (checkViernes.isChecked) diasVisita.add("Viernes")
        if (checkSabado.isChecked) diasVisita.add("Sábado")
        if (checkDomingo.isChecked) diasVisita.add("Domingo")

        return if (diasVisita.size > 0) {
            true
        } else {
            showDialog(aguaSanActivity!!, "Debe elegir un día de visita")
            false
        }

    }

    companion object {
        var cliente: Cliente? = null
        fun newInstance(b: Bundle?): Cliente2Fragment {
            val fragment = Cliente2Fragment()
            fragment.arguments = b
            cliente = null
            b?.let {
                it.getSerializable("cliente")?.let{ cli ->
                    this.cliente = cli as Cliente?
                }
            }

            return fragment
        }
    }


}
