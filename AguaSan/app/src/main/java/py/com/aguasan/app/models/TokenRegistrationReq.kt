package py.com.aguasan.app.models

import java.io.Serializable

data class TokenRegistrationReq(
    var userName: String?=null,
    var deviceId: String?=null,
    var tokenRegistrationId: String?=null
): Serializable

data class TokenRegistrationResponse(

    val tokenRegistrationId: String?=null
): Serializable