package py.com.aguasan.app.models

data class Auditoria (
    val fecUltAct: String?=null,
    val usrUltAct: String?=null,
    val fecAlta: String?=null,
    val usrAlta: String?=null
)