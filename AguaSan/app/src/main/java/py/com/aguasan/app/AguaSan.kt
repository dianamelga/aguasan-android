package py.com.aguasan.app

import android.annotation.SuppressLint
import android.content.Context
import androidx.multidex.MultiDexApplication
import android.text.TextWatcher
import androidx.room.Room
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.Gson
import io.reactivex.subjects.BehaviorSubject
import py.com.aguasan.app.comm.*
import py.com.aguasan.app.models.Session
import py.com.aguasan.app.persistence.db.AppDatabase
import py.com.aguasan.app.utils.toJSON

class AguaSan : MultiDexApplication() {
    var authParams : Pair<String, String> = Pair("", "") //must be updated later
    var database: AppDatabase? = null
    var apiClientes: AguaSanClientesApi = AguaSanClientesApi.createForApi(url, authParams)
    var apiAuth: AguaSanAuthApi = AguaSanAuthApi.createForApi(url, authParams)
    var apiHome: AguaSanHomeApi = AguaSanHomeApi.createForApi(url, authParams)
    var apiUsuarios: AguaSanUsuariosApi = AguaSanUsuariosApi.createForApi(url, authParams)
    var apiVisita: AguaSanVisitaApi= AguaSanVisitaApi.createForApi(url, authParams)
    var apiBarrios: AguaSanBarriosApi= AguaSanBarriosApi.createForApi(url, authParams)
    var apiCiudades: AguaSanCiudadesApi= AguaSanCiudadesApi.createForApi(url, authParams)
    var apiFirebase: AguaSanFirebaseAdminApi= AguaSanFirebaseAdminApi.createForApi(url, authParams)
    var apiEventos: AguaSanEventosApi= AguaSanEventosApi.createForApi(url, authParams)

    var session: Session?=null
        get() {
        return field ?: getUserSession()!!
        }
    var sessionObserver: BehaviorSubject<Session> = BehaviorSubject.create()

    @SuppressLint("CheckResult")
    override fun onCreate() {
        super.onCreate()

        apiAuth = AguaSanAuthApi.createForApi(url, authParams)

        database = Room.databaseBuilder(this, AppDatabase::class.java, "aguasan-db").build()


        createApis(getToken())

        sessionObserver.subscribe {
            session = it
            saveUserSession(it)

            val token = if (it.token != null) {
                saveToken(it.token)
                it.token
            } else {
                getToken() ?: ""
            }

            createApis(token)

        }


    }

    private fun createApis(token: String) {
        authParams = Pair("", token)
        apiClientes = AguaSanClientesApi.createForApi(url, authParams)
        apiHome = AguaSanHomeApi.createForApi(url, authParams)
        apiUsuarios = AguaSanUsuariosApi.createForApi(url, authParams)
        apiVisita = AguaSanVisitaApi.createForApi(url, authParams)
        apiBarrios = AguaSanBarriosApi.createForApi(url, authParams)
        apiCiudades = AguaSanCiudadesApi.createForApi(url, authParams)
        apiFirebase = AguaSanFirebaseAdminApi.createForApi(url, authParams)
        apiEventos = AguaSanEventosApi.createForApi(url, authParams)
    }

    private fun saveUserSession(session: Session) {
        val editor = getSharedPreferences(AUTH, MODE_PRIVATE).edit()
        editor.putString("session", session.toJSON())
        editor.apply()
    }

    private fun getUserSession(): Session? {
        val sessionStr = getSharedPreferences(AUTH, MODE_PRIVATE).getString("session", null)
        return if(sessionStr != null) {
            try {
                Gson().fromJson(sessionStr, Session::class.java)
            }catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                null
            }
        }else {
            null
        }
    }

    private fun saveToken(token: String) {
        val editor = getSharedPreferences(AUTH, MODE_PRIVATE).edit()
        editor.putString("token", token)
        editor.apply()
    }

    private fun getToken(): String {
        return getSharedPreferences(AUTH, MODE_PRIVATE).getString("token", null) ?: ""
    }

    companion object {

        private const val AUTH = "AUTH"
        var textWatcher: TextWatcher? = null
        //DIANA
        //var url = "http://192.168.0.9:8090/aguasan/"
        //LUIS
        //var url = "http://192.168.0.28:8090/aguasan/"
        //DESA
        //var url = "http://18.231.22.188:8090/aguasan/"
        //PRODU
        var url = "http://192.168.100.5:8090/aguasan/"
        var conexionLocal = false
        const val PHONEPATTERN = "\\(?(09)\\)?([0-9]{8})"

    }
}