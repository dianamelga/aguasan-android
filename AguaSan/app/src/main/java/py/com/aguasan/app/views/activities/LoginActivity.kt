package py.com.aguasan.app.views.activities


import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.firebase.crashlytics.FirebaseCrashlytics
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_login.*
import py.com.aguasan.app.BuildConfig
import py.com.aguasan.app.R
import py.com.aguasan.app.models.*
import py.com.aguasan.app.persistence.entities.Sesion
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.NotificationUtils
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.viewModels.AuthViewModel

class LoginActivity : AguaSanActivity() {

    var viewModel: AuthViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel = AuthViewModel(aguasan!!, this@LoginActivity)

        btConfiguracion.setOnClickListener {
            startActivity(Intent(this@LoginActivity, TestActivity::class.java))
        }

        btConfiguracion.visibility = View.GONE

        btLogin.setOnClickListener {

            viewModel?.getSesionFromDatabase()?.subscribe(object: AguaSanObserver<List<Sesion>>(this){
                override fun onNext(t: List<Sesion>) {
                    if(t.isEmpty()) {
                        //si es primer logueo
                        val pref = applicationContext.getSharedPreferences(NotificationUtils.SHARED_PREF, 0)
                        val loginRequest = LoginReq(tvUsername.text.toString(), etPassword.text.toString(),
                            deviceId = UtilTools.getDeviceId(this@LoginActivity), tokenRegistrationId = pref.getString("regId", ""))
                        viewModel?.serial(loginRequest)
                            ?.subscribe(object : AguaSanObserver<SerialResponse>(this@LoginActivity) {
                                override fun onNext(t: SerialResponse) {
                                    viewModel?.newSesion(Sesion(
                                        userName = tvUsername.text.toString(),
                                        serialInstal = t.serial!!.toInt(),
                                        token = t.token!!,
                                        enSesion = 1,
                                        esAdmin = if(t.isAdmin == true) 1 else 0,
                                        fecUltLogueo = UtilTools.ISO8601now(),
                                        mail = t.mail!!
                                    ))?.subscribeBy(onError = {
                                        FirebaseCrashlytics.getInstance().recordException(it)
                                        UtilTools.showDialog(this@LoginActivity, "Hubo un problema para iniciar sesion")
                                    },
                                    onComplete = {
                                        aguasan!!.sessionObserver.onNext(Session(tvUsername.text.toString(), t.token!!, true, t.isAdmin ?: false, t.mail?: ""))
                                        goToMainActivity()
                                    })

                                }

                            })
                    }else {
                        val sesion = t.first()
                        val serialInstal = t.first().serialInstal
                        val loginRequest = LoginReq(tvUsername.text.toString(), etPassword.text.toString(), serialInstal)
                        viewModel?.login(loginRequest)
                            ?.subscribe(object : AguaSanObserver<LoginResponse>(this@LoginActivity) {
                                override fun onNext(t: LoginResponse) {
                                    sesion.apply {
                                        token = t.token!!
                                        enSesion = 1
                                        mail = t.mail!!
                                        esAdmin = if(t.isAdmin == true) 1 else 0
                                        fecUltLogueo = UtilTools.ISO8601now()
                                    }

                                    viewModel?.updateSesion(sesion)?.subscribeBy(onError = {
                                        FirebaseCrashlytics.getInstance().recordException(it)
                                        UtilTools.showDialog(this@LoginActivity, "Hubo un problema para iniciar sesion")
                                    },
                                        onComplete = {
                                            aguasan!!.sessionObserver.onNext(Session(tvUsername.text.toString(), t.token!!, true, t.isAdmin ?: false, t.mail?: ""))
                                            goToMainActivity()
                                        })
                                }

                            })
                    }
                }

            })

        }


    }

    private fun goToMainActivity() {

        val i = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(i)
        finish()
    }




}
