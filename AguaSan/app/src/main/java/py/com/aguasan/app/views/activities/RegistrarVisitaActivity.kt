package py.com.aguasan.app.views.activities

import android.os.Bundle
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.views.fragments.RegistroVisitaFragment

class RegistrarVisitaActivity : AguaSanActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrar_visita)

        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "Nueva Visita"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val bundle = Bundle()
        bundle.putSerializable("cliente", intent?.getSerializableExtra("cliente") as Cliente)
        val fragment = RegistroVisitaFragment.newInstance(bundle)

        supportFragmentManager.beginTransaction()
            .add(R.id.fragment, fragment)
            .commit()


    }
}
