package py.com.aguasan.app.models

import java.io.Serializable

data class Visita(
    val nroVisita: Long?=null,
    var cliente: Cliente?=null,
    val situacion: SituacionVisita?=null,
    val bidonesCanje: Int?=null,
    val bidones:Int?=null,
    val fecAlta: String?=null,
    val usrAlta: String?=null,
    val fecUltAct: String?=null,
    val usrUltAct: String?=null
): Serializable {
    fun toEntity(): py.com.aguasan.app.persistence.entities.Visita {
        return py.com.aguasan.app.persistence.entities.Visita(
            nroVisita = nroVisita!!.toInt(),
            codCliente = cliente?.codCliente!!,
            codSituacion = situacion?.codSituacion!!,
            bidonesCanje = bidonesCanje ?: 0,
            bidones = bidones ?: 0,
            fecAlta = fecAlta ?: "",
            usrAlta = usrAlta ?: "",
            fecUltAct = fecUltAct ?: "",
            usrUltAct = usrUltAct ?: ""

        )
    }
}