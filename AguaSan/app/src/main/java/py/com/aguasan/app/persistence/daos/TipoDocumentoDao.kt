package py.com.aguasan.app.persistence.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.persistence.entities.TIPO_DOCUMENTO_TABLE_NAME
import py.com.aguasan.app.persistence.entities.TipoDocumento

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface TipoDocumentoDao {
    @Query("SELECT * FROM tipo_documento")
    fun getAll(): Observable<List<TipoDocumento>>

    @Query("SELECT b.* FROM clientes a JOIN tipo_documento b ON " +
            "a.cod_tipo_documento = b.cod_tipo_documento " +
            "WHERE a.cod_cliente = :codCliente")
    fun getTipoDocumentoFromCliente(codCliente: Int): Observable<TipoDocumento>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tiposDocumentos: List<TipoDocumento>): Completable

    @Query("DELETE FROM $TIPO_DOCUMENTO_TABLE_NAME")
    fun deleteAll(): Completable
}