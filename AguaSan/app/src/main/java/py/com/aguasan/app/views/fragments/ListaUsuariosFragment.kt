package py.com.aguasan.app.views.fragments


import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_lista_usuarios.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.UsuarioAdapter
import py.com.aguasan.app.models.Usuario
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.viewModels.UsuariosViewModel


class ListaUsuariosFragment : AguaSanFragment() {
    private var usuarios: ArrayList<Usuario> = ArrayList()
    private var usuariosCached: ArrayList<Usuario> = ArrayList()
    private lateinit var usuarioAdapter: UsuarioAdapter
    private lateinit var usuariosViewModel: UsuariosViewModel
    private var callback: (Usuario) -> Unit = {}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        usuariosViewModel = UsuariosViewModel(aguaSanActivity?.aguasan!!, aguaSanActivity!!)
        usuarioAdapter = UsuarioAdapter(usuarios)
        return inflater.inflate(R.layout.fragment_lista_usuarios, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvUsuarios.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            androidx.recyclerview.widget.RecyclerView.VERTICAL,
            false
        )
        rvUsuarios.adapter = usuarioAdapter

        usuarioAdapter.callback = {
            callback(it)
        }

        usuarioAdapter.longCallback = {
            val builder = AlertDialog.Builder(aguaSanActivity!!, R.style.AppTheme_Dialog)
            builder.setTitle("Opciones")

            // add a list
            val options = arrayOf("Editar")
            builder.setItems(options) { dialog, which ->
                when(which) {
                    0 -> {
                        //TODO llamada a fragmend de edicion
                    }
                }
            }

            builder.setNegativeButton("Cancelar", null)

            // create and show the alert dialog
            val dialog = builder.create()
           // dialog.show()
        }



        usuariosViewModel.getUsuariosFromDatabase().subscribe(object: AguaSanObserver<List<Usuario>>(aguaSanActivity!!){
            override fun onNext(t: List<Usuario>) {
                usuarios.clear()
                usuarios.addAll(t)
                usuariosCached.clear()
                usuariosCached.addAll(usuarios)

                if(usuarios.size <= 0) {
                    usuariosViewModel.getUsuarios().subscribe(object : AguaSanObserver<List<Usuario>>(aguaSanActivity!!) {
                        override fun onNext(t: List<Usuario>) {
                            usuarios.clear()
                            usuarios.addAll(t)
                            usuariosCached.clear()
                            usuariosCached.addAll(usuarios)
                            usuariosViewModel.insertAllToDatabase(t).subscribe()
                            usuarioAdapter.notifyDataSetChanged()
                        }

                    })
                }else{
                    usuarioAdapter.notifyDataSetChanged()
                }
            }

        })





        slRefresh.setOnRefreshListener {
            // actualizamos la lista de usuarios
            usuarios.clear()
            usuariosCached.clear()
            usuarioAdapter.notifyDataSetChanged()
            usuariosViewModel.deleteAllUsuariosFromDatabase().subscribeBy(onComplete ={
                usuariosViewModel.getUsuarios().subscribe(object: AguaSanObserver<List<Usuario>>(aguaSanActivity!!) {
                    override fun onNext(t: List<Usuario>) {
                        usuarios.clear()
                        usuarios.addAll(t)
                        usuariosCached.clear()
                        usuariosCached.addAll(usuarios)
                        usuariosViewModel.insertAllToDatabase(t).subscribe()
                        usuarioAdapter.notifyDataSetChanged()
                        slRefresh.isRefreshing = false

                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        slRefresh.isRefreshing = false
                    }

                })
            })

        }


    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_search, menu)
        (menu.findItem(R.id.action_search).actionView as SearchView).apply {
            this.queryHint= "Buscar"
            setIconifiedByDefault(false)
            setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    p0?.let { s ->
                        val temp = ArrayList<Usuario>()
                        if (s.isNotBlank() || s.isNotEmpty()) {
                            for (item in usuariosCached) {
                                if(item.userName?.contains(s, true) == true ||
                                        item.activo?.contains(s, true) == true ||
                                        item.nombre?.contains(s,true) == true) {
                                    temp.add(item)
                                }
                            }
                        } else {
                            temp.addAll(usuariosCached)
                        }
                        usuarios.clear()
                        usuarios.addAll(temp)
                        usuarioAdapter?.notifyDataSetChanged()
                    }
                    return false
                }
            })
        }
    }


    override fun onPause() {
        super.onPause()

        slRefresh?.isRefreshing = false
        slRefresh?.destroyDrawingCache()
        slRefresh?.clearAnimation()

    }

    companion object {
        fun newInstance(b: Bundle?=null, callback: (Usuario) -> Unit= {}): ListaUsuariosFragment {
            val fragment = ListaUsuariosFragment()
            b?.let{
                fragment.arguments = it
            }

            fragment.callback = callback

            return fragment
        }
    }

}
