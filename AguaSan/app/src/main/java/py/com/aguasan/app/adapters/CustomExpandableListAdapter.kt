package py.com.aguasan.app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Auditoria
import py.com.aguasan.app.models.Formulario

class CustomExpandableListAdapter internal constructor(private val context: Context, private val titleList: List<String>,
                                                       private val dataList: HashMap<String, List<Any>>
                                                       ) : BaseExpandableListAdapter() {

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return this.dataList[this.titleList[listPosition]]!![expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(listPosition: Int, expandedListPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val data = getChild(listPosition, expandedListPosition)
        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            when(data) {
                is Formulario -> {
                    val formulario = data as Formulario
                    convertView = layoutInflater.inflate(R.layout.list_item_formulario, null)
                    val nroFormulario = convertView!!.findViewById<TextView>(R.id.tvNroDocumento)
                    nroFormulario.text = formulario.nroFormulario.toString()
                    val bidones = convertView!!.findViewById<TextView>(R.id.tvBidones)
                    bidones.text = formulario.bidones.toString()
                    val dispensers = convertView!!.findViewById<TextView>(R.id.tvDispensers)
                    dispensers.text = formulario.dispensers.toString()
                    val bebederos = convertView!!.findViewById<TextView>(R.id.tvBebederos)
                    bebederos.text = formulario.bebederos.toString()

                }
                else -> {
                    val auditoria = data as Auditoria
                    convertView = layoutInflater.inflate(R.layout.list_item_auditoria, null)
                    val fecAlta = convertView!!.findViewById<TextView>(R.id.tvFechaAlta)
                    fecAlta.text = auditoria.fecAlta
                    val usrAlta = convertView!!.findViewById<TextView>(R.id.tvUserAlta)
                    usrAlta.text = auditoria.usrAlta
                    val fecUltAct = convertView!!.findViewById<TextView>(R.id.tvFecUltAct)
                    fecUltAct.text = auditoria.fecUltAct
                    val usrUltAct = convertView!!.findViewById<TextView>(R.id.tvUserUltAct)
                    usrUltAct.text = auditoria.usrUltAct
                }
            }

        }

        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return this.dataList[this.titleList[listPosition]]!!.size
    }

    override fun getGroup(listPosition: Int): Any {
        return this.titleList[listPosition]
    }

    override fun getGroupCount(): Int {
        return this.titleList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(listPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val listTitle = getGroup(listPosition) as String
        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_group, null)
        }
        val listTitleTextView = convertView!!.findViewById<TextView>(R.id.tvHeader)
        listTitleTextView.text = listTitle

        val arrow = convertView!!.findViewById<ImageView>(R.id.ivArrow)
        if(isExpanded) {
            arrow.setImageResource(android.R.drawable.arrow_up_float)
        }else{
            arrow.setImageResource(android.R.drawable.arrow_down_float)
        }
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}
