package py.com.aguasan.app.utils

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import android.util.Log
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import py.com.aguasan.app.BuildConfig
import py.com.aguasan.app.R
import py.com.aguasan.app.models.AguaSanError
import py.com.aguasan.app.views.activities.AguaSanActivity
import py.com.aguasan.app.views.activities.LoginActivity
import retrofit2.adapter.rxjava2.HttpException

abstract class AguaSanObserver<T>(var context: Context, var cb: (AguaSanError) -> Unit = {}) : Observer<T> {
    var willShowError = true

    override fun onComplete() {
        if (BuildConfig.DEBUG) Log.d("OBSERVER", "onComplete")
    }

    override fun onSubscribe(d: Disposable) {
        if (BuildConfig.DEBUG) Log.d("OBSERVER", "onSubscribe")
        (context as? AguaSanActivity)?.addForDisposing(d)
    }

    override fun onError(e: Throwable) {
        if (BuildConfig.DEBUG) Log.d("OBSERVER", "onError")
        if (willShowError) {
            if (e is HttpException) {
                if (e.response()?.code() == 403) {
                    manageSessionExpired()
                }else {
                    val error = AguaSanError.create(e.response()?.errorBody())
                    error?.let {
                        if (error.code.equals("a1100")) {
                            manageSessionExpired()
                        } else if (error.code.equals("a1011") || error.code.equals("s-a2409") || error.code.equals("a1407") || error.code.equals(
                                "a1409"
                            ) || error.code.equals("a1410")
                        ) {
                            cb(it)
                        } else {
                            showMessage(it.message)
                        }
                    } ?: showMessage(context?.getString(R.string.generic_error_msg))
                }
            } else if(e is java.net.ConnectException) {

                if(e.message?.contains("timeout") == true) {
                    showMessage(context?.getString(R.string.conexion_timeout_msg))
                }else{
                    showMessage(context?.getString(R.string.conexion_failed_msg))
                }

            }else {
                showMessage(context?.getString(R.string.generic_error_msg))
            }

        }
    }

    private fun showMessage(message: String?) {
        if (context is AguaSanActivity) {
            AlertDialog.Builder(context, R.style.AppTheme_Dialog)
                .setMessage(message ?: context.getString(R.string.generic_error_msg))
                .setNegativeButton("Aceptar") { dialog, _ ->
                    dialog.dismiss()
                }
                .create().run {
                    this.window?.setWindowAnimations(R.style.AppTheme_Dialog)
                    this
                }.show()
        }
    }

    private fun manageSessionExpired() {
        if (context is AguaSanActivity) {
            AlertDialog.Builder(context, R.style.AppTheme_Dialog)
                .setMessage("Sesión expirada.")
                .setNegativeButton("Aceptar") { dialog, _ ->
                    dialog.dismiss()
                    (context as AguaSanActivity).aguasan?.authParams = Pair("", "")
                    val intent = Intent(context, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    context.startActivity(intent)
                }
                .create().run {
                    this.window?.setWindowAnimations(R.style.AppTheme_Dialog)
                    this
                }.show()
        }
    }
}