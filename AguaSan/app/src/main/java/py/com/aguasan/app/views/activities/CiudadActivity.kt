package py.com.aguasan.app.views.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_ciudad.*
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.models.GenericApiResponse
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.viewModels.CiudadesViewModel
import py.com.aguasan.app.views.fragments.ListaDatosGeneralesFragment
import py.com.aguasan.app.views.fragments.NuevaCiudadFragment

class CiudadActivity : AguaSanActivity() {
    private var modoEdicion: Boolean = false
    private lateinit var ciudadesViewModel: CiudadesViewModel

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ciudad)
        ciudadesViewModel = CiudadesViewModel(aguasan!!, this@CiudadActivity)

        supportActionBar?.title = "Ciudades"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)

        //Recibo la lista de ante mano y le paso al fragment
        val bundle = Bundle()
        bundle.putSerializable(
            ListaDatosGeneralesFragment.LISTA,
            intent.getSerializableExtra(ListaDatosGeneralesFragment.LISTA)
        )

        val fragment = ListaDatosGeneralesFragment.newInstance(bundle, {
            //ON CLICK CALLBACK
            //MODO EDICION
            modoEdicion = true
            val bundle = Bundle()
            bundle.putSerializable(NuevaCiudadFragment.CIUDAD_SELECTED, it as Ciudad)
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, NuevaCiudadFragment.newInstance(bundle))
                .addToBackStack(null)
                .commit()
        },
            //ON REFRESH CALLBACK
            { data, cache, adapter, afterRefresh ->
                ciudadesViewModel.listCiudades().subscribe(object: AguaSanObserver<List<Ciudad>>(this) {
                    override fun onNext(t: List<Ciudad>) {
                        ciudadesViewModel.deleteAllFromDatabase().subscribeBy(onComplete= {
                            ciudadesViewModel.insertAllCiudadesToDatabase(t).subscribeBy(onComplete= {
                                data.clear()
                                data.addAll(t)
                                cache.clear()
                                cache.addAll(data)
                                adapter?.notifyDataSetChanged()

                                afterRefresh()
                            })
                        })


                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        afterRefresh()
                    }

                })

            },
            {item, data, cache, adapter ->
                //DELETE CALLBACK
                item as Ciudad
                ciudadesViewModel.deleteCiudad(item.codCiudad!!).subscribe(object: AguaSanObserver<GenericApiResponse>(this) {
                    override fun onNext(t: GenericApiResponse) {
                        ciudadesViewModel.deleteCiudadFromDatabase(item.codCiudad).subscribeBy(onComplete={
                            UtilTools.showDialog(this@CiudadActivity, t.message) {
                                ciudadesViewModel.getCiudadesFromDatabase().subscribe(object: AguaSanObserver<List<Ciudad>>(this@CiudadActivity){
                                    override fun onNext(t: List<Ciudad>) {
                                        data.clear()
                                        cache.clear()
                                        data.addAll(t)
                                        cache.addAll(data)
                                        adapter?.notifyDataSetChanged()
                                    }

                                })

                            }
                        })


                    }

                })

            })


        btAddCiudad.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, NuevaCiudadFragment.newInstance())
                .addToBackStack(null)
                .commit()

        }

        supportFragmentManager.beginTransaction()
            .add(R.id.fragment, fragment)
            .commit()



        supportFragmentManager.addOnBackStackChangedListener {
            val current = supportFragmentManager.backStackEntryCount + 1
            when (current) {
                1 -> {
                    modoEdicion = false
                    supportActionBar?.title = "Ciudades"
                    btAddCiudad?.visibility = View.VISIBLE
                }
                2 -> {
                    if(!modoEdicion) {
                        supportActionBar?.title = "Nueva Ciudad"
                    }else{
                        supportActionBar?.title = "Modificar Ciudad"
                    }
                    btAddCiudad?.visibility = View.GONE
                }
            }
        }

    }
}
