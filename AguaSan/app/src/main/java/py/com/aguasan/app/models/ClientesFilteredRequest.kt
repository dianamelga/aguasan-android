package py.com.aguasan.app.models

import java.io.Serializable

/**
 * Created by Diana Melgarejo on 7/21/21.
 */
data class ClientesFilteredRequest(
    val pagina: Int,
    val cantidad: Int,
    val filtros: Map<String, String>
): Serializable

object ClientesFilters {
    const val codCliente = "codCliente"
    const val nroDocumento = "nroDocumento"
    const val nombres = "nombres"
    const val apellidos = "apellidos"
}