package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Entity(tableName = "tipo_documento")
data class TipoDocumento(
    @PrimaryKey
    @ColumnInfo(name = "cod_tipo_documento") var codTipoDocumento: String,
    @ColumnInfo(name = "tipo_documento") var tipoDocumento: String
) {
    companion object {
        fun TipoDocumento.toDomain() : py.com.aguasan.app.models.TipoDocumento {
            return py.com.aguasan.app.models.TipoDocumento(
                codTipoDocumento = this.codTipoDocumento,
                tipoDocumento = this.tipoDocumento
            )
        }
    }
}

const val TIPO_DOCUMENTO_TABLE_NAME = "tipo_documento"