package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import okhttp3.internal.userAgent
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.TipoDocumento
import py.com.aguasan.app.persistence.entities.Barrio.Companion.toDomain
import py.com.aguasan.app.utils.toDiasVisita

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Entity(tableName = "$CLIENTE_TABLE_NAME")
data class Cliente(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "cod_cliente_local") var codClienteLocal: Int=0,
    @ColumnInfo(name = "cod_cliente") var codCliente: Int,
    @ColumnInfo(name = "nombres") var nombres: String,
    @ColumnInfo(name = "apellidos") var apellidos: String,
    @ColumnInfo(name = "celular") var celular: String,
    @ColumnInfo(name = "telefono") var telefono: String?,
    @ColumnInfo(name = "cod_tipo_documento") var codTipoDocumento: String,
    @ColumnInfo(name = "nro_documento") var nroDocumento: String,
    @ColumnInfo(name = "direccion") var direccion: String,
    @ColumnInfo(name = "observacion") var observacion: String?,
    @ColumnInfo(name = "cod_ciudad") var codCiudad: String,
    @ColumnInfo(name = "cod_barrio") var codBarrio: String,
    @ColumnInfo(name = "dias_visita") var diasVisita: String,
    @ColumnInfo(name = "activo") var activo: String,
    @ColumnInfo(name = "fec_alta") var fecAlta: String,
    @ColumnInfo(name = "usr_alta") var usrAlta: String,
    @ColumnInfo(name = "fec_ult_act") var fecUltAct: String,
    @ColumnInfo(name = "usr_ult_act") var usrUltAct: String
) {
    fun toDomain(): Cliente {
        //TODO: complete the data
        return Cliente(
            codCliente = codCliente,
            nombres = nombres,
            apellidos = apellidos,
            celular = celular,
            telefono = telefono,
            tipoDocumento = TipoDocumento(codTipoDocumento = codTipoDocumento, tipoDocumento = ""),
            nroDocumento = nroDocumento,
            ciudad = Ciudad(codCiudad = codCiudad, ciudad= ""),
            barrio = Barrio(codBarrio = codBarrio, codCiudad = codCiudad, barrio = "").toDomain(),
            direccion = direccion,
            observacion = observacion,
            diasVisita = diasVisita.toDiasVisita(),
            formulario = null,
            ubicaciones = null,
            visitas = null,
            formularios = null,
            activo = activo,
            fechaAlta = fecAlta,
            userAlta = usrAlta,
            fechaUltimaAct = fecUltAct,
            userUltimaAct = usrUltAct

        )
    }
}

const val CLIENTE_TABLE_NAME = "clientes"