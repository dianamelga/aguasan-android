package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.VisitaRepository
import py.com.aguasan.app.models.Visita
import java.util.concurrent.TimeUnit

class VisitaViewModel (var application: AguaSan, context: Context?) : AguaSanViewModel(context) {

    fun createVisita(visita: Visita): Observable<Visita> {
        return VisitaRepository(application).createVisita(visita)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun getVisitas(codCliente: Int): Observable<List<Visita>> {
        return VisitaRepository(application).getVisitas(codCliente)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun getVisitasFromDatabase(codCliente: Int): Observable<List<Visita>> {
        return VisitaRepository(application).getVisitasFromDatabase(codCliente)
            .take(1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertVisitaToDatabase(visita: Visita, cb:() -> Unit ={}): Completable {
        return VisitaRepository(application).insertVisita(visita, cb)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }
}