package py.com.aguasan.app.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import py.com.aguasan.app.R

import kotlinx.android.synthetic.main.datosgenerales_item.view.*
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.Ciudad

class DatosGeneralesAdapter(
    private val values: List<Any>,
    private val callback: (Any) -> Unit = {},
    private val longCallback: (Any) -> Unit = {}
) : RecyclerView.Adapter<DatosGeneralesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.datosgenerales_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        when(item) {
            is Ciudad -> {
                holder.mIdView.text = item.codCiudad
                holder.mContentView.text = item.ciudad
            }
            is Barrio -> {
                holder.mIdView.text = item.codBarrio
                holder.mContentView.text = item.barrio + " - Ciudad: "+item.ciudad?.codCiudad+ " "+item.ciudad
            }
        }


        with(holder.mView) {
            tag = item
            setOnClickListener{
                callback(item)
            }

            setOnLongClickListener {
                longCallback(item)
                true
            }
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.item_number
        val mContentView: TextView = mView.content

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
