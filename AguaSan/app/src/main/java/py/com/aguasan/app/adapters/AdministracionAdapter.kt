package py.com.aguasan.app.adapters

import android.content.Context
import android.graphics.Point
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import py.com.aguasan.app.R
import py.com.aguasan.app.models.ItemAdministracion

class AdministracionAdapter (val items: List<ItemAdministracion>)  : androidx.recyclerview.widget.RecyclerView.Adapter<AdministracionAdapter.AdminViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): AdministracionAdapter.AdminViewHolder {
        val windowManager = p0.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_list_administracion, p0, false)
        return AdminViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: AdministracionAdapter.AdminViewHolder, position: Int) {
        val item = items[position]
        holder.icon?.setImageResource(item.icon!!)
        holder.title?.text = item.title
        holder.viewItem?.setOnClickListener {
            item.callback()
        }

    }



    class AdminViewHolder (v: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var icon: ImageView?=null
        var title: TextView?=null
        var viewItem: View?=null

        init {
            icon = v.findViewById(R.id.ivIcon)
            title = v.findViewById(R.id.tvTitle)
            viewItem = v
        }
    }
}


