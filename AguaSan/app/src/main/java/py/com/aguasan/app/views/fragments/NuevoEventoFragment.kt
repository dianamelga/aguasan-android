package py.com.aguasan.app.views.fragments


import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_nuevo_evento.*
import kotlinx.android.synthetic.main.fragment_nuevo_evento.btGuardar

import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.ClientesFilteredRequest
import py.com.aguasan.app.models.Evento
import py.com.aguasan.app.utils.*
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.viewModels.EventosViewModel

class NuevoEventoFragment : AguaSanFragment() {
    private lateinit var eventosViewModel: EventosViewModel
    private lateinit var clientesViewModel: ClientesViewModel
    private var cliente: Cliente?=null
    private var clientes: ArrayList<Cliente> = ArrayList()
    private val MAX_QUERY_CLIENTES = 20

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        clientesViewModel = ClientesViewModel(aguaSan!!, aguaSanActivity!!)
        eventosViewModel = EventosViewModel(aguaSan!!, aguaSanActivity!!)
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_nuevo_evento, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etFechaEvento.isDateRestrictionActive = true
        etFechaEvento.isCurrentPreviousDateDisabled = true


        val clienteAdapter: ArrayAdapter<Cliente> = ArrayAdapter(aguaSanActivity!!, R.layout.spinner_text, clientes)

        clientesViewModel.getClientesFromDatabase().subscribe(object: AguaSanObserver<List<Cliente>>(aguaSanActivity!!){
            override fun onNext(t: List<Cliente>) {
                clientes.clear()
                clientes.addAll(t)
                if(clientes.isEmpty()) {
                    val request = ClientesFilteredRequest(1, MAX_QUERY_CLIENTES, HashMap())
                    clientesViewModel.getClientes(request).subscribe(object: AguaSanObserver<List<Cliente>>(aguaSanActivity!!) {
                        override fun onNext(t: List<Cliente>) {
                            clientesViewModel.insertAllClientesToDatabase(clientes).subscribeBy(onSuccess={
                                clientes.addAll(t)
                                clienteAdapter.notifyDataSetChanged()
                            })

                        }

                    })
                }
            }

        })



        spCliente.adapter = clienteAdapter
        spCliente.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                cliente = parent?.getItemAtPosition(position) as Cliente
            }
        }




        btGuardar.setOnClickListener {
            if(isFormValid()) {
                val evento = Evento(nroEvento = null,
                    codCliente = cliente?.codCliente,
                    fecEvento = etFechaEvento.text?.toString()?.asISO8601FromString("dd/MM/yyyy")?.asFormattedDate("yyyy-MM-dd HH:mm:ss"),
                    observacion = etObservacion.text?.toString(),
                    estado = "PEND",
                    fecAlta = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss"),
                    usrAlta = aguaSan?.session?.username!!,
                    fecUltAct = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss"),
                    usrUltAct = aguaSan?.session?.username!!)

                eventosViewModel.createEvento(evento).subscribe(object: AguaSanObserver<Evento>(aguaSanActivity!!) {
                    override fun onNext(t: Evento) {
                        UtilTools.showDialog(aguaSanActivity!!, "Evento Creado correctamente.") {
                            aguaSanActivity?.finish()
                        }
                    }

                })
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun isFormValid(): Boolean {
        return tilCliente.condition({cliente != null}, "Seleccione un cliente") &&
                tilFechaEvento.isValidTextWithFocus("Este campo es obligatorio") &&
                tilObservacion.isValidTextWithFocus("Este campo es obligatorio")
    }

    companion object {
        fun newInstance(b: Bundle?=null): NuevoEventoFragment {
            val fragment = NuevoEventoFragment()
            b?.let{
                fragment.arguments = it
            }

            return fragment

        }
    }


}
