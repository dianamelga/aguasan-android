package py.com.aguasan.app.adapters

import android.content.Context
import android.graphics.Point
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Visita
import py.com.aguasan.app.utils.asFormattedDateWithHour

class VisitaAdapter (val items: List<Visita>)  : androidx.recyclerview.widget.RecyclerView.Adapter<VisitaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val windowManager = parent.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_visita, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.nroVisita?.text = item.nroVisita?.toString()
        holder.situacion?.text = "${item.situacion?.codSituacion} - ${item.situacion?.situacion}"
        holder.bidones?.text = item.bidones?.toString()
        holder.bidonesCanje?.text = item.bidonesCanje?.toString()
        holder.fecAlta?.text = item.fecAlta?.asFormattedDateWithHour()
        holder.usrAlta?.text = item.usrAlta
    }


    class ViewHolder(v: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var nroVisita: TextView?= null
        var situacion: TextView?= null
        var bidones: TextView? = null
        var bidonesCanje: TextView?= null
        var fecAlta: TextView?= null
        var usrAlta: TextView?= null
        init {
            nroVisita = v.findViewById(R.id.tvNroVisita)
            situacion = v.findViewById(R.id.tvSituacion)
            bidones = v.findViewById(R.id.tvBidones)
            bidonesCanje = v.findViewById(R.id.tvBidonesCanje)
            fecAlta = v.findViewById(R.id.tvFecAlta)
            usrAlta = v.findViewById(R.id.tvUsrAlta)
        }
    }
}