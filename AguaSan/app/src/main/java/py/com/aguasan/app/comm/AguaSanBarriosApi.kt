package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.GenericApiResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface AguaSanBarriosApi {
    @GET("barrios")
    fun listBarrios(
    ): Observable<List<Barrio>>

    @POST("barrios")
    fun createBarrio(
        @Body barrio: Barrio
    ): Observable<Barrio>

    @PUT("barrios")
    fun updateBarrio(
        @Body barrio: Barrio
    ): Observable<Barrio>

    @DELETE("barrios/{codBarrio}/{codCiudad}")
    fun deleteBarrio(
        @Path("codBarrio") codBarrio: String,
        @Path("codCiudad") codCiudad: String
    ): Observable<GenericApiResponse>

    @GET("barrios/{codBarrio}/{codCiudad}")
    fun getBarrio(
        @Path("codBarrio") codBarrio: String,
        @Path("codCiudad") codCiudad: String
    ): Observable<Barrio>


    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanBarriosApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanBarriosApi::class.java)
        }
    }
}