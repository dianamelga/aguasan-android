package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import py.com.aguasan.app.models.Ciudad

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Entity(tableName = "$BARRIO_TABLE_NAME", primaryKeys = ["cod_barrio","cod_ciudad"])
data class Barrio(

    @ColumnInfo(name = "cod_barrio") var codBarrio: String,
    @ColumnInfo(name = "barrio") var barrio: String,
    @ColumnInfo(name = "cod_ciudad") var codCiudad: String
) {
    companion object {
        fun Barrio.toDomain(): py.com.aguasan.app.models.Barrio {
            return py.com.aguasan.app.models.Barrio(
                codBarrio = this.codBarrio,
                barrio = this.barrio,
                ciudad = Ciudad(codCiudad = this.codCiudad, ciudad = "")
            )
        }
    }
}

const val BARRIO_TABLE_NAME = "barrio"