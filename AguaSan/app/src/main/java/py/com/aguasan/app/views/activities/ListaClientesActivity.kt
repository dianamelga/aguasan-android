package py.com.aguasan.app.views.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import py.com.aguasan.app.R
import py.com.aguasan.app.views.fragments.ListaClientesFragment

class ListaClientesActivity : AguaSanActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_clientes)

        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "Buscar Cliente"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val fragment = ListaClientesFragment.newInstance {
            val i = Intent(this, DetalleClienteActivity::class.java)
            i.putExtra("cliente", it)
            startActivity(i)
        }

        supportFragmentManager.beginTransaction()
            .add(R.id.fragment, fragment)
            .commit()
    }

}
