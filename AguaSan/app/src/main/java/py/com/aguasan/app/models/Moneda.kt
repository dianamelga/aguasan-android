package py.com.aguasan.app.models

import java.io.Serializable

class Moneda : Serializable {
    var codigo: Long? = null
    var descripcion: String? = null
    var iso: String? = null
    var simbolo: String? = null

    companion object {
        val GUARANI = Moneda().run {
            this.codigo = 6900
            this.simbolo = "GS"
            this.iso = "PYG"
            this
        }
    }
}
