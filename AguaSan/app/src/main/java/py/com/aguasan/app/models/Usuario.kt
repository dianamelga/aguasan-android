package py.com.aguasan.app.models

import py.com.aguasan.app.persistence.entities.Usuario
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.utils.asFormattedDate
import java.io.Serializable

data class Usuario(
    val userName: String?=null,
    val password: String?=null,
    val nombre: String?=null,
    val tipoDocumento: TipoDocumento?=null,
    val nroDocumento: String?=null,
    val email: String?=null,
    val isAdmin: String?=null,
    val activo: String?=null,
    val fecAlta: String?=null,
    val fecUltAct: String?=null
): Serializable {
    fun toEntity(): Usuario {
        val now = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss")
        return py.com.aguasan.app.persistence.entities.Usuario(
            username = userName!!,
            password = password!!,
            nombre = nombre!!,
            codTipoDocumento = tipoDocumento?.codTipoDocumento!!,
            nroDocumento = nroDocumento!!,
            email = email!!,
            esAdmin = isAdmin!!,
            activo = activo!!,
            fecAlta = fecAlta ?: now,
            fecUltAct = fecUltAct ?: now
        )
    }
}