package py.com.aguasan.app.views.activities

import android.annotation.TargetApi
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_captura_ubicacion.*
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.TipoUbicacion
import py.com.aguasan.app.models.Ubicacion
import py.com.aguasan.app.utils.*
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.viewModels.HomeViewModel

class CapturaUbicacionActivity : AguaSanActivity() {
    private lateinit var viewModel: ClientesViewModel
    private lateinit var homeViewModel: HomeViewModel




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_captura_ubicacion)
        viewModel = ClientesViewModel(aguasan!!, this)
        homeViewModel = HomeViewModel(aguasan!!, this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "Nueva Ubicación de Cliente"

        homeViewModel.getTiposUbicacionesFromDatabase().subscribe(object: AguaSanObserver<List<TipoUbicacion>>(this){
            override fun onNext(ubicaciones: List<TipoUbicacion>) {
                spUbicacion.adapter = ArrayAdapter<TipoUbicacion>(this@CapturaUbicacionActivity, R.layout.spinner_text, ubicaciones)
            }

        })


        btCapturarCoordenadas.setOnClickListener {
            if (LocationHelper.hasPermissions(this)) {
                val locationHelper = LocationHelper(this) {
                    etLatitud.setText(it?.latitude.toString())
                    etLongitud.setText(it?.longitude.toString())

                    etLatitud.isEnabled = false
                    etLongitud.isEnabled = false
                    viewModel.dismissProgress()


                }

                viewModel.showProgress()
                locationHelper.captureLocation()
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        LocationHelper.getPermissionsNeeded(this),
                        LocationHelper.REQUEST_PERMISSIONS
                    )
                }
            }
        }

        btAceptar.setOnClickListener {
            if(isFormValid()) {
                val now = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss")
                val user = aguasan!!.session!!.username
                val ubi = Ubicacion(
                    tipoUbicacion = spUbicacion.selectedItem as TipoUbicacion,
                    latitud = etLatitud.text.toString(),
                    longitud = etLongitud.text.toString(),
                    cliente = intent?.extras?.getSerializable("cliente") as Cliente?,
                    referencia = etReferencia?.text.toString(),
                    fecAlta = now,
                    usrAlta = user,
                    fecUltAct = now,
                    usrUltAct = user
                )

                intent.putExtra("ubicacion", ubi)
                setResult(Activity.RESULT_OK, intent)

                finish()
            }
        }

        btCancelar.setOnClickListener {
            finish()
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LocationHelper.REQUEST_PERMISSIONS) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                btAceptar.performClick()
            }
        }
    }

    fun isFormValid(): Boolean {
        return tilLatitud.isValidTextWithFocus("Debe ingresar Latitud") &&
                tilLongitud.isValidTextWithFocus("Debe ingresar Longitud") &&
                tilReferencia.isValidTextWithFocus("Ingrese una referencia")
    }

}
