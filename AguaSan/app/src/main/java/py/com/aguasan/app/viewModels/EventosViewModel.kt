package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.EventosRepository
import py.com.aguasan.app.models.Evento
import java.util.concurrent.TimeUnit

class EventosViewModel(var application: AguaSan, context: Context?) : AguaSanViewModel(context) {

    fun getListEventos(): Observable<List<Evento>> {
        return EventosRepository(application).getListEventos()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(1)
    }

    fun getEvento(nroEvento: Int) : Observable<Evento> {
        return EventosRepository(application).getEvento(nroEvento)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(1)
    }

    fun createEvento(evento: Evento): Observable<Evento> {
        return EventosRepository(application).createEvento(evento)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }

    fun updateEvento(evento: Evento) : Observable<Evento> {
        return EventosRepository(application).updateEvento(evento)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }
}