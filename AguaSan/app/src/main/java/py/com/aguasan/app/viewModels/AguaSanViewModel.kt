package py.com.aguasan.app.viewModels

import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import py.com.aguasan.app.utils.ActivityTracker
import py.com.aguasan.app.views.activities.AguaSanActivity

open class AguaSanViewModel(var ctx : Context?) {
    private val activityTracker: ActivityTracker =  ActivityTracker.instance

    init {
        activityTracker.counter.subscribe {
            if(it <= 0) {
                dismissProgress()
            }
        }
    }

    fun showProgress() {
        ctx?.let {
            var intent = Intent(AguaSanActivity.SHOW_PROGRESS_ACTION)
            LocalBroadcastManager.getInstance(it)
                .sendBroadcast(intent)
        }
    }

    fun dismissProgress() {
        ctx?.let {
            var intent = Intent(AguaSanActivity.DISMISS_PROGRESS_ACTION)
            LocalBroadcastManager.getInstance(it)
                .sendBroadcast(intent)
        }
    }

    fun addTask() {
        var counter = activityTracker.counter.value ?: 0
        if (counter == 0) {
            showProgress()
        }
        counter++
        activityTracker.counter.onNext(counter)
    }

    fun finishTask() {
        var counter = activityTracker.counter.value ?: 0
        counter--
        activityTracker.counter.onNext(counter)
    }

}