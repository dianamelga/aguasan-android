package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.LoginReq
import py.com.aguasan.app.models.LoginResponse
import py.com.aguasan.app.models.SerialResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface AguaSanAuthApi {


    @POST("aguasan-login/login")
    fun login(
        @Body request: LoginReq
    ): Observable<LoginResponse>

    @POST("aguasan-auth/serial")
    fun serial(
        @Body request: LoginReq
    ): Observable<SerialResponse>



    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanAuthApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanAuthApi::class.java)
        }
    }
}