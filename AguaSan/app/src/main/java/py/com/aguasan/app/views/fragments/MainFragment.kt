package py.com.aguasan.app.views.fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_main.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.MainAdapter
import py.com.aguasan.app.views.activities.AdministracionActivity
import py.com.aguasan.app.views.activities.ClienteActivity
import py.com.aguasan.app.views.activities.EventoActivity
import py.com.aguasan.app.views.activities.ListaClientesActivity

class MainFragment : AguaSanFragment() {

    private var adapter: MainAdapter? = null
    private var options: ArrayList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        adapter = MainAdapter(aguaSanActivity!!, options)
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        options.add(MainAdapter.FIND_CLIENT)
        options.add(MainAdapter.ADD_CLIENT)
        options.add(MainAdapter.FIND_EVENT)
        options.add(MainAdapter.ADD_EVENT)
        if(aguaSan?.session?.esAdmin == true)
            options.add(MainAdapter.ADMIN)

        adapter?.callback = {
            when(it) {
                MainAdapter.FIND_CLIENT -> {
                    val i = Intent(aguaSanActivity!!, ListaClientesActivity::class.java)
                    aguaSanActivity!!.startActivity(i)
                }
                MainAdapter.ADD_CLIENT -> {
                    val i = Intent(aguaSanActivity!!, ClienteActivity::class.java)
                    i.putExtra(ListaUbicacionesFragment.PERMITE_INCLUIR, true)
                    startActivity(i)
                    aguaSanActivity!!.showProgress()
                }
                MainAdapter.ADMIN -> {
                    val i = Intent(aguaSanActivity!!, AdministracionActivity::class.java)
                    startActivity(i)
                }
                MainAdapter.ADD_EVENT -> {
                    val i = Intent(aguaSanActivity!!, EventoActivity::class.java)
                    i.putExtra(EventoActivity.ADD, true)
                    startActivity(i)
                }
                MainAdapter.FIND_EVENT -> {
                    val i = Intent(aguaSanActivity!!, EventoActivity::class.java)
                    i.putExtra(EventoActivity.ADD, false)
                    startActivity(i)
                }
            }

        }
        adapter?.notifyDataSetChanged()


        rvOptions.layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 2)
        rvOptions.adapter = adapter


    }

    companion object {
        fun newInstance(b: Bundle? = null): AguaSanFragment {
            val fragment = MainFragment()
            b?.let { fragment.arguments = it }
            return fragment
        }

    }
}
