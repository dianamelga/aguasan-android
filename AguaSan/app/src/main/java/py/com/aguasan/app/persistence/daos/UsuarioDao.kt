package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.persistence.entities.Usuario

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface UsuarioDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(usuarios: List<Usuario>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewUsuario(usuario: Usuario): Completable

    @Update
    fun updateUsuario(usuario: Usuario): Completable

    @Query("SELECT * FROM usuarios")
    fun getAll(): Observable<List<Usuario>>

    @Query("DELETE FROM usuarios")
    fun deleteAll(): Completable

    @Query("DELETE FROM usuarios WHERE username = :userName")
    fun deleteUser(userName: String): Completable
}