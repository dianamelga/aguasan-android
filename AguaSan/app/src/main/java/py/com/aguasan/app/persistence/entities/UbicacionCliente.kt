package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Entity(tableName = "$UBICACION_CLIENTE_TABLE_NAME")
data class UbicacionCliente(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "cod_ubicacion_local") var codUbicacionLocal: Int=0,
    @ColumnInfo(name = "cod_ubicacion") var codUbicacion: Int,
    @ColumnInfo(name = "cod_tipo_ubicacion") var codTipoUbicacion: Int,
    @ColumnInfo(name = "cod_cliente") var codCliente: Int,
    @ColumnInfo(name = "latitud") var latitud: String,
    @ColumnInfo(name = "longitud") var longitud: String,
    @ColumnInfo(name = "path_foto") var pathFoto: String,
    @ColumnInfo(name = "referencia") var referencia: String,
    @ColumnInfo(name = "fec_alta") var fecAlta: String,
    @ColumnInfo(name = "usr_alta") var usrAlta: String,
    @ColumnInfo(name = "fec_ult_act") var fecUltAct: String,
    @ColumnInfo(name = "usr_ult_act") var usrUltAct: String
)

const val UBICACION_CLIENTE_TABLE_NAME = "ubicaciones_clientes"