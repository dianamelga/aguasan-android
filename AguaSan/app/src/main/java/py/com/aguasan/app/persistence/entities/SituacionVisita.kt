package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/26/20.
 */

const val SITUACION_VISITA = "situacion_visita"

@Entity(tableName = "situacion_visita")
data class SituacionVisita(
    @PrimaryKey
    @ColumnInfo(name = "cod_situacion") var codSituacion: Int,
    @ColumnInfo(name = "situacion") var situacion: String
) {
    companion object {
        fun SituacionVisita.toDomain() : py.com.aguasan.app.models.SituacionVisita {
            return py.com.aguasan.app.models.SituacionVisita(
                codSituacion = this.codSituacion,
                situacion = this.situacion
            )
        }
    }
}