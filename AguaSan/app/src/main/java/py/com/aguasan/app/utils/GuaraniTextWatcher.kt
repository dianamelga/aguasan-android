package py.com.aguasan.app.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.util.*

class GuaraniTextWatcher : TextWatcher {
    private var df: DecimalFormat? = null
    private var dfnd: DecimalFormat? = null
    private var hasFractionalPart: Boolean = false
    private var et: EditText? = null

    constructor(et: EditText) {
        val nf = NumberFormat.getNumberInstance(Locale("es", "PY"))
        this.df = nf as DecimalFormat
        this.df!!.applyPattern("###,###.##")
        this.df!!.isDecimalSeparatorAlwaysShown = true
        this.dfnd = nf
        this.dfnd!!.applyPattern("#,###")
        this.et = et
        this.hasFractionalPart = false
    }

    constructor(et: EditText, current: Locale) {
        val nf = NumberFormat.getNumberInstance(current)
        this.df = nf as DecimalFormat
        this.df!!.applyPattern("###,##0.00")
        this.df!!.isDecimalSeparatorAlwaysShown = true
        this.dfnd = nf
        this.dfnd!!.applyPattern("#,###")
        this.et = et
        this.hasFractionalPart = false
    }

    override fun afterTextChanged(s: Editable) {
        this.et!!.removeTextChangedListener(this)

        try {
            val inilen = this.et!!.text.length
            val v = s.toString().replace(this.df!!.decimalFormatSymbols.groupingSeparator.toString(), "")
            val n = this.df!!.parse(v)
            val cp = this.et!!.selectionStart
            if (this.hasFractionalPart) {
                this.et!!.setText(this.df!!.format(n))
            } else {
                this.et!!.setText(this.dfnd!!.format(n))
            }

            val endlen = this.et!!.text.length
            val sel = cp + (endlen - inilen)
            if (sel > 0 && sel <= this.et!!.text.length) {
                this.et!!.setSelection(sel)
            } else {
                this.et!!.setSelection(this.et!!.text.length - 1)
            }
        } catch (var8: NumberFormatException) {
        } catch (var9: ParseException) {
        }

        this.et!!.addTextChangedListener(this)
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        this.hasFractionalPart = s.toString().contains(this.df!!.decimalFormatSymbols.decimalSeparator.toString())
    }

    companion object {
        private val TAG = "NumberTextWatcher"
    }
}