package py.com.aguasan.app.models

import java.io.Serializable

data class TipoUbicacion(
    val codTipoUbicacion: String? = null,
    val tipoUbicacion: String? = null
) : Serializable {

    override fun toString(): String {
        return tipoUbicacion ?: super.toString()
    }

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is TipoUbicacion -> {
                codTipoUbicacion == other.codTipoUbicacion
            }
            else -> super.equals(other)
        }
    }

    fun toEntity(): py.com.aguasan.app.persistence.entities.TipoUbicacion {
        return py.com.aguasan.app.persistence.entities.TipoUbicacion(
            codTipoUbicacion = codTipoUbicacion!!,
            tipoUbicacion =  tipoUbicacion!!
        )
    }

}