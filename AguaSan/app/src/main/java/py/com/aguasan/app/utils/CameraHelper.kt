package py.com.aguasan.app.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat

class CameraHelper {

    companion object {
        const val REQUEST_PERMISSIONS = 102

        fun hasPermissions(context: Context): Boolean {
            return Build.VERSION.SDK_INT >= 21 &&
                    ContextCompat.checkSelfPermission(
                        context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
        }


        fun getPermissionsNeeded(activity: Activity): Array<String> {
            val permissions = ArrayList<String>()
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            return permissions.toTypedArray()
        }
    }
}