package py.com.aguasan.app.views.fragments


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_administracion.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.AdministracionAdapter
import py.com.aguasan.app.models.*
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.viewModels.BarriosViewModel
import py.com.aguasan.app.viewModels.CiudadesViewModel
import py.com.aguasan.app.viewModels.HomeViewModel
import py.com.aguasan.app.views.activities.BarrioActivity
import py.com.aguasan.app.views.activities.CiudadActivity
import py.com.aguasan.app.views.activities.UsuarioActivity

class AdministracionFragment : AguaSanFragment() {

    private var adapter: AdministracionAdapter? = null
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var ciudadesViewModel: CiudadesViewModel
    private lateinit var barriosViewModel: BarriosViewModel

    init {
        resourceId = R.layout.fragment_administracion
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        homeViewModel = HomeViewModel(aguaSan!!, aguaSanActivity!!)
        ciudadesViewModel = CiudadesViewModel(aguaSan!!, aguaSanActivity!!)
        barriosViewModel = BarriosViewModel(aguaSan!!, aguaSanActivity!!)
        return inflater.inflate(R.layout.fragment_administracion, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val items: ArrayList<ItemAdministracion> = ArrayList()
        items.add(ItemAdministracion(
            R.drawable.ic_user, "Usuarios"
        ) {
            val i = Intent(aguaSanActivity!!, UsuarioActivity::class.java)
            startActivity(i)
        })
        items.add(ItemAdministracion(R.drawable.navigation_empty_icon, "Ciudades")
        {
            ciudadesViewModel.getCiudadesFromDatabase().subscribe(object: AguaSanObserver<List<Ciudad>>(aguaSanActivity!!) {
                override fun onNext(t: List<Ciudad>) {
                    val ciudades= ArrayList<Ciudad>()
                    ciudades.addAll(t)
                    if(ciudades.isEmpty()) {
                        ciudadesViewModel.listCiudades().subscribe(object : AguaSanObserver<List<Ciudad>>(aguaSanActivity!!) {
                            override fun onNext(t: List<Ciudad>) {

                                ciudades.addAll(t)
                                val i = Intent(aguaSanActivity!!, CiudadActivity::class.java)
                                i.putExtra(ListaDatosGeneralesFragment.LISTA, ciudades)
                                startActivity(i)
                            }

                        })
                    }else{
                        val i = Intent(aguaSanActivity!!, CiudadActivity::class.java)
                        i.putExtra(ListaDatosGeneralesFragment.LISTA, ciudades)
                        startActivity(i)
                    }
                }

            })


        })
        items.add(ItemAdministracion(R.drawable.navigation_empty_icon, "Barrios") {

            val i = Intent(aguaSanActivity!!, BarrioActivity::class.java)
            startActivity(i)
        })
        items.add(ItemAdministracion(R.drawable.ic_excel, "Reportes")
        {
            val reporteReq = ReporteReq(aguaSan?.session?.mail)
            homeViewModel.sendReports(reporteReq).subscribe(object : AguaSanObserver<GenericApiResponse>(aguaSanActivity!!) {
                override fun onNext(t: GenericApiResponse) {
                    UtilTools.showDialog(aguaSanActivity!!, t.message)
                }

            })
        })

        adapter = AdministracionAdapter(items)

        val llm = androidx.recyclerview.widget.LinearLayoutManager(context)
        llm.orientation = RecyclerView.VERTICAL
        rvAdministracion.layoutManager = llm
        rvAdministracion.adapter = adapter


    }

    companion object {
        fun newInstance(b: Bundle? = null): AdministracionFragment {
            val fragment = AdministracionFragment()
            b?.let {
                fragment.arguments = it
            }

            return fragment
        }
    }


}
