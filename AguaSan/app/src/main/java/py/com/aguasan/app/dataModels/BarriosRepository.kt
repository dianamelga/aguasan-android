package py.com.aguasan.app.dataModels

import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.models.GenericApiResponse
import py.com.aguasan.app.persistence.entities.Barrio.Companion.toDomain

class BarriosRepository(var application: AguaSan) {
    private val barrioDAO = application.database!!.barrioDao()

    fun listBarrios(): Observable<List<Barrio>> {
        return application.apiBarrios!!.listBarrios()
    }

    fun createBarrio(barrio: Barrio): Observable<Barrio>{
        return application.apiBarrios!!.createBarrio(barrio)
    }

    fun updateBarrio(barrio: Barrio): Observable<Barrio> {
        return application.apiBarrios!!.updateBarrio(barrio)
    }

    fun deleteBarrio(codBarrio: String, codCiudad: String): Observable<GenericApiResponse> {
        return application.apiBarrios!!.deleteBarrio(codBarrio, codCiudad)
    }

    fun getBarrio(codBarrio: String, codCiudad: String): Observable<Barrio> {
        return application.apiBarrios!!.getBarrio(codBarrio, codCiudad)
    }

    fun insertBarrioToDatabase(barrio: Barrio): Completable {
        return barrioDAO.insert(barrio.toEntity())
    }

    fun insertAllBarriosToDatabase(barrios: List<Barrio>): Completable {
        return barrioDAO.insertAll(barrios.map{ domain -> domain.toEntity() })
    }

    fun deleteBarrioFromDatabase(codBarrio: String, codCiudad: String): Completable {
        return barrioDAO.deleteBarrio(codBarrio, codCiudad)
    }

    fun deleteAllFromDatabase(): Completable {
        return barrioDAO.deleteAll()
    }


    fun getBarriosFromDatabase(ciudad: Ciudad): Observable<List<Barrio>> {
        return barrioDAO.getBarrioFromCiudad(ciudad.codCiudad!!).map { barrios -> barrios.map { entity -> entity.toDomain() }}
    }

    fun getAllBarriosFromDatabase(): Observable<List<Barrio>> {
        return barrioDAO.getAll().map { barrios -> barrios.map { entity -> entity.toDomain() }}
    }
}