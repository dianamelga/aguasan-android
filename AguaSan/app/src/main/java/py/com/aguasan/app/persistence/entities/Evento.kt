package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/26/20.
 */

const val EVENTO_TABLE_NAME = "evento"
@Entity(tableName = "$EVENTO_TABLE_NAME")
data class Evento(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "nro_evento_local") var nroEventoLocal: Int,
    @ColumnInfo(name = "nro_evento") var nroEvento: Int,
    @ColumnInfo(name = "cod_cliente") var codCliente: Int,
    @ColumnInfo(name = "fec_evento") var fecEvento: String,
    @ColumnInfo(name = "observacion") var observacion: String,
    @ColumnInfo(name = "estado") var estado: String,
    @ColumnInfo(name = "fec_alta") var fecAlta: String,
    @ColumnInfo(name = "usr_alta") var usrAlta: String,
    @ColumnInfo(name = "fec_ult_act") var fecUltAct: String,
    @ColumnInfo(name = "usr_ult_act") var usrUltAct: String
)