package py.com.aguasan.app.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.fragment_cliente1.*
import okhttp3.internal.notify
import py.com.aguasan.app.AguaSan

import py.com.aguasan.app.R
import py.com.aguasan.app.models.*
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.isValidMobileNumberWithFocus
import py.com.aguasan.app.utils.isValidTextWithFocus
import py.com.aguasan.app.viewModels.BarriosViewModel
import py.com.aguasan.app.viewModels.CiudadesViewModel

import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.viewModels.HomeViewModel
import py.com.aguasan.app.views.activities.ClienteActivity

class Cliente1Fragment : AguaSanFragment(), Step {
    private val ciudades = ArrayList<Ciudad>()
    private val barrios = ArrayList<Barrio>()
    private val tiposDocumento = ArrayList<TipoDocumento>()
    private var selectedCiudad: Ciudad? = null
    private var selectedBarrio: Barrio? = null
    private var selectedTipoDocumento: TipoDocumento? = null


    private lateinit var clienteViewModel: ClientesViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var ciudadesViewModel: CiudadesViewModel
    private lateinit var barriosViewModel: BarriosViewModel

    private lateinit var tipoDocumentoAdapter: ArrayAdapter<TipoDocumento>
    private lateinit var ciudadAdapter: ArrayAdapter<Ciudad>
    private lateinit var barrioAdapter: ArrayAdapter<Barrio>

    init {
        resourceId = R.layout.fragment_cliente1
    }

    override fun onSelected() {
    }

    override fun verifyStep(): VerificationError? {
        return if (isFormValid()) {
            val cliente = (activity as ClienteActivity).cliente

            cliente.nombres = etNombres.text.toString()
            cliente.apellidos = etApellidos.text.toString()
            cliente.tipoDocumento = selectedTipoDocumento
            cliente.nroDocumento = etNroDocumento.text.toString()
            cliente.ciudad = selectedCiudad
            cliente.barrio = selectedBarrio
            cliente.celular = etCelular.text.toString()
            cliente.telefono = etTelefono.text.toString()
            cliente.direccion = etDireccion.text.toString()
            cliente.observacion = etObservacion?.text?.toString()

            null
        } else {
            VerificationError("Error!")
        }

    }

    override fun onError(error: VerificationError) {
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        clienteViewModel = ClientesViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        homeViewModel = HomeViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        ciudadesViewModel= CiudadesViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        barriosViewModel = BarriosViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        return inflater.inflate(R.layout.fragment_cliente1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tipoDocumentoAdapter = ArrayAdapter(aguaSanActivity!!, R.layout.spinner_text, tiposDocumento)
        ciudadAdapter = ArrayAdapter(aguaSanActivity!!, R.layout.spinner_text, ciudades)
        barrioAdapter = ArrayAdapter(aguaSanActivity!!, R.layout.spinner_text, barrios)

        val tagCiudad = "Seleccione una Ciudad"
        val tagBarrio = "Seleccione un Barrio"
        val tagTipoDocumento = "Seleccione un Tipo de Documento"

        spCiudad.adapter = ciudadAdapter
        spBarrio.adapter = barrioAdapter
        spTipoDocumento.adapter = tipoDocumentoAdapter

        setSpinnerListeners()


        ciudadesViewModel.getCiudadesFromDatabase().subscribe(object: AguaSanObserver<List<Ciudad>>(aguaSanActivity!!) {
                override fun onNext(t: List<Ciudad>) {
                    ciudades.clear()
                    ciudades.add(0, Ciudad("0", tagCiudad))
                    ciudades.addAll(t)
                    ciudadAdapter.notifyDataSetChanged()
                    cliente?.let{
                        spCiudad.setSelection(ciudades.indexOf(it.ciudad))
                        barriosViewModel.getBarriosFromDatabase(it.ciudad!!).subscribe(object: AguaSanObserver<List<Barrio>>(aguaSanActivity!!){
                            override fun onNext(t: List<Barrio>) {
                                barrios.clear()
                                barrios.add(0, Barrio("0", tagBarrio))
                                barrios.addAll(t)
                                barrioAdapter.notifyDataSetChanged()
                                spBarrio.setSelection(barrios.indexOf(it.barrio))
                            }

                        })
                    }
                }

        })




        homeViewModel.getTiposDocumentoFromDatabase().subscribe(object: AguaSanObserver<List<TipoDocumento>>(aguaSanActivity!!) {
                override fun onNext(t: List<TipoDocumento>) {
                    tiposDocumento.clear()
                    tiposDocumento.add(0, TipoDocumento("0", tagTipoDocumento))
                    tiposDocumento.addAll(t)
                    tipoDocumentoAdapter.notifyDataSetChanged()
                    cliente?.let{spTipoDocumento.setSelection(tiposDocumento.indexOf(it.tipoDocumento))}

                }

        })




        cliente?.let {
            //etNroFormulario.setText(it.nroFormulario?.toString())
            etNombres.setText(it.nombres)
            etApellidos.setText(it.apellidos)
            etNroDocumento.setText(it.nroDocumento)
            etCelular.setText(it.celular)
            etTelefono.setText(it.telefono)
            etDireccion.setText(it.direccion)
            etObservacion.setText(it.observacion)
        }

    }

    private fun setSpinnerListeners() {
        val tagBarrio = "Seleccione un Barrio"

        spCiudad.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position > 0) {
                    barriosViewModel.getBarriosFromDatabase(parent?.getItemAtPosition(position) as Ciudad).subscribe(object: AguaSanObserver<List<Barrio>>(aguaSanActivity!!){
                        override fun onNext(t: List<Barrio>) {
                            barrios.clear()
                            barrios.add(0, Barrio("0", tagBarrio))
                            barrios.addAll(t)
                            barrioAdapter.notifyDataSetChanged()
                            selectedCiudad = parent?.getItemAtPosition(position) as Ciudad
                        }

                    })

                } else {
                    barrioAdapter.notifyDataSetChanged()
                    selectedCiudad = null
                }
                barrioAdapter.notifyDataSetChanged()

            }
        }

        spBarrio.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position > 0) {
                    selectedBarrio = parent?.getItemAtPosition(position) as Barrio
                } else {
                    selectedBarrio = null
                }
            }
        }

        spTipoDocumento.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position > 0) {
                    selectedTipoDocumento = parent?.getItemAtPosition(position) as TipoDocumento
                    if(selectedTipoDocumento?.codTipoDocumento == "N/A") etNroDocumento.setText("")
                } else {
                    selectedTipoDocumento = null
                }
            }
        }
    }

    override fun isFormValid(): Boolean {

        return (//tilNroFormulario.isValidTextWithFocus("Ingrese un número de formulario válido") &&
                tilNombres.isValidTextWithFocus("Ingrese un nombre válido") &&
                tilApellidos.isValidTextWithFocus("Ingrese un apellido válido") &&
                (if (selectedTipoDocumento == null) {
                    tilTipoDocumento.error = "Elija un Tipo de Documento válido"
                    false
                } else {
                    tilTipoDocumento.error = null
                    true
                }) &&
                        (("N/A" != (spTipoDocumento.selectedItem as TipoDocumento).codTipoDocumento &&
                                tvNroDocumento.isValidTextWithFocus("Ingrese un Número de Documento válido")) ||
                        "N/A" == (spTipoDocumento.selectedItem as TipoDocumento).codTipoDocumento )&&
                (if (selectedCiudad == null) {
                    tilCiudad.error = "Elija una ciudad válida"
                    false
                } else {
                    tilCiudad.error = null
                    true
                }) &&
                (if (selectedBarrio == null) {
                    tilBarrio.error = "Elija un barrio válido"
                    false
                } else {
                    tilBarrio.error = null
                    true
                }) &&
                        (tilCelular.editText?.text.isNullOrEmpty() ||
                                tilCelular.isValidMobileNumberWithFocus("Ingrese un celular válido")) &&
                        (tilTelefono.editText?.text.isNullOrEmpty() ||
                                tilTelefono.isValidTextWithFocus("Ingrese un Teléfono válido.")) &&
                tilDireccion.isValidTextWithFocus("Ingrese una dirección válida"))
    }

    companion object {
        var cliente: Cliente? = null
        fun newInstance(b: Bundle?): Cliente1Fragment {
            val fragment = Cliente1Fragment()
            fragment.arguments = b
            cliente = null
            b?.let {
                it.getSerializable("cliente")?.let{ cli ->
                    this.cliente = cli as Cliente?
                }
            }


            return fragment
        }
    }

}
