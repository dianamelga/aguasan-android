package py.com.aguasan.app.views.activities

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.stepstone.stepper.Step
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_cliente.*
import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.MyStepperAdapter
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.utils.*
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.views.fragments.Cliente1Fragment
import py.com.aguasan.app.views.fragments.Cliente2Fragment
import py.com.aguasan.app.views.fragments.ListaUbicacionesFragment


class ClienteActivity : AguaSanActivity(), StepperLayout.StepperListener {

    var cliente: Cliente = Cliente()
    private lateinit var viewModel: ClientesViewModel
    var modificar = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cliente)
        viewModel = ClientesViewModel(aguasan!!, this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "Nuevo Cliente"

        intent?.extras?.getSerializable("cliente")?.let {
            modificar = true
            supportActionBar?.title = "Modificar Cliente"
            cliente = it as Cliente
        }

        intent?.extras?.putBoolean(ListaUbicacionesFragment.ADD_FOTO, true)

        val fragments = ArrayList<Step>()
        fragments.add(Cliente1Fragment.newInstance(intent?.extras))
        fragments.add(Cliente2Fragment.newInstance(intent?.extras))
        fragments.add(ListaUbicacionesFragment.newInstance(intent?.extras))
        val titles = ArrayList<String>()
        titles.add("Información")
        titles.add("Datos de Visita y Comodatos")
        titles.add("Ubicaciones")

        val stepperAdapter = MyStepperAdapter(
            supportFragmentManager, this@ClienteActivity,
            fragments, titles
        )

        stepperLayout.adapter = stepperAdapter
        stepperLayout.setListener(this)

        dismissProgress()

    }

    override fun onStepSelected(newStepPosition: Int) {

    }

    override fun onError(verificationError: VerificationError?) {

    }

    override fun onReturn() {

    }

    override fun onCompleted(completeButton: View?) {

        if (!modificar) {
            viewModel.postCliente(cliente).subscribe(object : AguaSanObserver<Cliente>(this) {
                override fun onNext(t: Cliente) {

                    viewModel.insertClienteToDatabase(t).subscribeBy(onComplete= {
                        viewModel.insertAllFormulariosToDatabase(t.formularios ?: listOf()).subscribe()
                        viewModel.insertAllUbicacionesToDatabase(t.ubicaciones ?: listOf()).subscribe()
                        viewModel.insertAllVisitasToDatabase(t.visitas ?: listOf()).subscribe()
                        UtilTools.showDialog(this@ClienteActivity, "Cliente creado") {
                            setResult(Activity.RESULT_OK)
                            finish()
                        }
                    })
                }

            })
        } else {
            viewModel.editCliente(cliente).subscribe(object : AguaSanObserver<Cliente>(this) {
                override fun onNext(t: Cliente) {
                    UtilTools.showDialog(this@ClienteActivity, "Cliente modificado correctamente") {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }

                }

            })
        }


    }

}
