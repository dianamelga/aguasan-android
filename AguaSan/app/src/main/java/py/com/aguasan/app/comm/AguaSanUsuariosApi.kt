package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.Usuario
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface AguaSanUsuariosApi {

    @GET("aguasan-usuario")
    fun getUsuarios(
    ): Observable<List<Usuario>>

    @POST("aguasan-usuario")
    fun createUsuario(
        @Body usuario : Usuario
    ): Observable<Usuario>

    @PUT("aguasan-usuario")
    fun updateUsuario(
        @Body usuario: Usuario
    ): Observable<Usuario>

    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanUsuariosApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanUsuariosApi::class.java)
        }
    }
}