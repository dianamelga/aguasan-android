package py.com.aguasan.app.views.fragments


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_registro_visita.*

import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.SituacionVisita
import py.com.aguasan.app.models.Visita
import py.com.aguasan.app.utils.*
import py.com.aguasan.app.viewModels.HomeViewModel
import py.com.aguasan.app.viewModels.VisitaViewModel

class RegistroVisitaFragment : AguaSanFragment() {
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var visitaViewModel: VisitaViewModel
    private val situaciones: ArrayList<SituacionVisita> = ArrayList()
    private var situacionVisitaSelected: SituacionVisita?= null
    private var cliente: Cliente?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        homeViewModel = HomeViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        visitaViewModel = VisitaViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        cliente = arguments?.getSerializable("cliente") as Cliente
        return inflater.inflate(R.layout.fragment_registro_visita, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.getSituacionesVisitasFromDatabase().subscribe(object: AguaSanObserver<List<SituacionVisita>>(aguaSanActivity!!){
            override fun onNext(t: List<SituacionVisita>) {
                situaciones.clear()
                situaciones.addAll(t)
                if (situaciones.size <= 0) {
                    homeViewModel.getSituacionesVisitas()
                        .subscribe(object : AguaSanObserver<List<SituacionVisita>>(aguaSanActivity!!) {
                            override fun onNext(t: List<SituacionVisita>) {
                                situaciones.clear()
                                situaciones.addAll(t)
                                populateRadioGroups()
                            }
                        })
                }else {
                    populateRadioGroups()
                }
            }

        })


        rbCanjeTrue.setOnCheckedChangeListener { buttonView, isChecked ->
            etBidonesCanje.isEnabled = isChecked
            if(!isChecked) {
                etBidonesCanje.setText("0")
            }
        }
        rbComodatoTrue.setOnCheckedChangeListener { buttonView, isChecked ->
            etBidonesComodato.isEnabled = isChecked
            if(!isChecked) {
                etBidonesComodato.setText("0")
            }
        }



        btGuardar.setOnClickListener {
            if(isFormValid()) {
                val visita = Visita(
                    cliente = cliente,
                    situacion = situacionVisitaSelected,
                    bidonesCanje = etBidonesCanje.text.toString().toInt(),
                    bidones = etBidonesComodato.text.toString().toInt(),
                    fecAlta = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss"),
                    usrAlta = aguaSan?.session?.username,
                    fecUltAct = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss"),
                    usrUltAct = aguaSan?.session?.username
                )
                visitaViewModel.createVisita(visita).subscribe(object : AguaSanObserver<Visita>(aguaSanActivity!!) {
                    override fun onNext(t: Visita) {
                        visitaViewModel.insertVisitaToDatabase(t).subscribeBy(onComplete={
                            UtilTools.showDialog(aguaSanActivity!!, "Visita creada") {
                                val data = Intent()
                                data.putExtra("codCliente", cliente?.codCliente ?: 0)
                                aguaSanActivity!!.setResult(RESULT_OK, data)
                                aguaSanActivity!!.finish()
                            }
                        })


                    }

                })
            }
        }


    }

    private fun populateRadioGroups() {
        val radioGroup = RadioGroup(aguaSanActivity!!)
        radioGroup.orientation = LinearLayout.VERTICAL
        var check = false
        for (situ in situaciones) {
            val radioButton = RadioButton(aguaSanActivity!!)
            radioButton.id = situ.codSituacion!!
            radioButton.text = situ.situacion
            if(!check) {
                radioButton.isChecked = true
                check = true
                situacionVisitaSelected = situ
            }
            radioGroup.addView(radioButton)
        }


        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            var found = false
            for(situ in situaciones) {
                if(situ.codSituacion == checkedId) {
                    found = true
                    situacionVisitaSelected = situ
                    break
                }
            }
            if(!found) {
                situacionVisitaSelected = null
            }
        }
        radiogroupSituVisita.addView(radioGroup)
    }

    override fun isFormValid(): Boolean {
        if(situacionVisitaSelected == null) {
            UtilTools.showDialog(aguaSanActivity!!, "Debe seleccionar una situación")
            return false
        }

        if (!rbCanjeTrue.isChecked) {
            etBidonesCanje.setText("0")
        } else {

            if ((!tilBidonesCanje.isValidTextWithFocus("Ingrese un valor")) ||
                !tilBidonesCanje.condition({etBidonesCanje.text.toString().toInt() > 0}, "Ingrese un valor")) {
                return false
            }

        }

        if (!rbComodatoTrue.isChecked) {
            etBidonesComodato.setText("0")
        } else {

            if ((!tilBidonesComodato.isValidTextWithFocus("Ingrese un valor")) ||
                !tilBidonesComodato.condition({etBidonesComodato.text.toString().toInt() > 0}, "Ingrese un valor")) {
                return false
            }
        }

        return (tilBidonesCanje.isValidTextWithFocus("Ingrese un valor") &&
                tilBidonesComodato.isValidTextWithFocus("Ingrese un valor"))
    }

    companion object {
        fun newInstance(b: Bundle? = null): AguaSanFragment {
            val fragment = RegistroVisitaFragment()
            b?.let { fragment.arguments = it }
            return fragment
        }


    }


}
