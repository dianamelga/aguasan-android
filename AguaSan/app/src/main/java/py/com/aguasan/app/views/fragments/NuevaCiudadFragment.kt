package py.com.aguasan.app.views.fragments


import android.app.Activity
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_nueva_ciudad.*

import py.com.aguasan.app.R
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.utils.isValidTextWithFocus
import py.com.aguasan.app.viewModels.CiudadesViewModel

class NuevaCiudadFragment : AguaSanFragment() {
    private var ciudad: Ciudad?=null
    private lateinit var ciudadesViewModel: CiudadesViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        ciudadesViewModel = CiudadesViewModel(aguaSan!!, aguaSanActivity!!)
        arguments?.getSerializable(CIUDAD_SELECTED)?.let{
            ciudad = it as Ciudad
        }
        return inflater.inflate(R.layout.fragment_nueva_ciudad, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ciudad?.let{
            etCodCiudad.setText(it.codCiudad)
            tilCodCiudad.isEnabled = false
            etCiudad.setText(it.ciudad)
        }

        btGuardar.setOnClickListener {
            if(isFormValid()) {
                if(ciudad != null) {
                    //UPDATE
                    ciudadesViewModel.updateCiudad(Ciudad(etCodCiudad.text.toString(), etCiudad.text.toString()))
                        .subscribe(
                            object : AguaSanObserver<Ciudad>(aguaSanActivity!!) {
                                override fun onNext(t: Ciudad) {

                                    ciudadesViewModel.deleteCiudadFromDatabase(t.codCiudad!!).subscribeBy(onComplete={
                                        ciudadesViewModel.insertCiudadToDatabase(t).subscribeBy(onComplete={
                                            UtilTools.showDialog(aguaSanActivity!!, "Ciudad modificada correctamente.") {
                                                aguaSanActivity?.setResult(Activity.RESULT_OK)
                                                aguaSanActivity?.finish()
                                            }
                                        })
                                    })



                                }

                            }
                        )
                }else {
                    //INSERT
                    ciudadesViewModel.createCiudad(Ciudad(etCodCiudad.text.toString(), etCiudad.text.toString()))
                        .subscribe(
                            object : AguaSanObserver<Ciudad>(aguaSanActivity!!) {
                                override fun onNext(t: Ciudad) {

                                    ciudadesViewModel.insertCiudadToDatabase(t).subscribeBy(onComplete={
                                        UtilTools.showDialog(aguaSanActivity!!, "Ciudad creada.") {
                                            aguaSanActivity?.setResult(Activity.RESULT_OK)
                                            aguaSanActivity?.finish()
                                        }
                                    })


                                }

                            }
                        )
                }
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun isFormValid(): Boolean {
        return tilCodCiudad.isValidTextWithFocus("Debe completar este campo") &&
                tilCiudad.isValidTextWithFocus("Debe completar este campo")
    }

    companion object {
        const val CIUDAD_SELECTED = "ciudad_selected"
        fun newInstance(b: Bundle?=null): NuevaCiudadFragment {
            val fragment = NuevaCiudadFragment()
            b?.let {
                fragment.arguments = it
            }
            return fragment
        }
    }


}
