package py.com.sudameris.app.utils

import android.content.Context
import androidx.appcompat.widget.AppCompatSpinner
import android.util.AttributeSet
import android.widget.AdapterView


class CustomSpinner constructor(context: Context, attr: AttributeSet): AppCompatSpinner(context, attr) {
    var listener: AdapterView.OnItemSelectedListener? = null

    override fun setSelection(position: Int) {
        super.setSelection(position)
        listener?.onItemSelected(this, selectedView, position, 0)
    }

    fun setOnItemSelectedEvenIfUnchangedListener(
            listener: AdapterView.OnItemSelectedListener) {
        this.listener = listener
    }

}