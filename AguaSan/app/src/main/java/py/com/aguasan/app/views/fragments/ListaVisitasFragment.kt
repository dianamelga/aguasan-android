package py.com.aguasan.app.views.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.fragment_lista_visitas.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.ClienteAdapter
import py.com.aguasan.app.adapters.VisitaAdapter
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.Visita
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.viewModels.VisitaViewModel

class ListaVisitasFragment : AguaSanFragment(), Step {

    private val visitas: ArrayList<Visita> = ArrayList()
    private lateinit var visitaViewModel: VisitaViewModel
    private var adapter: VisitaAdapter?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        visitaViewModel = VisitaViewModel(aguaSan!!, aguaSanActivity!!)
        return inflater.inflate(R.layout.fragment_lista_visitas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        visitas.clear()
        cliente?.visitas?.let {
            visitas.addAll(it)
        }
        adapter = VisitaAdapter(visitas)

        val llm = androidx.recyclerview.widget.LinearLayoutManager(context)
        llm.orientation = RecyclerView.VERTICAL
        rvVisitas.layoutManager = llm
        rvVisitas.adapter = adapter


    }

    override fun onSelected() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun verifyStep(): VerificationError? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(error: VerificationError) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        var cliente: Cliente? = null
        fun newInstance(b: Bundle?): ListaVisitasFragment {
            val fragment = ListaVisitasFragment()
            fragment.arguments = b
            cliente = null
            b?.let {
                it.getSerializable("cliente")?.let{ cli ->
                    this.cliente = cli as Cliente
                }
            }
            return fragment
        }


    }




}
