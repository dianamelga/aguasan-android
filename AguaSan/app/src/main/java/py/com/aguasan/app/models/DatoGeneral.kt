package py.com.aguasan.app.models

import java.io.Serializable

data class DatoGeneral (
    val codigo: String?=null,
    val descripcion: String?=null
): Serializable