package py.com.aguasan.app.adapters;

import android.content.Context;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.util.List;

public class MyStepperAdapter extends AbstractFragmentStepAdapter {

    //private static final String CURRENT_STEP_POSITION_KEY = "1";

    private  List<Step> ListaFragment;
    private List<String> ListaTitulos;

    public void addFrament(Step fragment){
        ListaFragment.add(fragment);
        ListaTitulos.add("Titulo");
    }



    public MyStepperAdapter(@NonNull FragmentManager fm, @NonNull Context context, List<Step> Steps, List<String> Titulos) {
        super(fm, context);
        ListaFragment = Steps;
        ListaTitulos = Titulos;
    }

    @Override
    public Step createStep(@IntRange(from = 0L) int position) {

        return ListaFragment.get(position);
    }

    @Override
    public int getCount() {
        return ListaFragment.size();

    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0L) int position) {
        StepViewModel stepViewModel;

        stepViewModel = new StepViewModel.Builder(context).setTitle(ListaTitulos.get(position)).create();
        return  stepViewModel;

    }

    public  List<Step> getListaFragment() {
        return ListaFragment;
    }

    public  List<String> getListaTitulos() {
        return ListaTitulos;
    }
}
