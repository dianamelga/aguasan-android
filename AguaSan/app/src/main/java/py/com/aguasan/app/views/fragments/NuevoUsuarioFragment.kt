package py.com.aguasan.app.views.fragments


import android.app.Activity
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_nuevo_usuario.*
import kotlinx.android.synthetic.main.fragment_nuevo_usuario.btGuardar
import kotlinx.android.synthetic.main.fragment_nuevo_usuario.spTipoDocumento
import kotlinx.android.synthetic.main.fragment_nuevo_usuario.tvNroDocumento

import py.com.aguasan.app.R
import py.com.aguasan.app.models.TipoDocumento
import py.com.aguasan.app.models.Usuario
import py.com.aguasan.app.utils.*
import py.com.aguasan.app.viewModels.HomeViewModel
import py.com.aguasan.app.viewModels.UsuariosViewModel

class NuevoUsuarioFragment : AguaSanFragment() {

    private lateinit var usuariosViewModel: UsuariosViewModel
    private lateinit var homeViewModel: HomeViewModel
    private val tiposDocumento = ArrayList<TipoDocumento>()
    private var selectedTipoDocumento: TipoDocumento? = null
    private var usuario: Usuario? =null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        usuariosViewModel = UsuariosViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        homeViewModel = HomeViewModel(aguaSanActivity!!.aguasan!!, aguaSanActivity!!)
        arguments?.getSerializable(USUARIO_SELECTED)?.let{
            usuario = it as Usuario
        }

        return inflater.inflate(R.layout.fragment_nuevo_usuario, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel.getTiposDocumentoFromDatabase().subscribe(object: AguaSanObserver<List<TipoDocumento>>(aguaSanActivity!!){
            override fun onNext(t: List<TipoDocumento>) {
                val tagTipoDocumento = "Seleccione un Tipo de Documento"
                tiposDocumento.clear()
                tiposDocumento.add(0, TipoDocumento("0", tagTipoDocumento))
                tiposDocumento.addAll(t)

                setTipoDocumentoSpinner()

                usuario?.let{
                    it.tipoDocumento?.let{t ->
                        var idx = 0
                        for(tipoDoc in tiposDocumento) {
                            if(t.codTipoDocumento == tipoDoc.codTipoDocumento) {
                                spTipoDocumento.setSelection(idx, false)
                                break
                            }
                            idx++
                        }
                    }
                }
            }

        })


        usuario?.let{
            tilUsername.isEnabled = false
            lblFecUltAct.visibility = View.VISIBLE
            tvFecUltAct.visibility = View.VISIBLE
            lblFecAlta.visibility = View.VISIBLE
            tvFecAlta.visibility = View.VISIBLE

            etUsername.setText(it.userName)
            etPassword.setText(it.password)
            etNombre.setText(it.nombre)
            etNroDocumento.setText(it.nroDocumento)
            etEmail.setText(it.email)
            swEsAdmin.isChecked = it.isAdmin?.toUpperCase() == "S"
            swActivo.isChecked = it.activo?.toUpperCase() == "S"
            tvFecUltAct.text = it.fecUltAct?.asFormattedDateWithHour()
            tvFecAlta.text = it.fecAlta?.asFormattedDateWithHour()



        }


        btGuardar.setOnClickListener {

            if(isFormValid()) {
                val now = UtilTools.ISO8601now().asFormattedDate("yyyy-MM-dd HH:mm:ss")
                val usr = Usuario(
                    userName = tilUsername.editText?.text.toString(),
                    password = tilPassword.editText?.text.toString(),
                    nombre = tilNombreUsuario.editText?.text.toString(),
                    tipoDocumento = selectedTipoDocumento,
                    nroDocumento = tvNroDocumento.editText?.text.toString(),
                    email = tilEmail.editText?.text.toString(),
                    isAdmin = if (swEsAdmin.isChecked) "S" else "N",
                    activo = if (swActivo.isChecked) "S" else "N",
                    fecAlta = now,
                    fecUltAct = now
                )

                if(usuario != null) {
                    usuariosViewModel.updateUsuario(usr)
                        .subscribe(object : AguaSanObserver<Usuario>(aguaSanActivity!!) {
                            override fun onNext(t: Usuario) {

                                usuariosViewModel.deleteUsuarioFromDatabase(t.userName!!)
                                    .andThen(usuariosViewModel.insertUsuarioToDatabase(t)).subscribeBy (onComplete= {
                                        UtilTools.showDialog(aguaSanActivity!!, "Usuario modificado correctamente.") {
                                            aguaSanActivity?.setResult(Activity.RESULT_OK)
                                            aguaSanActivity?.finish()
                                        }
                                    })


                            }

                        })
                }else {
                    usuariosViewModel.createUsuario(usr)
                        .subscribe(object : AguaSanObserver<Usuario>(aguaSanActivity!!) {
                            override fun onNext(t: Usuario) {
                                usuariosViewModel.insertUsuarioToDatabase(t).subscribeBy(onComplete={
                                    UtilTools.showDialog(aguaSanActivity!!, "Usuario creado.") {
                                        aguaSanActivity?.setResult(Activity.RESULT_OK)
                                        aguaSanActivity?.finish()
                                    }
                                })


                            }

                        })
                }
            }
        }
    }

    private fun setTipoDocumentoSpinner() {
        val tipoDocumentoAdapter: ArrayAdapter<TipoDocumento> = ArrayAdapter(aguaSanActivity!!, R.layout.spinner_text, tiposDocumento)
        spTipoDocumento.adapter = tipoDocumentoAdapter

        spTipoDocumento.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position > 0) {
                    selectedTipoDocumento = parent?.getItemAtPosition(position) as TipoDocumento
                } else {
                    selectedTipoDocumento = null
                }
            }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun isFormValid(): Boolean {
        return tilUsername.isValidTextWithFocus("Ingrese un usuario válido") &&
                tilPassword.isValidTextWithFocus("Ingrese una contraseña válida") &&
                tilNombreUsuario.isValidTextWithFocus("Ingrese un nombre válido") &&
                (selectedTipoDocumento != null) &&
                tvNroDocumento.isValidTextWithFocus("Ingrese un numero de documento válido")
    }


    companion object {
        const val USUARIO_SELECTED = "usuario_selected"
        fun newInstance(b: Bundle?=null): NuevoUsuarioFragment {
            val fragment = NuevoUsuarioFragment()
            b?.let{
                fragment.arguments = it
            }

            return fragment
        }
    }

}
