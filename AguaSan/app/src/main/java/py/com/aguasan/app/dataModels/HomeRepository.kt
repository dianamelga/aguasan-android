package py.com.aguasan.app.dataModels

import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.*
import py.com.aguasan.app.persistence.entities.SituacionVisita.Companion.toDomain
import py.com.aguasan.app.persistence.entities.TipoDocumento.Companion.toDomain
import py.com.aguasan.app.persistence.entities.TipoUbicacion.Companion.toDomain

class HomeRepository(var application: AguaSan) {
    private val situacionVisitaDAO = application.database!!.situacionVisitaDao()
    private val tiposDocumentosDAO = application.database!!.tipoDocumentoDao()
    private val tipoUbicacionesDAO = application.database!!.tipoUbicacionDao()

    fun registerToken(tokenRegistrationReq: TokenRegistrationReq): Observable<TokenRegistrationResponse> {
        return application.apiFirebase!!.registerToken(tokenRegistrationReq)
    }

    fun sendReports(reporteReq: ReporteReq): Observable<GenericApiResponse> {
        return application.apiHome!!.sendReports(reporteReq)
    }

    fun getSituacionesVisitas():Observable<List<SituacionVisita>> {
        return application.apiHome!!.getSituacionesVisitas()
    }

    fun sendMail(mailRequest: MailReq): Observable<String> {
        return application.apiHome!!.sendMail(mailRequest)
    }

    fun sendDatabase(mailRequest: MailReq): Observable<String> {
        return application.apiHome!!.sendDatabase(mailRequest)
    }

    fun getTiposDocumento(): Observable<List<TipoDocumento>> {
        return application.apiHome!!.getTiposDocumento()
    }

    fun getUbicacionesTipos(): Observable<List<TipoUbicacion>> {
        return application.apiHome!!.getUbicacionesTipos()
    }

    fun getSituacionesVisitasFromDatabase(): Observable<List<SituacionVisita>> {
        return situacionVisitaDAO.getAll().map { situaciones -> situaciones.map { entity -> entity.toDomain() }}
    }

    fun getTiposDocumentoFromDatabase(): Observable<List<TipoDocumento>> {
        return tiposDocumentosDAO.getAll().map { tiposDocumentos -> tiposDocumentos.map { entity -> entity.toDomain() }}
    }

    fun getTiposUbicacionesFromDatabase(): Observable<List<TipoUbicacion>> {
        return tipoUbicacionesDAO.getAll().map{ tiposUbicaciones -> tiposUbicaciones.map { entity -> entity.toDomain() }}
    }

    fun insertUbicacionesTiposToDatabase(ubicacionesTipos: List<TipoUbicacion>): Completable {
        return tipoUbicacionesDAO.insertAll(ubicacionesTipos.map{ domain -> domain.toEntity()} )
    }

    fun deleteUbicacionesTiposFromDatabase(): Completable {
        return tipoUbicacionesDAO.deleteAll()
    }

    fun insertSituacionesVisitasToDatabase(situaciones: List<SituacionVisita>): Completable {
        return situacionVisitaDAO.insertAll(situaciones.map { domain -> domain.toEntity() })
    }

    fun deleteSituacionesVisitasFromDatabase(): Completable {
        return situacionVisitaDAO.deleteAll()
    }

    fun insertTiposDocumentosToDatabase(tiposDocumentos: List<TipoDocumento>): Completable {
        return tiposDocumentosDAO.insertAll(tiposDocumentos.map{ domain -> domain.toEntity() })
    }

    fun deleteTiposDocumentosFromDatabase(): Completable {
        return tiposDocumentosDAO.deleteAll()
    }
}