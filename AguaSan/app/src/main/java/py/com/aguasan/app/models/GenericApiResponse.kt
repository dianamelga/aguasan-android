package py.com.aguasan.app.models

data class GenericApiResponse(
    val message : String?= null
)