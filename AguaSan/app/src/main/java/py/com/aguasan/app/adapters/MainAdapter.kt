package py.com.aguasan.app.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Point
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import py.com.aguasan.app.R

class MainAdapter(val context: Context, var options: List<String>, var callback: (String) -> Unit = {})  : androidx.recyclerview.widget.RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MainViewHolder {
        val windowManager = p0.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val view = LayoutInflater.from(p0.context).inflate(R.layout.main_item_option, p0, false)
        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return options.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val item = options[position]

        when(item) {
            ADD_CLIENT -> {
                holder.image?.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.add_user))
                holder.title?.text = "Nuevo Cliente"
                holder.viewItem?.setOnClickListener { callback(item) }
            }
            FIND_CLIENT -> {
                holder.image?.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.find_user))
                holder.title?.text = "Buscar Cliente"
                holder.viewItem?.setOnClickListener { callback(item) }
            }
            ADD_EVENT -> {
                holder.image?.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.add_calendar))
                holder.title?.text = "Nuevo Evento"
                holder.viewItem?.setOnClickListener { callback(item) }
            }
            FIND_EVENT -> {
                holder.image?.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.add_event))
                holder.title?.text = "Consultar Eventos"
                holder.viewItem?.setOnClickListener { callback(item) }
            }
            ADMIN -> {
                holder.image?.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.admin))
                holder.title?.text = "Administración"
                holder.viewItem?.setOnClickListener { callback(item) }
            }
        }
    }


    class MainViewHolder(v: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var image: ImageView?=null
        var title: TextView?=null
        var viewItem: View?=null

        init {
            image = v.findViewById(R.id.imgOption)
            title = v.findViewById(R.id.titleOption)
            viewItem = v
        }
    }

    companion object {
        const val ADD_CLIENT = "add_client"
        const val FIND_CLIENT = "find_client"
        const val ADD_EVENT = "add_event"
        const val FIND_EVENT = "find_event"
        const val ADMIN = "admin"
    }

}