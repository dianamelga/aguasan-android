package py.com.aguasan.app.adapters

import android.content.Context
import android.graphics.Point
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente

class ClienteAdapter(clientes: List<Cliente>)  : androidx.recyclerview.widget.RecyclerView.Adapter<ClienteAdapter.ClienteViewHolder>() {
    var items: List<Cliente> = emptyList()
    var callback: (Cliente) -> Unit = {}
    var funLlamar: (numero: String) -> Unit= {}
    var funEditar: (Cliente) -> Unit = {}
    var funRegVisita: (Cliente) -> Unit = {}
    var longCallback: (Cliente) -> Unit = {}

    init {
        items = clientes
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ClienteViewHolder {
        val windowManager = p0.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_list_cliente_view, p0, false)
        return ClienteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ClienteViewHolder, position: Int) {
        val item = items[position]
        holder.nombreCliente?.text = item.nombres+" "+item.apellidos
        holder.nroFormulario?.text = item.formulario?.nroFormulario?.toString() ?: "Sin Formulario"
        holder.nroDocumento?.text = item.tipoDocumento?.codTipoDocumento + " "+ item.nroDocumento
        holder.ciudad?.text = item.ciudad?.codCiudad + " - " + item.ciudad?.ciudad
        holder.barrio?.text = item.barrio?.codBarrio + " - " + item.barrio?.barrio
        holder.activo?.text = item.activo
        holder.btVer?.setOnClickListener {
            callback(item)
        }
        holder.btLlamar?.setOnClickListener {
            funLlamar(item.celular.toString())
        }
        holder.btEditar?.setOnClickListener {
            funEditar(item)
        }
        holder.btRegVisita?.setOnClickListener {
            funRegVisita(item)
        }

        holder.itemView.setOnLongClickListener {

            longCallback(item)
            true
        }

    }


    class ClienteViewHolder(v: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var nombreCliente: TextView? = null
        var nroFormulario: TextView? = null
        var viewItem: View? = null
        var nroDocumento: TextView? = null
        var ciudad: TextView? =null
        var barrio: TextView? = null
        var btLlamar: LinearLayout? = null
        var btEditar: LinearLayout? = null
        var btVer: LinearLayout? = null
        var btRegVisita: LinearLayout? = null
        var activo: TextView?=null

        init {
            viewItem = v
            nombreCliente = v.findViewById(R.id.tvNombreApellido)
            nroFormulario = v.findViewById(R.id.tvNroFormulario)
            nroDocumento = v.findViewById(R.id.tvNroDocumento)
            ciudad = v.findViewById(R.id.tvCiudad)
            barrio = v.findViewById(R.id.tvBarrio)
            btLlamar = v.findViewById(R.id.llLlamar)
            btEditar = v.findViewById(R.id.llEditar)
            btVer = v.findViewById(R.id.llVer)
            btRegVisita = v.findViewById(R.id.llVisitar)
            activo = v.findViewById(R.id.tvActivo)
        }
    }
}