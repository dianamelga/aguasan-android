package py.com.aguasan.app.models

import py.com.aguasan.app.persistence.entities.Formulario
import java.io.Serializable

data class Formulario (
    val nroFormulario: Long?=null,
    var cliente: Cliente?=null,
    var bidones: Int?= 0,
    var dispensers: Int?=0,
    var bebederos: Int?=0,
    var fecAlta: String?=null,
    var usrAlta: String?=null,
    var fecUltAct: String?=null,
    var usrUltAct: String?=null
): Serializable {

    fun toEntity(): Formulario {
        return Formulario(
            nroFormulario = nroFormulario?.toInt()!!,
            codCliente = cliente?.codCliente!!,
            bidones = bidones ?: 0,
            bebederos = bebederos ?: 0,
            dispensers = dispensers ?: 0,
            fecAlta = fecAlta ?: "",
            usrAlta = usrAlta ?: "",
            fecUltAct = fecUltAct ?: "",
            usrUltAct = usrUltAct ?: ""
        )
    }
}