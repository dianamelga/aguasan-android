package py.com.aguasan.app.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import py.com.aguasan.app.R
import py.com.aguasan.app.views.fragments.AdministracionFragment

class AdministracionActivity : AguaSanActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_administracion)

        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "Administración"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val fragment = AdministracionFragment.newInstance()

        supportFragmentManager.beginTransaction()
            .add(R.id.fragment, fragment)
            .commit()
    }
}
