package py.com.aguasan.app.views.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import kotlinx.android.synthetic.main.fragment_detalle_cliente.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.CustomExpandableListAdapter
import py.com.aguasan.app.models.Auditoria
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.Formulario
import py.com.aguasan.app.utils.asFormattedDateWithHour

class DetalleClienteFragment : AguaSanFragment() {
    private lateinit var cliente: Cliente
    private var adapter: ExpandableListAdapter? = null
    private var titleList: List<String>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        cliente = arguments?.getSerializable("cliente") as Cliente
        return inflater.inflate(R.layout.fragment_detalle_cliente, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvCodCliente.text = cliente.codCliente.toString()
        tvNroDocumento.text = cliente.tipoDocumento?.codTipoDocumento + "; "+cliente.nroDocumento
        tvNombreApellido.text = cliente.nombres+" "+cliente.apellidos
        tvCelular.text = cliente.celular
        tvTelefono.text = cliente.telefono
        tvCiudad.text = cliente.ciudad?.codCiudad +"; "+cliente.ciudad?.ciudad
        tvBarrio.text = cliente.barrio?.codBarrio + "; "+cliente.barrio?.barrio
        tvDiasVisita.text = cliente.diasVisita.toString()
        tvDireccion.text = cliente.direccion
        tvObservacion.text = cliente.observacion
        tvActivo.text = cliente.activo

        val listData = HashMap<String, List<Any>>()

        val formularios: ArrayList<Formulario> = ArrayList()
        cliente.formulario?.let{
        formularios.add(it)}

        val auditorias: ArrayList<Auditoria> = ArrayList()
        auditorias.add(
            Auditoria(
                fecAlta = cliente.fechaAlta?.asFormattedDateWithHour(), usrAlta = cliente.userAlta,
                fecUltAct = cliente.fechaUltimaAct?.asFormattedDateWithHour(), usrUltAct = cliente.userUltimaAct
            )
        )

        listData["Formulario"] = formularios
        listData["Auditoria"] = auditorias


        titleList = ArrayList(listData.keys)
        adapter = CustomExpandableListAdapter(aguaSanActivity!!, titleList as ArrayList<String>, listData)
        expandableListView!!.setAdapter(adapter)

        expandableListView!!.setOnGroupExpandListener { groupPosition ->
           /* Toast.makeText(
                aguaSanActivity!!,
                (titleList as ArrayList<String>)[groupPosition] + " List Expanded.",
                Toast.LENGTH_SHORT
            ).show()*/
        }

        expandableListView!!.setOnGroupCollapseListener { groupPosition ->
            /*Toast.makeText(
                aguaSanActivity!!,
                (titleList as ArrayList<String>)[groupPosition] + " List Collapsed.",
                Toast.LENGTH_SHORT
            ).show()*/
        }

        expandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
           /* Toast.makeText(
                aguaSanActivity!!,
                "Clicked: " + (titleList as ArrayList<String>)[groupPosition] + " -> " + listData[(titleList as ArrayList<String>)[groupPosition]]!!.get(
                    childPosition
                ),
                Toast.LENGTH_SHORT
            ).show()*/
            false
        }


    }


    companion object {
        fun newInstance(b: Bundle? = null): DetalleClienteFragment {
            val fragment = DetalleClienteFragment()
            b?.let { fragment.arguments = it }
            return fragment
        }
    }
}
