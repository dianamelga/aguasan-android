package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Entity(tableName = "$FORMULARIO_TABLE_NAME")
data class Formulario(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "nro_formulario_local") var nroFormularioLocal: Int=0,
    @ColumnInfo(name = "nro_formulario") var nroFormulario: Int,
    @ColumnInfo(name = "cod_cliente") var codCliente: Int,
    @ColumnInfo(name = "bidones") var bidones: Int,
    @ColumnInfo(name = "bebederos") var bebederos: Int,
    @ColumnInfo(name = "dispensers") var dispensers: Int,
    @ColumnInfo(name = "fec_alta") var fecAlta: String,
    @ColumnInfo(name = "usr_alta") var usrAlta: String,
    @ColumnInfo(name = "fec_ult_act") var fecUltAct: String,
    @ColumnInfo(name = "usr_ult_act") var usrUltAct: String
)
const val FORMULARIO_TABLE_NAME = "formulario"