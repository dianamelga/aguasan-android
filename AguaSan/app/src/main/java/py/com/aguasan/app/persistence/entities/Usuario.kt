package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import py.com.aguasan.app.models.TipoDocumento
import py.com.aguasan.app.models.Usuario

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Entity(tableName = "$USUARIO_TABLE_NAME")
data class Usuario(
    @PrimaryKey
    @ColumnInfo(name = "username") var username: String,
    @ColumnInfo(name = "password") var password: String,
    @ColumnInfo(name = "nombre") var nombre: String,
    @ColumnInfo(name = "cod_tipo_documento") var codTipoDocumento: String,
    @ColumnInfo(name = "nro_documento") var nroDocumento: String,
    @ColumnInfo(name = "email") var email: String,
    @ColumnInfo(name = "es_admin") var esAdmin: String,
    @ColumnInfo(name = "activo") var activo: String,
    @ColumnInfo(name = "fec_alta") var fecAlta: String,
    @ColumnInfo(name = "fec_ult_act") var fecUltAct: String
) {

    fun toDomain(): Usuario {
        return Usuario(
            userName = username,
            password = password,
            nombre = nombre,
            tipoDocumento = TipoDocumento(codTipoDocumento, codTipoDocumento),
            nroDocumento = nroDocumento,
            email = email,
            isAdmin = esAdmin,
            activo = activo,
            fecAlta = fecAlta,
            fecUltAct = fecUltAct
        )
    }
}

const val USUARIO_TABLE_NAME = "usuarios"