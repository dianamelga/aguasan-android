package py.com.aguasan.app.models

import java.io.Serializable

data class MailReq (
    val subject: String,
    val message: String,
    val attachment: String?= null,
    val attachmentName: String? = null
): Serializable