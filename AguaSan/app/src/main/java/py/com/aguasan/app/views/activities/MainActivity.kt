package py.com.aguasan.app.views.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import py.com.aguasan.app.R
import py.com.aguasan.app.database.DBHelper
import py.com.aguasan.app.models.MailReq
import py.com.aguasan.app.models.TokenRegistrationReq
import py.com.aguasan.app.models.TokenRegistrationResponse
import py.com.aguasan.app.utils.*
import py.com.aguasan.app.utils.UtilTools.Companion.cargarDatosGenerales
import py.com.aguasan.app.viewModels.AuthViewModel
import py.com.aguasan.app.viewModels.BarriosViewModel
import py.com.aguasan.app.viewModels.CiudadesViewModel
import py.com.aguasan.app.viewModels.HomeViewModel
import py.com.aguasan.app.views.fragments.MainFragment

class MainActivity : AguaSanActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var barriosViewModel: BarriosViewModel
    private lateinit var ciudadesViewModel: CiudadesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "AguaSan"


        homeViewModel = HomeViewModel(aguasan!!, this@MainActivity)
        barriosViewModel = BarriosViewModel(aguasan!!, this@MainActivity)
        ciudadesViewModel = CiudadesViewModel(aguasan!!, this@MainActivity)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager, intent.extras)
        container.adapter = mSectionsPagerAdapter
        //container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        //tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))

        val header = navView.getHeaderView(0)
        header.headerUsername.text = aguasan!!.session!!.username!!
        header.headerSubtitle.text = aguasan!!.session!!.username + "@aguasan.com.py"


        cargarDatosGenerales(homeViewModel, barriosViewModel, ciudadesViewModel,this@MainActivity)


        registerNotificationToken()


    }



    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
/*
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main2, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_logout -> {
                cerrarSesion()
            }
        }
        return super.onOptionsItemSelected(item)
    }
*/

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_logout -> {
                cerrarSesion()
            }

           /* R.id.nav_pendientes -> {
                Toast.makeText(this@MainActivity, "En desarrollo...", Toast.LENGTH_SHORT).show()
            }*/
            R.id.nav_share -> {
                val mailRequest = MailReq(
                    "${aguasan?.session?.username} - database",
                    "Generado automaticamente.",
                    application.getDataBaseFile()?.toHexString(),
                    aguasan?.session?.username + "_" + DBHelper.DATABASE_NAME
                )
                homeViewModel.sendDatabase(mailRequest).subscribe(object : AguaSanObserver<String>(this@MainActivity) {
                    override fun onNext(t: String) {
                        Log.d("diana", t)
                    }

                })
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun cerrarSesion() {
        val authViewModel = AuthViewModel(aguasan!!, this)
        authViewModel.cerrarSesion().subscribeBy(onComplete = {
            aguasan!!.session!!.enSesion = false
            startActivity(Intent(this, LoginActivity::class.java))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity()
            }else{
                finish()
            }
        })
    }

    private fun registerNotificationToken(){

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful){
                    return@OnCompleteListener
                }

                val token = task.result?.toString() ?: ""
                val tstamp = UtilTools.ISO8601now()
                val deviceId = UtilTools.getDeviceId(this)

                val req = TokenRegistrationReq()
                req.deviceId = deviceId
                req.tokenRegistrationId = token
                req.userName = aguasan?.session?.username


                val vm = HomeViewModel(aguasan!!, this)
                val registervm = vm.registerToken(req)
                registervm.subscribe(object: AguaSanObserver<TokenRegistrationResponse>(this) {
                    override fun onNext(t: TokenRegistrationResponse) {
                        t.tokenRegistrationId?.let{storeRegIdInPref(it)}

                    }

                    override fun onSubscribe(d: Disposable) {
                        willShowError = false
                        super.onSubscribe(d)
                    }

                })
            })
    }

    private fun storeRegIdInPref(token: String) {
        val pref = applicationContext.getSharedPreferences(NotificationUtils.SHARED_PREF, 0)
        val editor = pref.edit()
        editor.putString("regId", token)
        editor.apply()
    }


    inner class SectionsPagerAdapter(fm: FragmentManager, var arg: Bundle?) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            /*return when(position) {
                0 -> ListaClientesFragment.newInstance(arg) {
                    startActivity(Intent(this@MainActivity, ClienteActivity::class.java))
                }
                else ->
            */
/*
            return ListaClientesFragment.newInstance(arg) {
                val i = Intent(this@MainActivity, DetalleClienteActivity::class.java)
                i.putExtra("cliente", it)
                startActivity(i)
            }*/

            return MainFragment.newInstance(arg)

        }

        override fun getCount(): Int {
            return 1
        }


    }
}

