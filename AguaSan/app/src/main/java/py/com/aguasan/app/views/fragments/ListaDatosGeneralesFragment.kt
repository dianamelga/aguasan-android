package py.com.aguasan.app.views.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import kotlinx.android.synthetic.main.fragment_datosgenerales_list.*
import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.DatosGeneralesAdapter
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.viewModels.BarriosViewModel

class ListaDatosGeneralesFragment : AguaSanFragment() {

    private var datos: ArrayList<Any> = ArrayList()
    private var datosCached: ArrayList<Any> = ArrayList()
    private var adapter: DatosGeneralesAdapter?=null
    private var callback: (Any) -> Unit={}
    private var onRefreshCallback: (ArrayList<Any>, ArrayList<Any>, DatosGeneralesAdapter?, () -> Unit)->Unit ={ data, cache, adapter, afterRefresh -> }
    private var deleteCallback: (Any, ArrayList<Any>, ArrayList<Any>, DatosGeneralesAdapter? ) -> Unit= { item, data, cache, adapter ->}
//    private lateinit var getListaDatos: () -> ArrayList<Any>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        datos = arguments?.getSerializable(LISTA) as ArrayList<Any>

       datosCached.addAll(datos)
        return inflater.inflate(R.layout.fragment_datosgenerales_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val barrioVieModel = BarriosViewModel(aguaSan!!, aguaSanActivity!!)
        barrioVieModel.getAllBarriosFromDatabase().subscribe(object: AguaSanObserver<List<Barrio>>(aguaSanActivity!!){
            override fun onNext(t: List<Barrio>) {
                TODO("Not yet implemented")
            }

        })

        adapter = DatosGeneralesAdapter(datos ?: emptyList(), {
            //ON CLICK
            callback(it)
        }, {
            //ON LONG CLICK
            val builder = AlertDialog.Builder(aguaSanActivity!!, R.style.AppTheme_Dialog)
            builder.setTitle("Opciones")

            // add a list
            val options = arrayOf("Eliminar")
            builder.setItems(options) { dialog, which ->
                when (which) {
                    0 -> {
                        deleteCallback(it, datos, datosCached, adapter)
                    }
                }
            }

            builder.setNegativeButton("Cancelar", null)

            // create and show the alert dialog
            val dialog = builder.create()
            dialog.show()
        })
        rvDatosGenerales.adapter = adapter

        slRefresh.setOnRefreshListener {
            onRefreshCallback(datos, datosCached, adapter) {
                slRefresh.isRefreshing = false
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_search, menu)
        (menu.findItem(R.id.action_search).actionView as SearchView).apply {
            this.queryHint= "Buscar"
            setIconifiedByDefault(false)
            setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    p0?.let { s ->
                        val temp = ArrayList<Any>()
                        if (s.isNotBlank() || s.isNotEmpty()) {
                            for (item in datosCached) {
                                when(item) {
                                    is Ciudad -> {
                                        if(item.codCiudad?.contains(s, true) == true ||
                                                    item.ciudad?.contains(s, true) == true) {
                                            temp.add(item)
                                        }
                                    }
                                    is Barrio -> {
                                        if(item.codBarrio?.contains(s, true) == true ||
                                            item.barrio?.contains(s, true) == true) {
                                            temp.add(item)
                                        }
                                    }
                                }
                            }
                        } else {
                            temp.addAll(datosCached)
                        }
                        datos.clear()
                        datos.addAll(temp)
                        adapter?.notifyDataSetChanged()
                    }
                    return false
                }
            })
        }
    }

    companion object {
        const val LISTA = "lista_datos_generales"
        fun newInstance(b: Bundle?, cb: (Any) -> Unit={},
                        onRefreshCallback: (ArrayList<Any>, ArrayList<Any>, DatosGeneralesAdapter?, () -> Unit)-> Unit ={ data, cache, adapter, afterRefresh -> },
                        deleteCallback: (Any, ArrayList<Any>, ArrayList<Any>, DatosGeneralesAdapter? ) -> Unit= { item, data, cache, adapter ->})
        : ListaDatosGeneralesFragment {
            val fragment = ListaDatosGeneralesFragment()
            fragment.callback = cb
            fragment.onRefreshCallback = onRefreshCallback
            fragment.deleteCallback = deleteCallback
            b?.let {
                fragment.arguments = it
            }
            return fragment
        }

//        fun newInstance(b: Bundle?, cb: (Any) -> Unit={},
//                        onRefreshCallback: (ArrayList<Any>, ArrayList<Any>, DatosGeneralesAdapter?, () -> Unit)-> Unit ={ data, cache, adapter, afterRefresh -> },
//                        deleteCallback: (Any, ArrayList<Any>, ArrayList<Any>, DatosGeneralesAdapter? ) -> Unit= { item, data, cache, adapter ->},
//                        getListaDatos: () -> ArrayList<Any>)
//                : ListaDatosGeneralesFragment {
//            val fragment = ListaDatosGeneralesFragment()
//            fragment.callback = cb
//            fragment.onRefreshCallback = onRefreshCallback
//            fragment.deleteCallback = deleteCallback
//            fragment.getListaDatos = getListaDatos
//            b?.let {
//                fragment.arguments = it
//            }
//            return fragment
//        }
    }

}
