package py.com.aguasan.app.viewHolders

import android.content.Context
import android.util.Log
import android.widget.TextView
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.View
import com.mindorks.placeholderview.annotations.expand.Collapse
import com.mindorks.placeholderview.annotations.expand.Expand
import com.mindorks.placeholderview.annotations.expand.Parent
import com.mindorks.placeholderview.annotations.expand.SingleTop
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente

@Parent
@SingleTop
@Layout(R.layout.item_list_cliente_view)
class ClienteHeaderList(val context: Context, val cliente: Cliente) {
    @View(R.id.tvNombreApellido)
    var nombreApellido: TextView?=null

    @View(R.id.tvNroDocumento)
    var nroDocumento: TextView?=null

    @View(R.id.tvCiudad)
    var ciudad: TextView?=null

    @View(R.id.tvBarrio)
    var barrio: TextView?=null

    @Resolve
    private fun onResolve() {
        Log.d(TAG, "onResolve")
        nombreApellido?.text = cliente.nombres+ " "+cliente.apellidos
    }

    @Expand
    private fun onExpand() {
        Log.d(TAG, "onExpand")
    }

    @Collapse
    private fun onCollapse() {
        Log.d(TAG, "onCollapse")
    }


    companion object {
        const val TAG= "ClienteHeaderList"
    }



}