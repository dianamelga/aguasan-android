package py.com.aguasan.app.models

import com.google.gson.Gson
import okhttp3.ResponseBody
import java.io.Serializable

class AguaSanError: Serializable, Exception() {
    var type : String? = ""
    var code : String = ""
    override var message: String? = ""
    var useApiMessage: Boolean = false

    companion object {
        fun create(raw: ResponseBody?) : AguaSanError? {
            var error: AguaSanError? = null
            try {
                error = Gson().fromJson<AguaSanError>(raw!!.string(), AguaSanError::class.java)
                if (!error.useApiMessage) {
                    error?.message = null
                }
            } catch (e: Exception) {
                error = null
            } finally {
                return error
            }
        }
    }
}