package py.com.aguasan.app.adapters

import android.content.Context
import android.graphics.Point
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Evento
import py.com.aguasan.app.utils.asFormattedDate
import py.com.aguasan.app.utils.asFormattedDateWithHour

class EventoAdapter(var items: List<Evento>,
                    var callback: (Evento) -> Unit = {},
                    var longCallback: (Evento) -> Unit = {})  : androidx.recyclerview.widget.RecyclerView.Adapter<EventoAdapter.EventoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventoViewHolder {
        val windowManager = parent.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_evento, parent, false)
        return EventoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: EventoViewHolder, position: Int) {
        val item = items[position]
        holder.nroEvento?.text = item.nroEvento?.toString()
        holder.cliente?.text = item.cliente?.nombres+ " "+item.cliente?.apellidos
        holder.estado?.text = item.estado
        holder.observacion?.text = item.observacion
        holder.fecUltAct?.text = item.fecUltAct?.asFormattedDateWithHour()
        holder.usrUltAct?.text = item.usrUltAct
        holder.fecEvento?.text = item.fecEvento?.asFormattedDateWithHour()
        holder.viewItem?.setOnClickListener { callback(item) }
        holder.viewItem?.setOnLongClickListener { longCallback(item) ; true }
    }



    class EventoViewHolder(v: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var nroEvento: TextView?=null
        var cliente: TextView?=null
        var observacion: TextView?=null
        var fecUltAct: TextView?= null
        var usrUltAct: TextView?=null
        var estado: TextView?=null
        var viewItem: View?=null
        var fecEvento: TextView?=null

        init {
            nroEvento = v.findViewById(R.id.tvNroEvento)
            cliente = v.findViewById(R.id.tvCliente)
            observacion = v.findViewById(R.id.tvObservacion)
            fecUltAct = v.findViewById(R.id.tvFecUltAct)
            usrUltAct = v.findViewById(R.id.tvUsrUltAct)
            estado = v.findViewById(R.id.tvEstado)
            fecEvento = v.findViewById(R.id.tvFecEvento)
            viewItem = v
        }
    }

}