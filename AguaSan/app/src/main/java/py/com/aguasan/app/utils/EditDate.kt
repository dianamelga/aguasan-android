package py.com.aguasan.app.utils

import android.app.DatePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.google.android.material.textfield.TextInputEditText
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList



class EditDate : TextInputEditText, View.OnClickListener {
    private var mLimitFromCurrentDate = false
    var isDateRestrictionActive = false
    var isCurrentPreviousDateDisabled = false
    var isWeekendsDateDisabled = false
    var isHolidaysDisabled = false
    var onError: (Boolean) -> Unit = {}
    private var holidays: ArrayList<String> = ArrayList()

    constructor(context: Context) : super(context) {
        this.setOnClickListener(this)
        this.isFocusableInTouchMode = false
        this.setOnTouchListener { v, event -> false }
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.setOnClickListener(this)
        this.isFocusableInTouchMode = false
        this.setOnTouchListener { v, event -> false }
    }

    fun setLimitFromCurrentDate(limit: Boolean) {
        this.mLimitFromCurrentDate = limit
    }

    private fun showCurrentDatePicker() {
        val c = Calendar.getInstance()
        val yearDefault = c.get(Calendar.YEAR)
        val monthDefault = c.get(Calendar.MONTH)
        val dayDefault = c.get(Calendar.DAY_OF_MONTH)
        val dlg = DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    //val c = Calendar.getInstance()
                    c.set(Calendar.YEAR, year)
                    c.set(Calendar.MONTH, month)
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    this.setText(UtilTools.datesFromMilis(c.timeInMillis, "dd/MM/yyyy"))
                }, yearDefault, monthDefault, dayDefault)

        dlg.show()
    }

    private fun showCertainDatePicker(date: String) {
        //must be dd/MM/yyyy
        val c = Calendar.getInstance()
        val yearDefault = date.substring(6, date.length).toInt()
        val monthDefault = date.substring(3, 5).toInt() - 1
        val dayDefault = date.substring(0, 2).toInt()

        val dlg = DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    //val c = Calendar.getInstance()
                    c.set(Calendar.YEAR, year)
                    c.set(Calendar.MONTH, month)
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    this.setText(UtilTools.datesFromMilis(c.timeInMillis, "dd/MM/yyyy"))
                }, yearDefault, monthDefault, dayDefault)
        dlg.show()


    }

    private fun showDatePickerWithRestriction(date: String){
        val c = Calendar.getInstance()
        val yearDefault = if (date.isBlank()) c.get(Calendar.YEAR) else date.substring(6, date.length).toInt()
        val monthDefault = if (date.isBlank()) c.get(Calendar.MONTH) else date.substring(3, 5).toInt() - 1
        val dayDefault = if (date.isBlank()) c.get(Calendar.DAY_OF_MONTH) else date.substring(0, 2).toInt()
        val dlg = DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    //val c = Calendar.getInstance()
                    c.set(Calendar.YEAR, year)
                    c.set(Calendar.MONTH, month)
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    var isErrorDetected = false
                    if (isWeekendsDateDisabled){
                        if ((c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)){
                            isErrorDetected = true
                            c.set(Calendar.DAY_OF_MONTH, dayOfMonth + 2)
                        }else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                            isErrorDetected = true
                            c.set(Calendar.DAY_OF_MONTH, dayOfMonth + 1)
                        }
                    }

                    if (isHolidaysDisabled && holidays.isNotEmpty()){
                        Collections.sort(holidays, object : Comparator<String>{
                            val f: DateFormat = SimpleDateFormat("dd/MM/yyyy")
                            override fun compare(o1: String?, o2: String?): Int {
                                return f.parse(o1).compareTo(f.parse(o2))
                            }
                        })
                        for (i in holidays){
                            val yearHoliday = i.substring(6, i.length).toInt()
                            val monthHoliday = i.substring(3, 5).toInt() - 1
                            val dayHoliday = i.substring(0, 2).toInt()
                            if (c.get(Calendar.YEAR) == yearHoliday
                                    && c.get(Calendar.MONTH) == monthHoliday
                                    && c.get(Calendar.DAY_OF_MONTH) == dayHoliday){
                                isErrorDetected = true
                                c.set(Calendar.DAY_OF_MONTH, dayOfMonth + 1)
                            }
                        }
                    }

                    if (isErrorDetected){
                        onError(true)
                    }
                    this.setText(UtilTools.datesFromMilis(c.timeInMillis, "dd/MM/yyyy"))
                }, yearDefault, monthDefault, dayDefault)

        if (isCurrentPreviousDateDisabled){
            val cc = Calendar.getInstance()
            cc.set(Calendar.YEAR, c.get(Calendar.YEAR))
            cc.set(Calendar.MONTH, c.get(Calendar.MONTH))
            cc.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH))
            dlg.datePicker.minDate = cc.timeInMillis
        }
        dlg.show()
    }

    override fun onClick(v: View) {
        val date = (v as EditDate).text.toString()
        if (isDateRestrictionActive){
            showDatePickerWithRestriction(date)
        }else{
            if (date.isBlank()) {
                showCurrentDatePicker()
            } else {
                showCertainDatePicker(date)
            }
        }
    }

    fun setHolidays(days: List<String>){
        this.holidays.addAll(days)
    }
}