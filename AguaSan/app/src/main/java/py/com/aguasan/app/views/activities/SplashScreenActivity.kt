package py.com.aguasan.app.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.content.ContextCompat
import android.util.TypedValue
import android.view.ViewGroup
import android.widget.ImageView
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Session
import py.com.aguasan.app.models.Usuario
import py.com.aguasan.app.persistence.entities.Sesion
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.viewModels.AuthViewModel

class SplashScreenActivity : AguaSanActivity() {
    private lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val imageView = ImageView(this)
        val padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64f, resources.displayMetrics).toInt()
        imageView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT)
        imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.aguasan_mejorado))
        imageView.scaleType = ImageView.ScaleType.FIT_CENTER
        imageView.setPadding(padding, padding, padding, padding)
        imageView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
        setContentView(imageView)

        viewModel = AuthViewModel(aguasan!!, this)

        Handler().postDelayed({
            viewModel.getSesionFromDatabase().subscribe(object: AguaSanObserver<List<Sesion>>(this){
                override fun onNext(t: List<Sesion>) {
                    if(t.isEmpty()){
                        //llevo a pantalla de loguin
                        goToLoginActivity()
                    }else {
                        val sesion = t.first()
                            val enSesion = sesion.enSesion == 1
                            val esAdmin:Boolean = sesion.esAdmin == 1

                            aguasan!!.sessionObserver.onNext(Session(sesion.userName,
                                sesion.token, enSesion, esAdmin,
                                sesion.mail))

                            if(enSesion) {
                                goToMainActivity()
                            }else {
                                goToLoginActivity()
                            }
                    }
                }

            })
        }, 2000)

    }

    private fun goToLoginActivity() {
        val i = Intent(this, LoginActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun goToMainActivity() {
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
        finish()
    }
}
