package py.com.aguasan.app.utils

import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import com.google.android.material.textfield.TextInputLayout
import androidx.core.content.FileProvider
import androidx.appcompat.app.AlertDialog
import android.text.InputType
import android.util.Patterns
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.gson.Gson
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.BuildConfig
import py.com.aguasan.app.R
import py.com.aguasan.app.database.DBHelper
import py.com.aguasan.app.models.Moneda
import py.com.aguasan.app.views.activities.AguaSanActivity
import java.io.File
import java.io.FileOutputStream
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import org.apache.commons.io.IOUtils
import kotlin.experimental.and

private val HEX_CHARS = "0123456789ABCDEF"

fun String.toDiasVisita(): List<String> {
    val diasVisitaList = ArrayList<String>()
    return if (this.isNotEmpty()) {
        Gson().fromJson<ArrayList<String>>(this, ArrayList::class.java)
    }else {
        diasVisitaList
    }
}

fun String.hexStringToByteArray() : ByteArray {

    val result = ByteArray(length / 2)

    for (i in 0 until length step 2) {
        val firstIndex = HEX_CHARS.indexOf(this[i])
        val secondIndex = HEX_CHARS.indexOf(this[i + 1])

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

fun ByteArray.toHexString() : String{
    val result = StringBuffer()

    forEach {
        val octet = it.toInt()
        val firstIndex = (octet and 0xF0).ushr(4)
        val secondIndex = octet and 0x0F
        result.append(HEX_CHARS[firstIndex])
        result.append(HEX_CHARS[secondIndex])
    }

    return result.toString()
}

fun Application.getDataBaseFile(): ByteArray? {
    val dbFile = this.getDatabasePath(DBHelper.DATABASE_NAME)
    var attachment: ByteArray? = null
    try {
        val input = java.io.FileInputStream(dbFile)
        attachment = IOUtils.toByteArray(input)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }

    return attachment

}

fun File.toByteArray(): ByteArray? {
    var barray: ByteArray?= null
    try {
        val input = java.io.FileInputStream(this.absolutePath)
        barray = IOUtils.toByteArray(input)
    }catch(e: java.lang.Exception) {
        e.printStackTrace()
    }

    return barray
}

fun BigDecimal.asAmount(code: Long? = 6900L): String {
    val nf = NumberFormat.getNumberInstance(Locale("es", "PY"))
    return if (code ?: 6900L == 6900L) {
        val df = nf as DecimalFormat
        df.applyPattern("###,###")
        df.format(this.toDouble())
    } else {
        val df = nf as DecimalFormat
        df.applyPattern("###,###.##")
        df.format(this.toDouble())
    }
}

fun BigDecimal.asAmountWithCurrency(moneda: Moneda?): String {
    return when (moneda) {
        null -> Moneda.GUARANI.simbolo + " " + this.asAmount(Moneda.GUARANI.codigo)
        else -> moneda.simbolo + " " + this.asAmount(moneda.codigo)
    }
}

fun BigDecimal.asAmountWithCurrencyFormatted(moneda: Moneda?): String {
    return when (moneda) {
        null -> Moneda.GUARANI.simbolo?.toLowerCase()?.capitalize() + ". " + this.asAmount(Moneda.GUARANI.codigo)
        else -> moneda.simbolo?.toLowerCase()?.capitalize() + ". " + this.asAmount(moneda.codigo)
    }
}

fun String.asCardNumber(): String {
    return "•••• •••• •••• " + this.substring(12)
}

fun String.asEmailHidden(): String {
    var arroba = this.indexOf("@")
    val p0 = this.substring(arroba - 2, this.length)
    arroba -= 3
    var finalString = ""
    for (i in 0..arroba) {
        finalString += "•"
    }
    finalString += p0
    return finalString
}

fun String.asCelularHidden(): String {
    return "(••••) ••• •" + this.substring(8)
}

fun String.asFormattedDate(): String {
    val milis = UtilTools.ISO8601toCalendar(this).timeInMillis
    return UtilTools.datesFromMilis(milis, "dd/MM/yyyy")
}
fun String.asFormattedDateWithHour(): String {
    val format = "yyyy-MM-dd HH:mm:ss"
    val milis = UtilTools.ISO8601toCalendar(this.asISO8601FromString(format)).timeInMillis
    return UtilTools.datesFromMilis(milis, "dd/MM/yyyy - HH:mm:ss")
}

fun String.asTimeInMilis(): Long {
    return UtilTools.ISO8601toCalendar(this).timeInMillis
}

fun String.asISO8601(): String {
    return UtilTools.ISO8601fromCalendar(UtilTools.ISO8601toCalendar(this))
}

fun String.asISO8601FromString(): String {
    return UtilTools.getISO8601fromString(this, "dd/MM/yyyy").asISO8601()
}

fun String.asISO8601FromString(format: String): String {
    return UtilTools.getISO8601fromString(this, format).asISO8601()
}

fun String.asFormattedDate(format: String): String {
    val milis = UtilTools.ISO8601toCalendar(this).timeInMillis
    return UtilTools.datesFromMilis(milis, format)
}

fun String.asFormattedDateFromIsoInstant(format: String): String {
    val milis = UtilTools.ISOInstanttoCalendar(this).timeInMillis
    return UtilTools.datesFromMilis(milis, format)
}

fun String.asCoin(): String {
    return "${this.toLowerCase().capitalize()}."
}

fun Bitmap.share(context: Context, subject: String) {
    val fileName = Calendar.getInstance().timeInMillis.toString()
    try {

        val dir = context.cacheDir
        if (dir != null && dir!!.isDirectory()) {
            val childrens = dir!!.list()
            for (i in childrens.indices) {
                File(dir, childrens[i]).delete()
            }
        }

        context.cacheDir.delete()
        val cachePath = File(context.cacheDir, "images")
        cachePath.delete()
        cachePath.mkdirs()
        val stream = FileOutputStream(cachePath.path + "/" + fileName + ".png")
        this.compress(Bitmap.CompressFormat.PNG, 100, stream)
        stream.close()

    } catch (e: Exception) {
        if (BuildConfig.DEBUG) e.printStackTrace()
    }

    try {
        val imagePath = File(context.cacheDir, "images")
        val newFile = File(imagePath, "$fileName.png")
        val contentUri = FileProvider.getUriForFile(
            context,
            BuildConfig.APPLICATION_ID + ".provider", newFile
        )

        if (contentUri != null) {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.setType(context.contentResolver.getType(contentUri))
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
            context.startActivity(Intent.createChooser(shareIntent, "Elige una app"))
        }
    } catch (e: Exception) {
        if (BuildConfig.DEBUG) e.printStackTrace()
    }
}

fun Any.toJSON(): String {
    return Gson().toJson(this)
}

fun EditText.getBigDecimal(moneda: Moneda = Moneda.GUARANI): BigDecimal {
    try {
        return if (moneda.codigo!!.toInt() == 6900) {
            BigDecimal(text!!.toString().replace("[^0-9]".toRegex(), ""))
        } else {
            BigDecimal(text!!.toString().replace("[^0-9\\.]".toRegex(), ""))
        }
    } catch (e: java.lang.Exception) {
        return BigDecimal.ZERO
    }
}

fun EditText.setCurrency(moneda: Moneda) {
    this.inputType = (InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL)
    AguaSan.textWatcher?.let {
        this.removeTextChangedListener(it)
    }
    if (moneda.codigo!!.toInt() == 6900) {
        AguaSan.textWatcher = GuaraniTextWatcher(this)
    } else {
        AguaSan.textWatcher = USDTextWatcher(this)
    }
    this.addTextChangedListener(AguaSan.textWatcher)
}

fun TextInputLayout.isValidAmount(moneda: Moneda, error: String, condicion: Boolean = true): Boolean {
    val amount = this.editText?.getBigDecimal(moneda)?.toLong() ?: 0
    return if (amount > 0 && condicion) {
        this.error = null
        true
    } else {
        this.error = error
        false
    }
}

fun TextInputLayout.isValidAmountWithFocus(moneda: Moneda, error: String): Boolean {

    return if (!this.isValidAmount(moneda, error)) {
        this.requestFocus()
        false
    } else {
        true
    }
}

fun TextInputLayout.isValidText(error: String): Boolean {
    val text = this.editText?.text.toString()
    return if (text.isNotEmpty()) {
        this.error = null
        true
    } else {
        this.error = error
        false
    }
}

fun TextInputLayout.isValidTextWithFocus(error: String): Boolean {
    return if (!this.isValidText(error)) {
        this.requestFocus()
        false
    } else {
        true
    }
}

fun TextInputLayout.isValidEmail(error: String): Boolean {
    val text = this.editText?.text.toString()
    val regex = Patterns.EMAIL_ADDRESS
    return if (text.isNotEmpty() && regex.matcher(text).matches()) {
        this.error = null
        true
    } else {
        this.error = error
        false
    }
}

fun TextInputLayout.isValidEmailWithFocus(error: String): Boolean {

    return if (!this.isValidEmail(error)) {
        this.requestFocus()
        false
    } else {
        true
    }
}


fun TextInputLayout.isOnlyText(error: String, reg: String = "^[a-zA-Z .]+\$"): Boolean {
    val text = this.editText?.text.toString()
    val regex = Regex(reg)
    return if (text.isNotEmpty() && regex.matches(text)) {
        this.error = null
        true
    } else {
        this.error = error
        false
    }
}

fun TextInputLayout.isValidMobileNumber(error: String): Boolean {
    val text = this.editText?.text.toString()
    val regex = Regex(AguaSan.PHONEPATTERN)
    return if (text.isNotEmpty() && regex.matches(text)) {
        this.error = null
        true
    } else {
        this.error = error
        false
    }
}

fun TextInputLayout.isValidMobileNumberWithFocus(error: String): Boolean {

    return if (!this.isValidMobileNumber(error)) {
        this.requestFocus()
        false
    } else {
        true
    }
}

fun TextInputLayout.condition(c: () -> Boolean, error: String): Boolean {
    return if (c()) {
        this.error = null
        true
    } else {
        this.error = error
        false
    }
}

fun TextInputLayout.condition(c: Boolean, error: String): Boolean {
    return if (c) {
        this.error = null
        true
    } else {
        this.error = error
        false
    }
}

fun TextInputLayout.conditionWithFocus(c: Boolean, error: String): Boolean {
    return if (!this.condition(c, error)) {
        this.requestFocus()
        false
    } else {
        true
    }

}

fun View.capture(): Bitmap {
    if (this.layoutParams != null) {
        val params = this.layoutParams
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.MATCH_PARENT
        this.requestLayout()
    } else {
        this.layoutParams =
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }
    if (this.measuredHeight <= 0) {
        this.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        val b = Bitmap.createBitmap(
            this.measuredWidth, this.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val c = Canvas(b)
        this.layout(0, 0, this.measuredWidth, this.measuredHeight)
        this.draw(c)
        return b
    } else {
        val b = Bitmap.createBitmap(
            this.width, this.height,
            Bitmap.Config.RGB_565
        )
        val c = Canvas(b)
        this.layout(this.left, this.top, this.right, this.bottom)
        this.draw(c)
        return b
    }
}

fun AguaSanActivity.logItOut() {
    AlertDialog.Builder(this, R.style.AppTheme_Dialog)
        .setMessage("Desea cerrar la sesión?")
        .setPositiveButton("Sí") { dialog, _ ->
            //hacer algo
        }
        .setNegativeButton("No") { dialog, _ ->
            dialog.dismiss()
        }
        .create().run {
            this.window?.setWindowAnimations(R.style.AppTheme_Dialog)
            this
        }.show()
}