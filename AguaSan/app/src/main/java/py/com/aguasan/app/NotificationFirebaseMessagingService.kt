package py.com.aguasan.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONException
import py.com.aguasan.app.utils.NotificationUtils
import py.com.aguasan.app.views.activities.LoginActivity

class NotificationFirebaseMessagingService : FirebaseMessagingService() {
    private lateinit var notificationUtils: NotificationUtils

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage == null)
            return


        try {
            handleDataMessage(remoteMessage)
        } catch (e: Exception) {
            Log.e("TAG", "Exception: " + e.message)
        }


    }

    private fun handleDataMessage(remoteMessage: RemoteMessage) {
        try {
            val title = remoteMessage.notification?.title ?: ""
            val message = remoteMessage.notification?.body ?: ""
            //val s2fa_token = remoteMessage.data["s2fa_token"]

            val bundle = Bundle()
            bundle.putString(NotificationUtils.TITLE, title)
            bundle.putString(NotificationUtils.MESSAGE, message)
            // bundle.putString(NotificationUtils.TOKEN_2FA, s2fa_token)

            if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
                // app is in foreground, broadcast the push message
                val pushNotification = Intent(NotificationUtils.PUSH_NOTIFICATION)
                pushNotification.putExtra(NotificationUtils.PUSH_NOTIFICATION, bundle)
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

                // play notification sound
                //val notificationUtils = NotificationUtils(applicationContext)
                //notificationUtils.playNotificationSound()
            } else {
                // app is in background, show the notification in notification tray
                val resultIntent = Intent(applicationContext, LoginActivity::class.java)
                resultIntent.putExtra(NotificationUtils.PUSH_NOTIFICATION, bundle)
                showNotificationMessage(applicationContext, title, message, resultIntent)
            }
        } catch (e: JSONException) {
            Log.e("TAG", "Json Exception: " + e.message)
        } catch (e: Exception) {
            Log.e("TAG", "Exception: " + e.message)
        }

    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(context: Context, title: String, message: String, intent: Intent) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils.showNotificationMessage(title, message, intent)
    }

    override fun onNewToken(p0: String) {
        //super.onNewToken(p0)
        Log.d("TAG", "token refresh: $p0")
        p0?.let {
            storeRegIdInPref(it)
        }
    }

    private fun storeRegIdInPref(token: String) {
        val pref = applicationContext.getSharedPreferences(NotificationUtils.SHARED_PREF, 0)
        val editor = pref.edit()
        editor.putString("regId", token)
        editor.apply()
    }
}