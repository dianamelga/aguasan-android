package py.com.aguasan.app.adapters

import android.content.Context
import android.graphics.Point
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Usuario

class UsuarioAdapter(usuarios: List<Usuario>) :
    androidx.recyclerview.widget.RecyclerView.Adapter<UsuarioAdapter.UsuarioViewHolder>() {


    var items: List<Usuario> = emptyList()
    var callback: (Usuario) -> Unit = {}
    var longCallback: (Usuario) -> Unit = {}

    init {
        items = usuarios
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): UsuarioViewHolder {
        val windowManager = p0.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val view =
            LayoutInflater.from(p0.context).inflate(R.layout.item_list_usuario_view, p0, false)
        return UsuarioViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: UsuarioViewHolder, position: Int) {
        val item = items[position]
        holder.tvUsername?.text = item.userName
        holder.tvNombre?.text = item.nombre
        holder.tvActivo?.text = if (item.activo == "S") "ACTIVO" else "INACTIVO"
        holder.viewItem?.setOnClickListener { callback(item) }
        holder.viewItem?.setOnLongClickListener {
            longCallback(item)
            true
        }
    }


    class UsuarioViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var tvUsername: TextView? = null
        var tvNombre: TextView? = null
        var tvActivo: TextView? = null
        var viewItem: View? = null

        init {
            tvUsername = v.findViewById(R.id.tvUsername)
            tvNombre = v.findViewById(R.id.tvNombre)
            tvActivo = v.findViewById(R.id.tvActivo)
            viewItem = v
        }
    }
}