package py.com.aguasan.app.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import io.reactivex.Observable

@Deprecated("Use Room DAOs")
class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    private var mWritableDatabase: SQLiteDatabase = writableDatabase
    private var mReadableDatabase: SQLiteDatabase = readableDatabase
    private val TAG = "SQL"

    override fun onCreate(db: SQLiteDatabase) {
        dropAllTables(db)
        db.execSQL(DBContract.CREATE_TABLE_SESION)
        db.execSQL(DBContract.CREATE_TABLE_CIUDAD)
        db.execSQL(DBContract.CREATE_TABLE_BARRIO)
        db.execSQL(DBContract.CREATE_TABLE_TIPO_DOCUMENTO)
        db.execSQL(DBContract.CREATE_TABLE_USUARIOS)
        db.execSQL(DBContract.CREATE_TABLE_CLIENTES)
        db.execSQL(DBContract.CREATE_TABLE_UBICACIONES_TIPOS)
        db.execSQL(DBContract.CREATE_TABLE_UBICACIONES_CLIENTES)
        db.execSQL(DBContract.CREATE_TABLE_FORMULARIO)
        db.execSQL(DBContract.CREATE_TABLE_SITUACION_VISITA)
        db.execSQL(DBContract.CREATE_TABLE_VISITA)
        db.execSQL(DBContract.CREATE_TABLE_EVENTO)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

        if(oldVersion < newVersion)
            db?.let{onCreate(it)}
    }

    @Synchronized
    fun execSQL(sql: String): Observable<Unit> {
        //Log.d(TAG, sql)
        return Observable.just(mWritableDatabase.execSQL(sql))

    }

    @Synchronized
    fun rawQuery(sql: String): Observable<Cursor> {
       // Log.d(TAG, sql)
        return Observable.just(mReadableDatabase.rawQuery(sql, null))
    }

    private fun dropAllTables(db: SQLiteDatabase) {
        db.execSQL("DROP TABLE IF EXISTS sesion")
        db.execSQL("DROP TABLE IF EXISTS ciudad")
        db.execSQL("DROP TABLE IF EXISTS barrio")
        db.execSQL("DROP TABLE IF EXISTS tipo_documento")
        db.execSQL("DROP TABLE IF EXISTS usuarios")
        db.execSQL("DROP TABLE IF EXISTS clientes")
        db.execSQL("DROP TABLE IF EXISTS ubicaciones_tipos")
        db.execSQL("DROP TABLE IF EXISTS ubicaciones_clientes")
        db.execSQL("DROP TABLE IF EXISTS formulario")
        db.execSQL("DROP TABLE IF EXISTS solicitud_accion")
        db.execSQL("DROP TABLE IF EXISTS situacion_visita")
        db.execSQL("DROP TABLE IF EXISTS visita")
        db.execSQL("DROP TABLE IF EXISTS evento")
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        val DATABASE_VERSION = 4
        val DATABASE_NAME = "aguasan.db"

    }
}