package py.com.aguasan.app.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.StateListDrawable
import android.os.Build
import android.provider.Settings
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.appcompat.app.AlertDialog
import android.view.View
import android.view.ViewGroup
import io.reactivex.rxkotlin.subscribeBy
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.BuildConfig
import py.com.aguasan.app.R
import py.com.aguasan.app.models.*
import py.com.aguasan.app.viewModels.BarriosViewModel
import py.com.aguasan.app.viewModels.CiudadesViewModel
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.viewModels.HomeViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class UtilTools {
    companion object {
        fun ISO8601now(): String {
            return ISO8601fromCalendar(GregorianCalendar.getInstance())
        }

        fun datesFromMilis(milis: Long, format: String?): String {
            var format = format
            if (format == null) {
                format = "yyyy-MM-dd"
            }
            try {
                val d = Date(milis)
                val dateFormat = SimpleDateFormat(format, Locale("es", "PY"))
                return dateFormat.format(d)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return ""
        }

        fun dateToDisplayFormat(date: String, dateFormat: String, displayFormat: String): String {
            val format = SimpleDateFormat(dateFormat)
            val display = SimpleDateFormat(displayFormat)
            try {
                val mDate = format.parse(date)
                return display.format(mDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return ""
        }

        fun ISO8601fromCalendar(calendar: Calendar): String {
            val date = calendar.time
            val formatted = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .format(date)
            return formatted.substring(0, 22) + ":" + formatted.substring(22)
        }

        fun ISO8601toCalendar(iso8601string: String): Calendar {
            val calendar = GregorianCalendar.getInstance()
            try {
                var s = iso8601string.replace("Z", "+00:00")
                s = s.substring(0, 22) + s.substring(23)  // to get rid of the ":"
                val date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s)
                calendar.time = date
                return calendar
            } catch (e: Exception) {
                //throw new ParseException("Invalid length", 0);
                if (BuildConfig.DEBUG) e.printStackTrace()

                return Calendar.getInstance()
            }
        }

        fun ISOInstanttoCalendar(isoInstant: String): Calendar{
            val calendar = GregorianCalendar.getInstance()
            try {
                //var s = isoInstant.replace("Z", "+00:00")
                //s = s.substring(0, 22) + s.substring(23)  // to get rid of the ":"
                val date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS").parse(isoInstant)
                calendar.time = date
                return calendar
            } catch (e: Exception) {
                //throw new ParseException("Invalid length", 0);
                if (BuildConfig.DEBUG) e.printStackTrace()

                return Calendar.getInstance()
            }
        }

        fun getISO8601fromString(date: String, dateFormat: String): String {
            val d = dateToDisplayFormat(date, dateFormat, "yyyy-MM-dd'T'HH:mm:ssZ")
            return d.substring(0, 22) + ":" + d.substring(22)
        }

        fun getStateListDrawable(context: Context, normalDrawable: Int, selectedDrawable: Int): StateListDrawable {
            val listDrawable = StateListDrawable()
            val selected = DrawableCompat.wrap(ContextCompat.getDrawable(context, selectedDrawable)!!).mutate()
            val normal = DrawableCompat.wrap(ContextCompat.getDrawable(context, normalDrawable)!!).mutate()
            listDrawable.addState(intArrayOf(android.R.attr.state_selected), selected)
            listDrawable.addState(intArrayOf(-android.R.attr.state_selected),normal)
            return listDrawable
        }


        fun view2Bitmap(v: View): Bitmap {
            if (v.layoutParams != null) {
                val params = v.layoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = ViewGroup.LayoutParams.MATCH_PARENT
                v.requestLayout()
            } else {
                v.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            }
            if (v.measuredHeight <= 0) {
                v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
                val b = Bitmap.createBitmap(v.measuredWidth, v.measuredHeight,
                    Bitmap.Config.ARGB_8888)
                val c = Canvas(b)
                v.layout(0, 0, v.measuredWidth, v.measuredHeight)
                v.draw(c)
                return b
            } else {
                val b = Bitmap.createBitmap(v.width, v.height,
                    Bitmap.Config.RGB_565)
                val c = Canvas(b)
                v.layout(v.left, v.top, v.right, v.bottom)
                v.draw(c)
                return b
            }
        }

        fun getDeviceId(context: Context): String{

            return Settings.Secure.getString(context.contentResolver,
                Settings.Secure.ANDROID_ID) + "-" + getSuffix(context)
        }

        fun generateRandomString(): String{
            val chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
            var result = ""
            for (i in 0..7) {
                result += chars[Math.floor(Math.random() * chars.length).toInt()]
            }
            return result
        }

        fun getSuffix(context: Context): String{
            val sharedPreferences = context.getSharedPreferences("DEVICE_SUFFIX_CREDENTIALS", Context.MODE_PRIVATE)
            val suffix = sharedPreferences.getString("DEVICE_SUFFIX", "")?: ""
            if (suffix.isEmpty()){
                val editor = sharedPreferences.edit()
                val randomString = generateRandomString()
                editor.putString("DEVICE_SUFFIX", randomString)
                editor.apply()
                return randomString
            }else{
                return suffix
            }
        }

        /*fun getSsign(deviceId: String, tstamp: String): String{
            try {
                val clientSecret = "8c6d6e3a00a94c52b01b9bdc4e78ab53ef711c7abf1d4ee7ed13af86515ff56f"
                return Crypto.SHA256("$deviceId##$tstamp##$clientSecret")
            }catch (e: Exception){
                return ""
            }
        }*/
        fun getAndroidVersionName(): String{
            return when(Build.VERSION.SDK_INT){
                16,17,18 -> "JELLY_BEAN"
                19,20 -> "KITKAT"
                21,22 -> "LOLLIPOP"
                23 -> "MARSHMALLOW"
                24,25 -> "NOUGAT"
                26,27 -> "OREO"
                28 -> "PIE"
                else -> "NOT FOUND"
            }
        }


        fun getGreeting(): String{
            val calendar = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
            return when(calendar){
                in 0..12 -> "Buenos Días"
                in 12..18 -> "Buenas Tardes"
                else -> "Buenas Noches"
            }
        }

        fun showDialog(context: Context?, message: String?, cb: () -> Unit={}){
            context?.let {
                AlertDialog.Builder(context, R.style.AppTheme_Dialog)
                    .setMessage(message ?: context.getString(R.string.generic_error_msg))
                    .setNegativeButton("Aceptar") { dialog, _ ->
                        dialog.dismiss()
                        cb()
                    }
                    .create().run {
                        this.window?.setWindowAnimations(R.style.AppTheme_Dialog)
                        this
                    }.show()
            }
        }

        fun cargarDatosGenerales(homeViewModel: HomeViewModel,
                                 barriosViewModel: BarriosViewModel,
                                 ciudadesViewModel: CiudadesViewModel,
                                 context: Context) {
            ciudadesViewModel.getCiudadesFromDatabase().subscribe(object: AguaSanObserver<List<Ciudad>>(context) {
                override fun onNext(ciudades: List<Ciudad>) {
                    if (ciudades.isEmpty()) {
                        ciudadesViewModel.listCiudades().subscribe(object : AguaSanObserver<List<Ciudad>>(context) {
                            override fun onNext(t: List<Ciudad>) {
                                ciudadesViewModel.insertAllCiudadesToDatabase(t).subscribe()
                            }

                            override fun onError(e: Throwable) {
                                super.onError(e)
                                barriosViewModel.deleteAllFromDatabase().subscribeBy(onComplete={
                                    ciudadesViewModel.deleteAllFromDatabase().subscribe()
                                })
                            }
                        })

                        barriosViewModel.listBarrios().subscribe(object : AguaSanObserver<List<Barrio>>(context) {
                            override fun onNext(t: List<Barrio>) {
                                barriosViewModel.insertAllBarriosToDatabase(t).subscribe()
                            }

                            override fun onError(e: Throwable) {
                                super.onError(e)
                                barriosViewModel.deleteAllFromDatabase().subscribeBy(onComplete={
                                    ciudadesViewModel.deleteAllFromDatabase().subscribe()
                                })

                            }
                        })
                    }
                }

            })


            homeViewModel.getTiposUbicacionesFromDatabase().subscribe(object: AguaSanObserver<List<TipoUbicacion>>(context) {
                override fun onNext(tiposUbicaciones: List<TipoUbicacion>) {
                    if(tiposUbicaciones.isEmpty()) {
                        homeViewModel.getUbicacionesTipos().subscribe(object: AguaSanObserver<List<TipoUbicacion>> (context) {
                            override fun onNext(t: List<TipoUbicacion>) {
                                homeViewModel.insertUbicacionesTiposToDatabase(t).subscribe()
                            }

                            override fun onError(e: Throwable) {
                                super.onError(e)
                                homeViewModel.deleteUbicacionesTiposFromDatabase().subscribe()
                            }

                        })
                    }
                }

            })


            homeViewModel.getSituacionesVisitasFromDatabase().subscribe(object: AguaSanObserver<List<SituacionVisita>>(context){
                override fun onNext(situacionesVisita: List<SituacionVisita>) {
                    if(situacionesVisita.isEmpty()) {
                        homeViewModel.getSituacionesVisitas().subscribe(object: AguaSanObserver<List<SituacionVisita>> (context) {
                            override fun onNext(t: List<SituacionVisita>) {
                                homeViewModel.insertSituacionesVisitasToDatabase(t).subscribe()
                            }

                            override fun onError(e: Throwable) {
                                super.onError(e)
                                homeViewModel.deleteSituacionesVisitasFromDatabase().subscribe()
                            }

                        })
                    }
                }

            })


            homeViewModel.getTiposDocumentoFromDatabase().subscribe(object: AguaSanObserver<List<TipoDocumento>>(context) {
                override fun onNext(tiposDocumentos: List<TipoDocumento>) {
                    if(tiposDocumentos.isEmpty()) {
                        homeViewModel.getTiposDocumento().subscribe(object: AguaSanObserver<List<TipoDocumento>> (context) {
                            override fun onNext(t: List<TipoDocumento>) {
                                homeViewModel.insertTiposDocumentosToDatabase(t).subscribe()
                            }

                            override fun onError(e: Throwable) {
                                super.onError(e)
                                homeViewModel.deleteTiposDocumentosFromDatabase().subscribe()
                            }

                        })
                    }
                }

            })


        }
    }
}