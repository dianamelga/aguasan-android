package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.AuthRepository
import py.com.aguasan.app.dataModels.HomeRepository
import py.com.aguasan.app.models.*
import py.com.aguasan.app.persistence.entities.UbicacionCliente
import java.util.concurrent.TimeUnit

class HomeViewModel(var application: AguaSan, context: Context?) : AguaSanViewModel(context) {

    fun registerToken(tokenRegistrationReq: TokenRegistrationReq): Observable<TokenRegistrationResponse> {
        return HomeRepository(application).registerToken(tokenRegistrationReq)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(1)
    }

    fun sendReports(reporteReq: ReporteReq): Observable<GenericApiResponse> {
        return HomeRepository(application).sendReports(reporteReq)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(1)
    }

    fun getSituacionesVisitas():Observable<List<SituacionVisita>> {
        return HomeRepository(application).getSituacionesVisitas()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(3)
    }
    fun sendDatabase(mailRequest: MailReq) : Observable<String> {
        return HomeRepository(application).sendDatabase(mailRequest)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }

    fun sendMail(mailRequest: MailReq) : Observable<String> {
        return HomeRepository(application).sendMail(mailRequest)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(1)
    }



    fun getTiposDocumento(): Observable<List<TipoDocumento>> {
         return HomeRepository(application).getTiposDocumento()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
             .retry(3)
    }



    fun getUbicacionesTipos(): Observable<List<TipoUbicacion>> {
        return HomeRepository(application).getUbicacionesTipos()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}
            .retry(3)
    }

    fun insertUbicacionesTiposToDatabase(ubicacionesTipos: List<TipoUbicacion>): Completable {
        return HomeRepository(application).insertUbicacionesTiposToDatabase(ubicacionesTipos)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}
    }

    fun deleteUbicacionesTiposFromDatabase(): Completable {
        return HomeRepository(application).deleteUbicacionesTiposFromDatabase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}
    }

    fun insertSituacionesVisitasToDatabase(situaciones: List<SituacionVisita>): Completable {
        return HomeRepository(application).insertSituacionesVisitasToDatabase(situaciones)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}
    }

    fun deleteSituacionesVisitasFromDatabase(): Completable {
        return HomeRepository(application).deleteSituacionesVisitasFromDatabase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}
    }

    fun insertTiposDocumentosToDatabase(tiposDocumentos: List<TipoDocumento>): Completable {
        return HomeRepository(application).insertTiposDocumentosToDatabase(tiposDocumentos)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}
    }

    fun deleteTiposDocumentosFromDatabase(): Completable {
        return HomeRepository(application).deleteTiposDocumentosFromDatabase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}
    }

    fun getSituacionesVisitasFromDatabase(): Observable<List<SituacionVisita>> {
        return HomeRepository(application).getSituacionesVisitasFromDatabase()
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}

    }
    fun getTiposDocumentoFromDatabase(): Observable<List<TipoDocumento>> {
        return HomeRepository(application).getTiposDocumentoFromDatabase()
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}

    }

    fun getTiposUbicacionesFromDatabase(): Observable<List<TipoUbicacion>> {
        return HomeRepository(application).getTiposUbicacionesFromDatabase()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace()}

    }
}