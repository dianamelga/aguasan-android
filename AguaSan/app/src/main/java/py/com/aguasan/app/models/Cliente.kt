package py.com.aguasan.app.models
import py.com.aguasan.app.utils.toJSON
import java.io.Serializable

data class Cliente(
    var codCliente: Int? = null,
    var nombres: String? = null,
    var apellidos: String? = null,
    var celular: String? = null,
    var telefono: String? = null,
    var tipoDocumento: TipoDocumento? = null,
    var nroDocumento: String? = null,
    var ciudad: Ciudad? = null,
    var barrio: Barrio? = null,
    var direccion: String? = null,
    var observacion: String? = null,
    var diasVisita: List<String>? = listOf(),
    var formulario: Formulario?= null,
    var ubicaciones: List<Ubicacion>?= listOf(),
    var visitas: List<Visita>?=null,
    var formularios: List<Formulario>?= listOf(),
    var activo: String?= null,
    var fechaAlta: String? = null,
    var userAlta: String? = null,
    var fechaUltimaAct: String? = null,
    var userUltimaAct: String? = null
): Serializable {
    override fun toString(): String {
        return nombres + " " + apellidos + " - " + tipoDocumento?.codTipoDocumento +
            " "+ nroDocumento ?: super.toString()
    }

    override fun equals(other: Any?): Boolean {
        return when(other) {
            is Cliente -> {
                codCliente?.equals(other.codCliente) ?: false
            }
            else -> false
        }

    }

    fun toEntity(): py.com.aguasan.app.persistence.entities.Cliente {
        return py.com.aguasan.app.persistence.entities.Cliente(
            codCliente = this.codCliente!!,
            nombres = this.nombres ?: "",
            apellidos = this.apellidos ?: "",
            celular = this.celular ?: "",
            telefono = this.telefono,
            codTipoDocumento = this.tipoDocumento?.codTipoDocumento!!,
            nroDocumento = this.nroDocumento ?: "",
            direccion = this.direccion ?: "",
            observacion = this.observacion ?: "",
            codCiudad = this.ciudad?.codCiudad!!,
            codBarrio = this.barrio?.codBarrio!!,
            diasVisita = this.diasVisita?.toJSON()!!,
            activo = this.activo!!,
            fecAlta = this.fechaAlta ?: "",
            usrAlta = this.userAlta ?: "",
            fecUltAct = this.fechaUltimaAct ?: "",
            usrUltAct = this.userUltimaAct ?: ""
        )
    }
}
