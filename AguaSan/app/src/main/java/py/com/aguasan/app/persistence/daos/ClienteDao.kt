package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import py.com.aguasan.app.persistence.entities.Cliente

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface ClienteDao {

    @Query("SELECT * FROM clientes WHERE cod_cliente = :codCliente")
    fun getCliente(codCliente: Int): Observable<List<Cliente>>

    @Query("SELECT * FROM clientes")
    fun getAll(): Observable<List<Cliente>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(clientes: List<Cliente>): Single<List<Long>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewCliente(cliente: Cliente): Completable

    @Update
    fun updateCliente(cliente: Cliente): Completable

    @Delete
    fun deleteCliente(cliente: Cliente): Completable

    @Query("DELETE FROM clientes")
    fun deleteAll(): Completable
}