package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import py.com.aguasan.app.persistence.entities.Formulario

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface FormularioDao {

    @Query("SELECT * FROM formulario WHERE cod_cliente = :codCliente ORDER BY nro_formulario DESC")
    fun getFormularioFromCliente(codCliente: Int): Observable<List<Formulario>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(formularios: List<Formulario>): Single<List<Long>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(formulario: Formulario): Completable

    @Update
    fun updateFormulario(formulario: Formulario): Completable


}