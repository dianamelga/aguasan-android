package py.com.aguasan.app.models

import java.io.Serializable

data class Ciudad(
    val codCiudad: String? = null,
    val ciudad: String? = null
): Serializable {
    override fun toString(): String {
        return ciudad ?: super.toString()
    }

    override fun equals(other: Any?): Boolean {
        return when(other) {
            is Ciudad -> {
                codCiudad.equals(other.codCiudad)
            }
            else -> false
        }

    }

    fun toEntity(): py.com.aguasan.app.persistence.entities.Ciudad {
        return py.com.aguasan.app.persistence.entities.Ciudad(
            codCiudad = codCiudad!!,
            ciudad = ciudad ?: ""
        )
    }
}