package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import py.com.aguasan.app.persistence.entities.Visita

/**
 * Created by Diana Melgarejo on 7/26/20.
 */
@Dao
interface VisitaDao {

    @Query("SELECT * FROM visita WHERE cod_cliente = :codCliente ORDER BY nro_visita_local DESC")
    fun getVisitasFromCliente(codCliente: Int): Observable<List<Visita>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(visita: Visita): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(visitas: List<Visita>): Single<List<Long>>

    @Delete
    fun deleteAll(visitas: List<Visita>): Completable

    @Query("DELETE FROM visita")
    fun deleteAll(): Completable
}