package py.com.aguasan.app.models

data class ItemAdministracion(
    var icon: Int? = null,
    var title: String? = null,
    var callback: () -> Unit={}
)