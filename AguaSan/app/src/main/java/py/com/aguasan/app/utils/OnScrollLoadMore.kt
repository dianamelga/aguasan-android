package py.com.aguasan.app.utils

class OnScrollLoadMore(val layoutManager: androidx.recyclerview.widget.RecyclerView.LayoutManager, val listener: LoadMoreListener): androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

    var load: Boolean = false

    override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        load = true
    }

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy > 0){
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            var pastVisiblesItems =
                when (layoutManager){
                    is androidx.recyclerview.widget.GridLayoutManager ->  layoutManager.findFirstVisibleItemPosition()
                    is androidx.recyclerview.widget.LinearLayoutManager -> layoutManager.findFirstVisibleItemPosition()
                    else -> 0
                }
            if (load){
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount){
                    load = false
                    listener.loadMoreElements()
                }
            }
        }
    }
}

interface LoadMoreListener {
    fun loadMoreElements()
}