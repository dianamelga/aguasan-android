package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Entity(tableName = "$CIUDAD_TABLE_NAME")
data class Ciudad(
    @PrimaryKey
    @ColumnInfo(name = "cod_ciudad") var codCiudad: String,
    @ColumnInfo(name = "ciudad") var ciudad: String
) {
    companion object {
        fun Ciudad.toDomain() : py.com.aguasan.app.models.Ciudad {
            return py.com.aguasan.app.models.Ciudad(
                codCiudad = this.codCiudad,
                ciudad = this.ciudad
            )
        }
    }
}

const val CIUDAD_TABLE_NAME = "ciudad"