package py.com.aguasan.app.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import py.com.aguasan.app.R
import android.content.DialogInterface
import android.widget.Toast
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AlertDialog
import android.util.Log


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap

    private var latitude: Double?=null
    private var longitude: Double?=null


    override fun onMarkerClick(p0: Marker?) =  false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_maps)

        //setSupportActionBar(toolbar as Toolbar)

        //supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        latitude = intent?.extras?.getDouble(LATITUDE)
        longitude = intent?.extras?.getDouble(LONGITUDE)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true

        val position = LatLng(latitude!!, longitude!!)
        mMap.addMarker(MarkerOptions().position(position).title("$latitude/$longitude"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position))


           /* val builder = AlertDialog.Builder(this)
            builder.setMessage("Open Google Maps?")
                .setCancelable(true)
                .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, id ->*/
                    val latitude = latitude.toString()
                    val longitude = longitude.toString()
                    val gmmIntentUri = Uri.parse("google.navigation:q=$latitude,$longitude")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")

                    try {
                        if (mapIntent.resolveActivity(packageManager) != null) {
                            startActivity(mapIntent)
                            finish()
                        }
                    } catch (e: NullPointerException) {
                        Log.e("Maps", "onClick: NullPointerException: Couldn't open map." + e.message)
                        Toast.makeText(this, "Couldn't open map", Toast.LENGTH_SHORT).show()
                    }
               /* })
                .setNegativeButton("No", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
            val alert = builder.create()
            alert.show()*/



    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                android.R.id.home -> {
                    super.onBackPressed()
                }
                else -> {
                }
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val LONGITUDE = "longitude"
        const val LATITUDE = "latitude"
    }
}
