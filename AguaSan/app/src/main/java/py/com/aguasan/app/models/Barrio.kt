package py.com.aguasan.app.models

import java.io.Serializable

data class Barrio (
    val codBarrio: String? = null,
    val barrio: String? = null,
    val ciudad: Ciudad? = null
): Serializable {

    override fun toString(): String {
        return barrio ?: super.toString()
    }

    override fun equals(other: Any?): Boolean {
        return when(other) {
            is Barrio -> {
                codBarrio == other.codBarrio && (ciudad?.codCiudad == other.ciudad?.codCiudad)
            }
            else -> false
        }

    }

    fun toEntity() : py.com.aguasan.app.persistence.entities.Barrio {
        return py.com.aguasan.app.persistence.entities.Barrio(
            codBarrio = codBarrio!!,
            barrio = barrio!!,
            codCiudad = ciudad?.codCiudad!!
        )
    }
}

