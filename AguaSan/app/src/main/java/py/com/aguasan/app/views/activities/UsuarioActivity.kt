package py.com.aguasan.app.views.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_usuario.*
import py.com.aguasan.app.R
import py.com.aguasan.app.views.fragments.ListaUsuariosFragment
import py.com.aguasan.app.views.fragments.NuevoUsuarioFragment

class UsuarioActivity : AguaSanActivity() {
    private var modoEdicion: Boolean = false
    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_usuario)

        supportActionBar?.title = "Usuarios"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)





        val fragment = ListaUsuariosFragment.newInstance {
            modoEdicion = true
            val bundle = Bundle()
            bundle.putSerializable(NuevoUsuarioFragment.USUARIO_SELECTED, it)

            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, NuevoUsuarioFragment.newInstance(bundle))
                .addToBackStack(null)
                .commit()

        }


        btAddUsuario.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, NuevoUsuarioFragment.newInstance())
                .addToBackStack(null)
                .commit()

        }

        supportFragmentManager.beginTransaction()
            .add(R.id.fragment, fragment)
            .commit()



        supportFragmentManager.addOnBackStackChangedListener {
            val current = supportFragmentManager.backStackEntryCount + 1
            when (current) {
                1 -> {
                    modoEdicion = false
                    supportActionBar?.title = "Usuarios"
                    btAddUsuario?.visibility = View.VISIBLE
                }
                2 -> {
                    if(!modoEdicion) {
                        supportActionBar?.title = "Nuevo Usuario"
                    }else{
                        supportActionBar?.title = "Modificar Usuario"
                    }

                    btAddUsuario?.visibility = View.GONE
                }
            }
        }

    }


}
