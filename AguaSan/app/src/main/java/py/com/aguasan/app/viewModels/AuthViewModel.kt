package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.AuthRepository
import py.com.aguasan.app.dataModels.HomeRepository
import py.com.aguasan.app.models.LoginReq
import py.com.aguasan.app.models.LoginResponse
import py.com.aguasan.app.models.SerialResponse
import py.com.aguasan.app.models.TokenRegistrationReq
import py.com.aguasan.app.persistence.entities.Sesion
import java.util.concurrent.TimeUnit


class AuthViewModel(var application: AguaSan, context: Context?) : AguaSanViewModel(context)  {


    fun login(request: LoginReq): Observable<LoginResponse> {
        return AuthRepository(application).login(request)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }
    fun serial(request: LoginReq): Observable<SerialResponse> {
        return AuthRepository(application).serial(request)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }

    fun getSesionFromDatabase(): Observable<List<Sesion>> {
        return AuthRepository(application).getAllSesionFromDatabase()
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun newSesion(sesion: Sesion): Completable {
        return AuthRepository(application).newSesion(sesion)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun updateSesion(sesion: Sesion): Completable {
        return AuthRepository(application).updateSesion(sesion)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun cerrarSesion(): Completable {
        return AuthRepository(application).cerrarSesion()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


}