package py.com.aguasan.app.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.views.activities.AguaSanActivity

open class AguaSanFragment: Fragment() {
    var aguaSanActivity: AguaSanActivity? = null
    var aguaSan : AguaSan? = null
    var resourceId: Int? = 0
    //var funcionalidad : String = Funcionalidad.EB_POSICION_CONSOLIDADA
    //var accion : String = Accion.CONSULTA

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        aguaSanActivity = activity as AguaSanActivity
        aguaSan = activity?.application as AguaSan
        arguments?.let {
            //funcionalidad = it.getString(KEY_FUNC)?: Funcionalidad.EB_POSICION_CONSOLIDADA
            //accion = it.getString(KEY_ACCION)?: Accion.CONSULTA
        }

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    open fun isFormValid(): Boolean {
        return true
    }

    companion object {
        const val KEY_FUNC = "k_funcionalidad"
        const val KEY_ACCION = "k_accion"
    }

}