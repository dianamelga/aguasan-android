package py.com.aguasan.app.models

import java.io.Serializable

data class SerialResponse(
    val serial: String?= null,
    val token: String?= null,
    val isAdmin: Boolean?=null,
    val mail: String?=null,
    val tokenRegistrationId: String?=null
): Serializable