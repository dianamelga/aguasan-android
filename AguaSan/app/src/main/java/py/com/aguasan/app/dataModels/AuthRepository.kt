package py.com.aguasan.app.dataModels

import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.LoginReq
import py.com.aguasan.app.models.LoginResponse
import py.com.aguasan.app.models.SerialResponse
import py.com.aguasan.app.models.TokenRegistrationReq
import py.com.aguasan.app.persistence.entities.Sesion

class AuthRepository (var application : AguaSan) {

    private val sesionDAO = application.database!!.sesionDao()

    fun login(request: LoginReq): Observable<LoginResponse> {
        return application.apiAuth!!.login(request)
    }

    fun serial(request: LoginReq): Observable<SerialResponse> {
        return application.apiAuth!!.serial(request)
    }

    fun getAllSesionFromDatabase(): Observable<List<Sesion>> {
        return sesionDAO.getAll()
    }

    fun newSesion(sesion: Sesion): Completable {
        return sesionDAO.insertSesion(sesion)
    }

    fun updateSesion(sesion:Sesion): Completable {
        return sesionDAO.updateSesion(sesion)
    }

    fun cerrarSesion(): Completable {
        return sesionDAO.cerrarSesion()
    }

}