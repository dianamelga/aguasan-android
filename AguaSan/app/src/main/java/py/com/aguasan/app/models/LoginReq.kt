package py.com.aguasan.app.models

import java.io.Serializable

data class LoginReq (
    val userName: String?=null,
    val password: String?=null,
    val serialInstal: Int?= null,
    val deviceId: String?="",
    val tokenRegistrationId: String?=""
): Serializable