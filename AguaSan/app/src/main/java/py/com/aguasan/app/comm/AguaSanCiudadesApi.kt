package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.models.GenericApiResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface AguaSanCiudadesApi {
    @GET("ciudades")
    fun listCiudades(
    ): Observable<List<Ciudad>>

    @POST("ciudades")
    fun createCiudad(
        @Body ciudad: Ciudad
    ): Observable<Ciudad>

    @PUT("ciudades")
    fun updateCiudad(
        @Body ciudad: Ciudad
    ): Observable<Ciudad>

    @DELETE("ciudades/{codCiudad}")
    fun deleteCiudad(
        @Path("codCiudad") codCiudad: String
    ): Observable<GenericApiResponse>

    @GET("ciudades/{codCiudad}")
    fun getCiudad(
        @Path("codCiudad") codCiudad: String
    ): Observable<Ciudad>


    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanCiudadesApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanCiudadesApi::class.java)
        }
    }
}