package py.com.aguasan.app.dataModels

import com.google.gson.Gson
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function6
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.asFlow
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.*
import py.com.aguasan.app.persistence.entities.UbicacionCliente
import py.com.aguasan.app.utils.asFormattedDateWithHour
import py.com.aguasan.app.utils.toDiasVisita
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors


class ClientesRepository(var application: AguaSan) {
    val clienteDAO = application.database!!.clienteDao()
    val barrioDAO = application.database!!.barrioDao()
    val ciudadDAO = application.database!!.ciudadDao()
    val tipoDocumentoDAO = application.database!!.tipoDocumentoDao()
    val ubicacionDAO = application.database!!.ubicacionClienteDao()
    val tipoUbicacionDAO = application.database!!.tipoUbicacionDao()
    val formularioDAO = application.database!!.formularioDao()
    val visitaDAO = application.database!!.visitaDao()
    val situacionVisitaDAO = application.database!!.situacionVisitaDao()

    fun getClientes(request: ClientesFilteredRequest): Observable<List<Cliente>> {
        return application.apiClientes!!.getClientes(request)
    }

    fun postCliente(cliente: Cliente): Observable<Cliente> {
        return application.apiClientes!!.postCliente(cliente)
    }

    fun editCliente(cliente: Cliente): Observable<Cliente> {
        return application.apiClientes!!.editCliente(cliente)
    }

    fun getClientesFromDatabase(codCliente: Int?=null): Observable<List<Cliente>> {
        val ubicacionesCliente: ArrayList<Ubicacion> = ArrayList()
        var diasVisitaList = ArrayList<String>()
        val visitasCliente: ArrayList<Visita> = ArrayList()
        val clientes: ArrayList<Cliente> = ArrayList()
        var formulario: Formulario?=null

        val clientesListObservable: Observable<List<py.com.aguasan.app.persistence.entities.Cliente>> = if(codCliente == null) {
            clienteDAO.getAll()
        }else {
            clienteDAO.getCliente(codCliente)
        }

        return clientesListObservable.map{ it.map {c -> c.toDomain()  }}

    }

    fun getClienteAllData(cliente: py.com.aguasan.app.persistence.entities.Cliente): Observable<Cliente> {
        val ubicacionesCliente: ArrayList<Ubicacion> = ArrayList()
        var diasVisitaList = cliente.diasVisita.toDiasVisita()
        val visitasCliente: ArrayList<Visita> = ArrayList()
        var formulario: Formulario?=null


                return Observable.combineLatest(
                    barrioDAO.getBarrioFromCliente(cliente.codCliente).subscribeOn(Schedulers.io()),
                    ciudadDAO.getCiudadFromCliente(cliente.codCliente).subscribeOn(Schedulers.io()),
                    tipoDocumentoDAO.getTipoDocumentoFromCliente(cliente.codCliente).subscribeOn(Schedulers.io()),
                    ubicacionDAO.getUbicacionesFromCliente(cliente.codCliente).subscribeOn(Schedulers.io()),
                    formularioDAO.getFormularioFromCliente(cliente.codCliente).subscribeOn(Schedulers.io()),
                    visitaDAO.getVisitasFromCliente(cliente.codCliente).subscribeOn(Schedulers.io()),
                    Function6 { barrio: py.com.aguasan.app.persistence.entities.Barrio,
                                ciudad: py.com.aguasan.app.persistence.entities.Ciudad,
                                tipoDocumento: py.com.aguasan.app.persistence.entities.TipoDocumento,
                                ubicaciones: List<UbicacionCliente>,
                                formularios: List<py.com.aguasan.app.persistence.entities.Formulario>,
                                visitas: List<py.com.aguasan.app.persistence.entities.Visita> ->


                        ubicaciones.map { ubicacion ->
                            tipoUbicacionDAO.getTipoUbicacion(ubicacion.codTipoUbicacion)
                                .subscribeOn(Schedulers.io())
                                .map { tipoUbicacion ->
                                    val ubi = Ubicacion(
                                        codUbicacionLocal = ubicacion.codUbicacionLocal,
                                        codUbicacion = ubicacion.codUbicacion,
                                        pathFoto = ubicacion.pathFoto,
                                        tipoUbicacion = TipoUbicacion(
                                            tipoUbicacion.codTipoUbicacion,
                                            tipoUbicacion.tipoUbicacion
                                        ),
                                        latitud = ubicacion.latitud,
                                        longitud = ubicacion.longitud
                                    )

                                    ubicacionesCliente.add(ubi)
                                }
                        }


                        if(formularios.isNotEmpty()) {
                            val f = formularios.first()
                            formulario = Formulario(
                                nroFormulario = f.nroFormulario.toLong(),
                                bidones = f.bidones,
                                dispensers = f.dispensers,
                                bebederos = f.bebederos,
                                fecAlta = f.fecAlta,
                                usrAlta = f.usrAlta,
                                fecUltAct = f.fecUltAct,
                                usrUltAct = f.usrUltAct
                            )
                        }

                        visitas.map { visita ->
                            situacionVisitaDAO.getSituacionVisita(visita.codSituacion)
                                .subscribeOn(Schedulers.io())
                                .map { situacionVisita ->
                                    val v = Visita(
                                        nroVisita = visita.nroVisita.toLong(),
                                        situacion = SituacionVisita(
                                            situacionVisita.codSituacion,
                                            situacionVisita.situacion
                                        ),
                                        bidonesCanje = visita.bidonesCanje,
                                        bidones = visita.bidones,
                                        fecAlta = visita.fecAlta,
                                        usrAlta = visita.usrAlta,
                                        fecUltAct = visita.fecUltAct,
                                        usrUltAct = visita.usrUltAct
                                    )
                                    visitasCliente.add(v)
                                }
                        }



                            Cliente(
                                formulario = formulario,
                                codCliente = cliente.codCliente,
                                nombres = cliente.nombres,
                                apellidos = cliente.apellidos,
                                tipoDocumento = TipoDocumento(
                                    tipoDocumento.codTipoDocumento,
                                    tipoDocumento.tipoDocumento
                                ),
                                nroDocumento = cliente.nroDocumento,
                                ciudad = Ciudad(
                                    codCiudad = ciudad.codCiudad,
                                    ciudad = ciudad.ciudad
                                ),
                                barrio = Barrio(
                                    codBarrio = barrio.codBarrio,
                                    barrio = barrio.barrio,
                                    ciudad = Ciudad(
                                        codCiudad = ciudad.codCiudad,
                                        ciudad = ciudad.ciudad
                                    )
                                ),
                                celular = cliente.celular,
                                telefono = cliente.telefono,
                                direccion = cliente.direccion,
                                observacion = cliente.observacion,
                                diasVisita = diasVisitaList,
                                ubicaciones = ubicacionesCliente,
                                visitas = visitasCliente,
                                activo = cliente.activo,
                                fechaAlta = cliente.fecAlta,
                                userAlta = cliente.usrAlta,
                                fechaUltimaAct = cliente.fecUltAct,
                                userUltimaAct = cliente.usrUltAct

                            )
                    }

                )

    }

    fun getUbicacionesFromDatabase(cliente: Cliente): Observable<ArrayList<Ubicacion>> {
        val list: ArrayList<Ubicacion> = ArrayList()
        return ubicacionDAO.getUbicacionesFromCliente(cliente.codCliente!!).flatMap { ubicaciones ->

            ubicaciones.map { ubi ->
                list.add(
                    Ubicacion(
                        codUbicacionLocal = ubi.codUbicacionLocal,
                    codUbicacion = ubi.codUbicacion,
                    pathFoto = ubi.pathFoto,
                    tipoUbicacion = TipoUbicacion(
                        ubi.codTipoUbicacion.toString(),
                        ubi.codTipoUbicacion.toString()), //TODO: change later
                    latitud = ubi.latitud,
                    longitud = ubi.longitud,
                    referencia = ubi.referencia,
                    fecAlta = ubi.fecAlta,
                    usrAlta = ubi.usrAlta,
                    fecUltAct = ubi.fecUltAct,
                    usrUltAct = ubi.usrUltAct
                )
                )
            }

           Observable.just(list)
        }

    }

    fun insertAllClientesToDatabase(clientes: List<Cliente>): Single<List<Long>> {
        return clienteDAO.insertAll(clientes.map { cliente -> cliente.toEntity() })
    }

    fun insertAllFormulariosToDatabase(formularios: List<Formulario>): Single<List<Long>> {
        return formularioDAO.insertAll(formularios.map{ formulario -> formulario.toEntity() })
    }

    fun insertAllVisitasToDatabase(visitas: List<Visita>): Single<List<Long>> {
        return visitaDAO.insertAll(visitas.map{ it.toEntity() })
    }

    fun insertAllUbicacionesToDatabase(ubicaciones: List<Ubicacion>): Single<List<Long>> {
        return ubicacionDAO.insertAll(ubicaciones.map { it.toEntity()})
    }

    fun insertClienteToDatabase(cliente: Cliente, cb: () -> Unit = {}):Completable {
        //TODO: obtener los objetos que faltan
        cliente.formulario?.cliente = cliente
        return clienteDAO.insertNewCliente(cliente.toEntity())
//            .concatWith(ubicacionDAO.insertAll(cliente.ubicaciones!!.map{ ubi -> ubi.toEntity()}))
//            .concatWith(formularioDAO.insert(cliente.formulario?.toEntity()!!))
    }

    fun deleteUbicacion(codUbicacion: Int) : Observable<String> {
        return application.apiClientes!!.deleteUbicacion(codUbicacion)
    }

    fun deleteUbicacionFromDatabase(ubicacion: Ubicacion): Completable {
        return ubicacionDAO.deleteUbicacionCliente(ubicacion.codUbicacionLocal!!)
    }

    fun deleteClientesFromDatabase(cb: () -> Unit = {}): Completable {
        return clienteDAO.deleteAll().andThen(
        ubicacionDAO.deleteAll()).andThen(
        visitaDAO.deleteAll())
    }

    fun activateCliente(codCliente: Long) : Observable<Any>{
        return application.apiClientes!!.activateCliente(codCliente)
    }

    fun deactivateCliente(codCliente: Long) : Observable<Any> {
        return application.apiClientes!!.deactivateCliente(codCliente)
    }

    fun deleteCliente(codCliente: Long) : Observable<Any> {
        return application.apiClientes!!.deleteCliente(codCliente)
    }
}