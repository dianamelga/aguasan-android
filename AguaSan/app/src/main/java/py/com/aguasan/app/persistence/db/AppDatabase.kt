package py.com.aguasan.app.persistence.db

import androidx.room.Database
import androidx.room.RoomDatabase
import py.com.aguasan.app.persistence.daos.*
import py.com.aguasan.app.persistence.entities.*

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Database(entities = [
    (Sesion::class),
    (Barrio::class),
    (Ciudad::class),
    (Cliente::class),
    (Evento::class),
    (Formulario::class),
    (SituacionVisita::class),
    (TipoDocumento::class),
    (TipoUbicacion::class),
    (UbicacionCliente::class),
    (Usuario::class),
    (Visita::class)
], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun sesionDao(): SesionDao
    abstract fun barrioDao(): BarrioDao
    abstract fun ciudadDao(): CiudadDao
    abstract fun clienteDao(): ClienteDao
    abstract fun eventoDao(): EventoDao
    abstract fun formularioDao(): FormularioDao
    abstract fun situacionVisitaDao(): SituacionVisitaDao
    abstract fun tipoDocumentoDao(): TipoDocumentoDao
    abstract fun tipoUbicacionDao(): TipoUbicacionDao
    abstract fun ubicacionClienteDao(): UbicacionClienteDao
    abstract fun usuarioDao(): UsuarioDao
    abstract fun visitaDao(): VisitaDao
}
