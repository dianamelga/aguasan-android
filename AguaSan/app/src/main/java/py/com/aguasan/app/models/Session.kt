package py.com.aguasan.app.models


data class Session(
    val username: String,
    val token: String,
    var enSesion: Boolean,
    var esAdmin: Boolean=false,
    var mail: String
)