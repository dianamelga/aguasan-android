package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.ClientesRepository
import py.com.aguasan.app.models.*
import java.util.concurrent.TimeUnit

class ClientesViewModel(var application: AguaSan, context: Context?) : AguaSanViewModel(context) {



    fun getClientes(request: ClientesFilteredRequest): Observable<List<Cliente>> {
        return ClientesRepository(application).getClientes(request)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { it.printStackTrace() }
    }

    fun postCliente(cliente: Cliente): Observable<Cliente> {
        return ClientesRepository(application).postCliente(cliente)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun editCliente(cliente: Cliente): Observable<Cliente> {
        return ClientesRepository(application).editCliente(cliente)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun getClientesFromDatabase(codCliente: Int?=null): Observable<List<Cliente>> {
        return ClientesRepository(application).getClientesFromDatabase(codCliente)
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }

    fun getClienteAllData(cliente: py.com.aguasan.app.persistence.entities.Cliente): Observable<Cliente> {
        return ClientesRepository(application).getClienteAllData(cliente)
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }

    }

    fun getUbicacionesFromDatabase(cliente: Cliente): Observable<ArrayList<Ubicacion>> {
        return ClientesRepository(application).getUbicacionesFromDatabase(cliente)
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


    fun insertClienteToDatabase(cliente: Cliente, cb:() -> Unit = {}): Completable {
        return ClientesRepository(application).insertClienteToDatabase(cliente,cb)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertAllClientesToDatabase(clientes: List<Cliente>): Single<List<Long>> {
        return ClientesRepository(application).insertAllClientesToDatabase(clientes)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnSuccess { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertAllFormulariosToDatabase(formularios: List<Formulario>): Single<List<Long>> {
        return ClientesRepository(application).insertAllFormulariosToDatabase(formularios)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnSuccess { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertAllUbicacionesToDatabase(ubicaciones: List<Ubicacion>): Single<List<Long>> {
        return ClientesRepository(application).insertAllUbicacionesToDatabase(ubicaciones)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnSuccess { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertAllVisitasToDatabase(visitas: List<Visita>): Single<List<Long>> {
        return ClientesRepository(application).insertAllVisitasToDatabase(visitas)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnSuccess { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteClientesFromDatabase(cb:() -> Unit = {}): Completable {
        return ClientesRepository(application).deleteClientesFromDatabase(cb)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteUbicacionFromDatabase(ubicacion: Ubicacion): Completable {
        return ClientesRepository(application).deleteUbicacionFromDatabase(ubicacion)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


    fun deleteUbicacion(codUbicacion: Int) : Observable<String> {
        return ClientesRepository(application).deleteUbicacion(codUbicacion)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun activateCliente(codCliente: Long) : Observable<Any>{
        return ClientesRepository(application).activateCliente(codCliente)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deactivateCliente(codCliente: Long) : Observable<Any>{
        return ClientesRepository(application).deactivateCliente(codCliente)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteCliente(codCliente: Long) : Observable<Any>{
        return ClientesRepository(application).deleteCliente(codCliente)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


}