package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.persistence.entities.SituacionVisita

/**
 * Created by Diana Melgarejo on 7/26/20.
 */
@Dao
interface SituacionVisitaDao {

    @Query("SELECT * FROM situacion_visita")
    fun getAll(): Observable<List<SituacionVisita>>

    @Query(" SELECT * FROM situacion_visita WHERE cod_situacion = :codSituacion")
    fun getSituacionVisita(codSituacion: Int): Observable<SituacionVisita>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(situaciones: List<SituacionVisita>): Completable

    @Query("DELETE FROM situacion_visita")
    fun deleteAll(): Completable
}