package py.com.aguasan.app.models

import py.com.aguasan.app.persistence.entities.UbicacionCliente
import java.io.Serializable

data class Ubicacion (
    var codUbicacionLocal: Int?=null,
    var codUbicacion: Int?=null,
    var tipoUbicacion: TipoUbicacion? = null,
    var latitud: String? = null,
    var longitud: String? = null,
    var pathFoto: String?= null,
    var foto: String?=null, //base 64
    var referencia: String?=null,
    var cliente: Cliente?=null,
    var fecAlta: String? = null,
    var usrAlta: String? = null,
    var fecUltAct: String? = null,
    var usrUltAct: String? = null
): Serializable {

    fun toEntity(): UbicacionCliente {
        return UbicacionCliente(
            codUbicacion = codUbicacion!!,
            codTipoUbicacion = tipoUbicacion?.codTipoUbicacion!!.toInt(),
            latitud = latitud!!,
            longitud = longitud!!,
            pathFoto = pathFoto ?: "",
            referencia = referencia ?:"",
            codCliente = cliente?.codCliente!!,
            fecAlta = fecAlta ?: "",
            usrAlta = usrAlta ?: "",
            fecUltAct = fecUltAct ?:"",
            usrUltAct = usrUltAct ?: ""

        )
    }
}