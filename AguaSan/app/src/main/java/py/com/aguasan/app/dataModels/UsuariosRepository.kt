package py.com.aguasan.app.dataModels

import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.TipoDocumento
import py.com.aguasan.app.models.Usuario
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.utils.asFormattedDate

class UsuariosRepository (var application : AguaSan) {

    private val usuarioDAO = application.database!!.usuarioDao()

    fun getUsuarios(): Observable<List<Usuario>> {
        return application.apiUsuarios!!.getUsuarios()
    }

    fun createUsuario(usuario: Usuario): Observable<Usuario> {
        return application.apiUsuarios!!.createUsuario(usuario)
    }

    fun updateUsuario(usuario: Usuario): Observable<Usuario> {
        return application.apiUsuarios!!.updateUsuario(usuario)
    }

    fun getUsuariosFromDatabase(): Observable<List<Usuario>> {
        return usuarioDAO.getAll().map{ usuarios -> usuarios.map { entity -> entity.toDomain()}}
    }

    fun deleteAllUsuariosFromDatabase(): Completable {
        return usuarioDAO.deleteAll()
    }

    fun deleteUsuarioFromDatabase(userName: String): Completable {
        return usuarioDAO.deleteUser(userName)
    }

    fun insertUsuarioToDatabase(usuario: Usuario): Completable {
        return usuarioDAO.insertNewUsuario(usuario.toEntity())
    }

    fun insertAllToDatabase(usuarios: List<Usuario>): Completable {
        return usuarioDAO.insertAll(usuarios.map{ domain -> domain.toEntity() })
    }
}