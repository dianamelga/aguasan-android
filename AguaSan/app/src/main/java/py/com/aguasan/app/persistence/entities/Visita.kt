package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/26/20.
 */

const val VISITA_TABLE_NAME = "visita"
@Entity(tableName = "$VISITA_TABLE_NAME")
data class Visita(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "nro_visita_local") var nroVisitaLocal: Int=0,
    @ColumnInfo(name = "nro_visita") var nroVisita: Int,
    @ColumnInfo(name = "cod_cliente") var codCliente: Int,
    @ColumnInfo(name = "cod_situacion") var codSituacion: Int,
    @ColumnInfo(name = "bidones_canje") var bidonesCanje: Int,
    @ColumnInfo(name = "bidones") var bidones: Int,
    @ColumnInfo(name = "fec_alta") var fecAlta: String,
    @ColumnInfo(name = "usr_alta") var usrAlta: String,
    @ColumnInfo(name = "fec_ult_act") var fecUltAct: String,
    @ColumnInfo(name = "usr_ult_act") var usrUltAct: String
) {

    fun toDomain(): py.com.aguasan.app.models.Visita {
        return py.com.aguasan.app.models.Visita(
            //TODO
        )
    }
}