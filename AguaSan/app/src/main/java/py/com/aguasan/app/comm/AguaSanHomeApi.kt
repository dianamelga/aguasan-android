package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AguaSanHomeApi {

    @POST("aguasan-reportes")
    fun sendReports(
        @Body reporteReq: ReporteReq
    ): Observable<GenericApiResponse>

    @POST("aguasan-mail/database")
    fun sendDatabase(
        @Body mailRequest: MailReq
    ): Observable<String>

    @POST("aguasan-mail/mail")
    fun sendMail(
        @Body mailRequest: MailReq
    ): Observable<String>

    @GET("aguasan-general/tiposdocumentos")
    fun getTiposDocumento(
    ): Observable<List<TipoDocumento>>

    @GET("aguasan-general/ciudades")
    fun getCiudades(
    ): Observable<List<Ciudad>>

    @GET("aguasan-general/barrios")
    fun getBarrios(
    ): Observable<List<Barrio>>

    @GET("aguasan-general/ubicaciones-tipos")
    fun getUbicacionesTipos(
    ): Observable<List<TipoUbicacion>>

    @GET("aguasan-general/situacion-visita")
    fun getSituacionesVisitas(
    ):Observable<List<SituacionVisita>>

    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanHomeApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanHomeApi::class.java)
        }
    }
}