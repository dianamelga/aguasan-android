package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.persistence.entities.CIUDAD_TABLE_NAME
import py.com.aguasan.app.persistence.entities.Ciudad

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface CiudadDao {

    @Query("SELECT * FROM $CIUDAD_TABLE_NAME")
    fun getAll(): Observable<List<Ciudad>>

    @Query("SELECT * FROM $CIUDAD_TABLE_NAME WHERE cod_ciudad = :codCiudad")
    fun getCiudad(codCiudad: String): Observable<Ciudad>

    @Query("SELECT a.* FROM ciudad a JOIN barrio b ON a.cod_ciudad = b.cod_ciudad " +
            "JOIN clientes c ON c.cod_barrio = b.cod_barrio AND c.cod_ciudad = b.cod_ciudad " +
            "WHERE c.cod_cliente = :codCliente")
    fun getCiudadFromCliente(codCliente: Int): Observable<Ciudad>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ciudad: Ciudad): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(ciudades: List<Ciudad>): Completable

    @Update
    fun updateCiudad(ciudad: Ciudad): Completable

    @Query("DELETE FROM $CIUDAD_TABLE_NAME")
    fun deleteAll(): Completable

    @Query("DELETE FROM $CIUDAD_TABLE_NAME WHERE cod_ciudad = :codCiudad")
    fun deleteCiudad(codCiudad: String): Completable

}