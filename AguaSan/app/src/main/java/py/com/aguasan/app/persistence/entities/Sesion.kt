package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Entity(tableName = "sesion")
data class Sesion(
    @PrimaryKey
    @ColumnInfo(name="username") var userName: String,
    @ColumnInfo(name="serial_instal") var serialInstal: Int,
    @ColumnInfo(name="token") var token: String,
    @ColumnInfo(name="en_sesion") var enSesion: Int,
    @ColumnInfo(name="es_admin") var esAdmin: Int,
    @ColumnInfo(name="mail") var mail: String="",
    @ColumnInfo(name="fec_ult_logueo") var fecUltLogueo: String
)