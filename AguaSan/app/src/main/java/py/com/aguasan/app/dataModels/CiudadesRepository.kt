package py.com.aguasan.app.dataModels

import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.models.GenericApiResponse
import py.com.aguasan.app.persistence.entities.Ciudad.Companion.toDomain

class CiudadesRepository(var application: AguaSan) {
    private val ciudadDAO = application.database!!.ciudadDao()

    fun listCiudades(): Observable<List<Ciudad>> {
        return application.apiCiudades!!.listCiudades()
    }


    fun createCiudad(ciudad: Ciudad): Observable<Ciudad> {
        return application.apiCiudades!!.createCiudad(ciudad)
    }


    fun updateCiudad(ciudad: Ciudad): Observable<Ciudad> {
        return application.apiCiudades!!.updateCiudad(ciudad)
    }


    fun deleteCiudad(codCiudad: String): Observable<GenericApiResponse> {
        return application.apiCiudades!!.deleteCiudad(codCiudad)
    }


    fun getCiudad(codCiudad: String): Observable<Ciudad> {
        return application.apiCiudades!!.getCiudad(codCiudad)
    }

    fun insertCiudadToDatabase(ciudad: Ciudad): Completable {
        return ciudadDAO.insert(ciudad.toEntity())
    }

    fun insertAllCiudadesToDatabase(ciudades: List<Ciudad>): Completable {
        return ciudadDAO.insertAll(ciudades.map { domain -> domain.toEntity() })
    }

    fun deleteCiudadFromDatabase(codCiudad: String): Completable {
        return ciudadDAO.deleteCiudad(codCiudad)
    }

    fun deleteAllFromDatabase(): Completable {
        return ciudadDAO.deleteAll()
    }

    fun getCiudadesFromDatabase(): Observable<List<Ciudad>> {
        return ciudadDAO.getAll().map { ciudades -> ciudades.map { entity -> entity.toDomain() }}
    }
}