package py.com.aguasan.app.views.activities

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_detalle_cliente.*
import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.PagerAdapter
import py.com.aguasan.app.views.fragments.DetalleClienteFragment
import py.com.aguasan.app.views.fragments.ListaUbicacionesFragment
import py.com.aguasan.app.views.fragments.ListaVisitasFragment

class DetalleClienteActivity : AguaSanActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_cliente)

        supportActionBar?.title = "Detalle de Cliente"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)


        val fragments = ArrayList<Fragment>()
        fragments.add(DetalleClienteFragment.newInstance(intent.extras))
        fragments.add(ListaUbicacionesFragment.newInstance(intent.extras))
        fragments.add(ListaVisitasFragment.newInstance(intent.extras))

        val titles = ArrayList<String>()
        titles.add("INFORMACION")
        titles.add("UBICACIONES")
        titles.add("VISITAS")
        val pagerAdapter = PagerAdapter(fragments, titles, supportFragmentManager)
        pager.adapter = pagerAdapter
        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayout))
    }
}
