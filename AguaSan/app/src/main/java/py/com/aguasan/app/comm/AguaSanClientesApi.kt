package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.ClientesFilteredRequest
import py.com.aguasan.app.models.Ubicacion
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface AguaSanClientesApi {

    @POST("aguasan-cliente/filtered")
    fun getClientes(
        @Body request: ClientesFilteredRequest
    ): Observable<List<Cliente>>

    @POST("aguasan-cliente")
    fun postCliente(
        @Body cliente: Cliente
    ): Observable<Cliente>

    @PUT("aguasan-cliente")
    fun editCliente(
        @Body cliente: Cliente
    ): Observable<Cliente>

    @DELETE("aguasan-cliente/ubicaciones/{codUbicacion}")
    fun deleteUbicacion(
        @Path("codUbicacion") codUbicacion: Int
    ): Observable<String>

    @PUT("aguasan-cliente/deactivate/{codCliente}")
    fun deactivateCliente(
        @Path("codCliente") codCliente: Long
    ): Observable<Any>

    @PUT("aguasan-cliente/activate/{codCliente}")
    fun activateCliente(
        @Path("codCliente") codCliente: Long
    ): Observable<Any>

    @DELETE("aguasan-cliente/{codCliente}")
    fun deleteCliente(
        @Path("codCliente") codCliente: Long
    ): Observable<Any>

    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanClientesApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanClientesApi::class.java)
        }
    }
}