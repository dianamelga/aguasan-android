package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.CiudadesRepository
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.models.GenericApiResponse
import java.util.concurrent.TimeUnit

class CiudadesViewModel(var application: AguaSan, context: Context?) : AguaSanViewModel(context) {

    fun listCiudades(): Observable<List<Ciudad>> {
        return CiudadesRepository(application).listCiudades()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(2)
    }



    fun createCiudad(ciudad: Ciudad): Observable<Ciudad> {
        return CiudadesRepository(application).createCiudad(ciudad)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


    fun updateCiudad(ciudad: Ciudad): Observable<Ciudad>{
        return CiudadesRepository(application).updateCiudad(ciudad)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


    fun deleteCiudad(codCiudad: String): Observable<GenericApiResponse>{
        return CiudadesRepository(application).deleteCiudad(codCiudad)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


    fun getCiudad(codCiudad: String): Observable<Ciudad> {
        return CiudadesRepository(application).getCiudad(codCiudad)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(1)
    }

    fun insertCiudadToDatabase(ciudad: Ciudad): Completable {
        return CiudadesRepository(application).insertCiudadToDatabase(ciudad)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertAllCiudadesToDatabase(ciudades: List<Ciudad>): Completable {
        return CiudadesRepository(application).insertAllCiudadesToDatabase(ciudades)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteCiudadFromDatabase(codCiudad: String): Completable{
        return CiudadesRepository(application).deleteCiudadFromDatabase(codCiudad)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteAllFromDatabase(): Completable{
        return CiudadesRepository(application).deleteAllFromDatabase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }


    fun getCiudadesFromDatabase(): Observable<List<Ciudad>> {
        return CiudadesRepository(application).getCiudadesFromDatabase()
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
            .retry(1)

    }
}