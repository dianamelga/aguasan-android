package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.Evento
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface AguaSanEventosApi {

    @GET("evento")
    fun getListEventos() : Observable<List<Evento>>

    @POST("evento")
    fun createEvento(
        @Body evento: Evento
    ): Observable<Evento>

    @PUT("evento")
    fun updateEvento(
        @Body evento: Evento
    ): Observable<Evento>

    @GET("evento/{nroEvento}")
    fun getEvento(
        @Path("nroEvento")nroEvento: Int
    ): Observable<Evento>

    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanEventosApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanEventosApi::class.java)
        }
    }
}