package py.com.aguasan.app.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import py.com.aguasan.app.R
import py.com.aguasan.app.views.fragments.ListaEventosFragment
import py.com.aguasan.app.views.fragments.NuevoEventoFragment

class EventoActivity : AguaSanActivity() {
    private var add: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_evento)

        supportActionBar?.title = "Eventos"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        add = intent.extras?.getBoolean(ADD,false) ?: false

        if(add) {
            supportActionBar?.title = "Nuevo Evento"
            val bundle = Bundle()
            val fragment = NuevoEventoFragment.newInstance(bundle)
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, fragment)
                .commit()
        }else{
            supportActionBar?.title = "Eventos"
            val bundle = Bundle()
            val fragment = ListaEventosFragment.newInstance(bundle) {

            }
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, fragment)
                .commit()
        }

        supportFragmentManager.addOnBackStackChangedListener {
            val current = supportFragmentManager.backStackEntryCount + 1
            when (current) {
                1 -> {
                    if(!add) {
                        supportActionBar?.title = "Eventos"
                    }else {
                        supportActionBar?.title = "Nuevo Evento"
                    }
                }
                2 -> {
                    supportActionBar?.title = "Modificar Evento"
                }
            }
        }

    }

    companion object {
        const val ADD = "add"
    }
}
