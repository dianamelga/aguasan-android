package py.com.aguasan.app.views.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_barrio.*
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.GenericApiResponse
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.viewModels.BarriosViewModel
import py.com.aguasan.app.views.fragments.ListaDatosGeneralesFragment
import py.com.aguasan.app.views.fragments.NuevoBarrioFragment

class BarrioActivity : AguaSanActivity() {
    private var modoEdicion: Boolean = false
    private lateinit var barriosViewModel: BarriosViewModel

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barrio)

        barriosViewModel = BarriosViewModel(aguasan!!, this)

        supportActionBar?.title = "Barrios"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)


        val bundle = Bundle()
        val fragment = ListaDatosGeneralesFragment.newInstance(bundle, {
            //ON CLICK CALLBACK
            //MODO EDICION
            modoEdicion = true
            val bundle = Bundle()
            bundle.putSerializable(NuevoBarrioFragment.BARRIO_SELECTED, it as Barrio)
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, NuevoBarrioFragment.newInstance(bundle))
                .addToBackStack(null)
                .commit()
        },
            //ON REFRESH CALLBACK
            { data, cache, adapter, afterRefresh ->
                barriosViewModel.listBarrios().subscribe(object: AguaSanObserver<List<Barrio>>(this) {
                    override fun onNext(t: List<Barrio>) {
                        barriosViewModel.deleteAllFromDatabase().subscribeBy(onComplete={

                            barriosViewModel.insertAllBarriosToDatabase(t).subscribeBy(onComplete ={
                                data.clear()
                                data.addAll(t)
                                cache.clear()
                                cache.addAll(data)
                                adapter?.notifyDataSetChanged()

                                afterRefresh()
                            })

                        })

                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        afterRefresh()
                    }

                })

            },
            {item, data, cache, adapter ->
                //DELETE CALLBACK
                item as Barrio
                barriosViewModel.deleteBarrio(item.codBarrio!!, item.ciudad?.codCiudad!!).subscribe(object: AguaSanObserver<GenericApiResponse>(this) {
                    override fun onNext(t: GenericApiResponse) {
                        barriosViewModel.deleteBarrioFromDatabase(item.codBarrio, item.ciudad.codCiudad).subscribeBy(onComplete = {
                            UtilTools.showDialog(this@BarrioActivity, t.message) {

                                barriosViewModel.getAllBarriosFromDatabase().subscribe(object: AguaSanObserver<List<Barrio>>(this@BarrioActivity) {
                                    override fun onNext(t: List<Barrio>) {
                                        data.clear()
                                        cache.clear()
                                        data.addAll(t)
                                        cache.addAll(data)
                                        adapter?.notifyDataSetChanged()
                                    }

                                })

                            }
                        })


                    }

                })

            })

        btAddBarrio.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, NuevoBarrioFragment.newInstance())
                .addToBackStack(null)
                .commit()

        }

        supportFragmentManager.beginTransaction()
            .add(R.id.fragment, fragment)
            .commit()



        supportFragmentManager.addOnBackStackChangedListener {
            val current = supportFragmentManager.backStackEntryCount + 1
            when (current) {
                1 -> {
                    modoEdicion = false
                    supportActionBar?.title = "Barrios"
                    btAddBarrio?.visibility = View.VISIBLE
                }
                2 -> {
                    if(!modoEdicion) {
                        supportActionBar?.title = "Nuevo Barrio"
                    }else{
                        supportActionBar?.title = "Modificar Barrio"
                    }
                    btAddBarrio?.visibility = View.GONE
                }
            }
        }
    }
}
