package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import py.com.aguasan.app.persistence.entities.Evento

/**
 * Created by Diana Melgarejo on 7/26/20.
 */
@Dao
interface EventoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(eventos: List<Evento>): Completable

    @Update
    fun updateEvento(evento: Evento): Completable

    @Query("DELETE FROM evento")
    fun deleteAll(): Completable
}