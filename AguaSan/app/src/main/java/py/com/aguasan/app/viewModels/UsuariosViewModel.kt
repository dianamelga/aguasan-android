package py.com.aguasan.app.viewModels

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.dataModels.UsuariosRepository
import py.com.aguasan.app.models.Usuario
import java.util.concurrent.TimeUnit

class UsuariosViewModel (var application: AguaSan, context: Context?) : AguaSanViewModel(context) {

    fun getUsuarios(): Observable<List<Usuario>> {
        return UsuariosRepository(application).getUsuarios()
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun createUsuario(usuario: Usuario): Observable<Usuario> {
        return UsuariosRepository(application).createUsuario(usuario)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun updateUsuario(usuario: Usuario): Observable<Usuario> {
        return UsuariosRepository(application).updateUsuario(usuario)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun getUsuariosFromDatabase(): Observable<List<Usuario>> {
        return UsuariosRepository(application).getUsuariosFromDatabase()
            .take(1)
            .debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnNext { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteAllUsuariosFromDatabase(): Completable {
        return UsuariosRepository(application).deleteAllUsuariosFromDatabase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun deleteUsuarioFromDatabase(userName: String): Completable {
        return UsuariosRepository(application).deleteUsuarioFromDatabase(userName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertUsuarioToDatabase(usuario: Usuario): Completable {
        return UsuariosRepository(application).insertUsuarioToDatabase(usuario)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }

    fun insertAllToDatabase(usuarios: List<Usuario>): Completable {
        return UsuariosRepository(application).insertAllToDatabase(usuarios)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { addTask() }
            .doOnComplete { finishTask() }
            .doOnError { finishTask(); it.printStackTrace() }
    }
}