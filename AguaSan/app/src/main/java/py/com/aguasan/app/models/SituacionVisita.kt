package py.com.aguasan.app.models

import java.io.Serializable

data class SituacionVisita (
    val codSituacion: Int?=null,
    val situacion: String?=null
): Serializable {
    
    fun toEntity(): py.com.aguasan.app.persistence.entities.SituacionVisita {
        return py.com.aguasan.app.persistence.entities.SituacionVisita(
            codSituacion = codSituacion!!,
            situacion = situacion!!
        )
    }
}