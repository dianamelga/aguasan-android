package py.com.aguasan.app.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Diana Melgarejo on 7/25/20.
 */

@Entity(tableName = "$TIPO_UBICACION_TABLE_NAME")
data class TipoUbicacion(
    @PrimaryKey
    @ColumnInfo(name = "cod_tipo_ubicacion") var codTipoUbicacion: String,
    @ColumnInfo(name = "tipo_ubicacion") var tipoUbicacion: String
) {

    companion object {
        fun TipoUbicacion.toDomain() : py.com.aguasan.app.models.TipoUbicacion {
            return py.com.aguasan.app.models.TipoUbicacion(
                codTipoUbicacion = this.codTipoUbicacion,
                tipoUbicacion = this.tipoUbicacion
            )
        }
    }
}


const val TIPO_UBICACION_TABLE_NAME = "ubicaciones_tipos"
