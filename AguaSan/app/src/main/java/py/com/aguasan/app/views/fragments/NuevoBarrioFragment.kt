package py.com.aguasan.app.views.fragments


import android.app.Activity
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_nuevo_barrio.*

import py.com.aguasan.app.R
import py.com.aguasan.app.models.Barrio
import py.com.aguasan.app.models.Ciudad
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.utils.isValidTextWithFocus
import py.com.aguasan.app.viewModels.BarriosViewModel
import py.com.aguasan.app.viewModels.CiudadesViewModel


class NuevoBarrioFragment : AguaSanFragment() {
    private var barrio: Barrio?=null
    private lateinit var barriosViewModel: BarriosViewModel
    private lateinit var ciudadesViewModel: CiudadesViewModel
    private var ciudadSelected: Ciudad?=null
    private val ciudades: ArrayList<Ciudad> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        barriosViewModel = BarriosViewModel(aguaSan!!, aguaSanActivity!!)
        ciudadesViewModel = CiudadesViewModel(aguaSan!!, aguaSanActivity!!)
        arguments?.getSerializable(BARRIO_SELECTED)?.let{
            barrio = it as Barrio
        }
        return inflater.inflate(R.layout.fragment_nuevo_barrio, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ciudadesViewModel.getCiudadesFromDatabase().subscribe(object: AguaSanObserver<List<Ciudad>>(aguaSanActivity!!) {
            override fun onNext(t: List<Ciudad>) {
                ciudades.clear()
                ciudades.addAll(t)
            }

        })

        val ciudadAdapter: ArrayAdapter<Ciudad> = ArrayAdapter(aguaSanActivity!!, R.layout.spinner_text, ciudades)
        spCiudad.adapter = ciudadAdapter
        spCiudad.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                ciudadSelected = parent?.getItemAtPosition(position) as Ciudad
            }
        }


        barrio?.let{
            etCodBarrio.setText(it.codBarrio)
            tilCodBarrio.isEnabled = false
            etBarrio.setText(it.barrio)
            spCiudad.setSelection(ciudades.indexOf(it.ciudad))
            spCiudad.isEnabled = false
            tilCiudad.isEnabled = false
        }


        btGuardar.setOnClickListener {
            if(isFormValid()) {
                if(barrio != null) {
                    //UPDATE
                    barriosViewModel.updateBarrio(Barrio(etCodBarrio.text.toString(), etBarrio.text.toString(), ciudadSelected))
                        .subscribe(object: AguaSanObserver<Barrio>(aguaSanActivity!!) {
                            override fun onNext(t: Barrio) {
                                barriosViewModel.deleteBarrioFromDatabase(t.codBarrio!!, t.ciudad?.codCiudad!!).subscribeBy(onComplete={
                                    barriosViewModel.insertBarrioToDatabase(t).subscribeBy(onComplete={
                                        UtilTools.showDialog(aguaSanActivity!!, "Barrio modificado correctamente.") {
                                            aguaSanActivity?.setResult(Activity.RESULT_OK)
                                            aguaSanActivity?.finish()
                                        }
                                    })
                                })
                            }

                        })
                }else{
                    //INSERT
                    barriosViewModel.createBarrio(Barrio(etCodBarrio.text.toString(), etBarrio.text.toString(), ciudadSelected))
                        .subscribe(object: AguaSanObserver<Barrio>(aguaSanActivity!!) {
                            override fun onNext(t: Barrio) {
                                barriosViewModel.insertBarrioToDatabase(t).subscribeBy(onComplete={
                                    UtilTools.showDialog(aguaSanActivity!!, "Barrio creado.") {
                                        aguaSanActivity?.setResult(Activity.RESULT_OK)
                                        aguaSanActivity?.finish()
                                    }
                                })
                            }

                        })
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun isFormValid(): Boolean {
        return tilCodBarrio.isValidTextWithFocus("Debe completar este campo.") &&
                tilBarrio.isValidTextWithFocus("Debe completar este campo.")
    }


    companion object {
        const val BARRIO_SELECTED = "barrio_selected"
        fun newInstance(b: Bundle?=null): NuevoBarrioFragment {
            val fragment = NuevoBarrioFragment()
            b?.let {
                fragment.arguments = it
            }
            return fragment
        }
    }

}
