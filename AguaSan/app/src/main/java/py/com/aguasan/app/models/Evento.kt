package py.com.aguasan.app.models

import java.io.Serializable

data class Evento(
    val nroEvento: Int?=null,
    val codCliente: Int?=null,
    var cliente: Cliente?=null,
    val fecEvento: String? = null,
    val observacion: String? = null,
    var estado: String? = null,
    val fecAlta: String?= null,
    val usrAlta: String?=null,
    var fecUltAct: String?= null,
    var usrUltAct: String?=null
): Serializable