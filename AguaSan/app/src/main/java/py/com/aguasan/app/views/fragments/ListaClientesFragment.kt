package py.com.aguasan.app.views.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.appcompat.widget.SearchView
import android.view.*
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.clientes_fragment.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.ClienteAdapter
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.ClientesFilteredRequest
import py.com.aguasan.app.models.ClientesFilters
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.LoadMoreListener
import py.com.aguasan.app.utils.OnScrollLoadMore
import py.com.aguasan.app.utils.UtilTools
import py.com.aguasan.app.viewModels.BarriosViewModel
import py.com.aguasan.app.viewModels.CiudadesViewModel
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.viewModels.HomeViewModel
import py.com.aguasan.app.views.activities.ClienteActivity
import py.com.aguasan.app.views.activities.RegistrarVisitaActivity

class ListaClientesFragment : AguaSanFragment(), LoadMoreListener {
    private lateinit var viewModel: ClientesViewModel
    private lateinit var clientesAdapter: ClienteAdapter
    private var clientes: ArrayList<Cliente> = ArrayList()
    private var clientesCached: ArrayList<Cliente> = ArrayList()
    private val REQUEST_CODE = 100
    private val REQUEST_CODE_VISITA = 10
    private val REQUEST_PERMISSIONS_PHONE_CALL = 101

    private val MAX_QUERY_CLIENTES = 20
    private var currentPage = 1
    private var filtro = "*"
    var callback: (Cliente) -> Unit = {}
    private var currentPhoneNumber: String? = null
    private val dataUpdated: PublishSubject<Unit> = PublishSubject.create()

    init {
        resourceId = R.layout.clientes_fragment
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ClientesViewModel(aguaSanActivity?.aguasan!!, aguaSanActivity!!)

        return inflater.inflate(R.layout.clientes_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clientesAdapter = ClienteAdapter(clientes)
        clientesAdapter.callback = callback //ver
        clientesAdapter.longCallback = {
            if (aguaSan?.session?.esAdmin == true) {
                val builder = AlertDialog.Builder(aguaSanActivity!!, R.style.AppTheme_Dialog)
                builder.setTitle("Opciones")

                // add a list
                val options = arrayOf(
                    "Inactivar",
                    "Eliminar"
                )

                builder.setItems(options) { dialog, which ->
                    when (which) {
                        0 -> {
                            viewModel.deactivateCliente(it.codCliente?.toLong() ?: 0)
                                .subscribe(object : AguaSanObserver<Any>(aguaSanActivity!!) {
                                    override fun onNext(t: Any) {
                                        val c =
                                            clientes.filter { c -> c.codCliente != it.codCliente && c.activo?.toUpperCase() == "S"}
                                        if (c.isNotEmpty()) {
                                            clientes.clear()
                                            clientes.addAll(c)
                                            clientesAdapter.notifyDataSetChanged()
                                        }

                                        UtilTools.showDialog(
                                            aguaSanActivity!!,
                                            "Cliente Inactivado"
                                        )
                                    }

                                })
                        }
                        1 -> {
                            viewModel.deleteCliente(it.codCliente?.toLong() ?: 0)
                                .subscribe(object : AguaSanObserver<Any>(aguaSanActivity!!) {
                                    override fun onNext(t: Any) {
                                        val c =
                                            clientes.filter { c -> c.codCliente != it.codCliente && c.activo?.toUpperCase() == "S"}
                                        if (c.isNotEmpty()) {
                                            clientes.clear()
                                            clientes.addAll(c)
                                            clientesAdapter.notifyDataSetChanged()
                                        }

                                        UtilTools.showDialog(aguaSanActivity!!, "Cliente Eliminado")
                                    }

                                })
                        }
                    }
                }
                builder.setNegativeButton("Cancelar", null)

                // create and show the alert dialog
                val dialog = builder.create()
                dialog.show()
            }
        }
        clientesAdapter.funLlamar = {
            currentPhoneNumber = it
            if (ActivityCompat.checkSelfPermission(
                    aguaSanActivity!!,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    REQUEST_PERMISSIONS_PHONE_CALL
                )

            } else {
                llamarCliente()
            }
        }
        clientesAdapter.funEditar = {
            val i = Intent(aguaSanActivity!!, ClienteActivity::class.java)
            i.putExtra(ListaUbicacionesFragment.PERMITE_INCLUIR, true)
            i.putExtra("cliente", it)
            startActivityForResult(i, REQUEST_CODE)
            aguaSanActivity!!.showProgress()
        }
        clientesAdapter.funRegVisita = {
            val i = Intent(aguaSanActivity!!, RegistrarVisitaActivity::class.java)
            i.putExtra("cliente", it)
            startActivityForResult(i, REQUEST_CODE_VISITA)
        }




        rvClientes.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            androidx.recyclerview.widget.RecyclerView.VERTICAL,
            false
        )
        rvClientes.addOnScrollListener(
           getScrollListener()
        )
        rvClientes.adapter = clientesAdapter


        viewModel.getClientesFromDatabase().subscribe(object: AguaSanObserver<List<Cliente>>(aguaSanActivity!!) {
            override fun onNext(t: List<Cliente>) {
                clientes.clear()
                clientes.addAll(t)
                clientesCached.clear()
                clientesCached.addAll(t)

                if (clientes.size <= 0) {
                    val filters = HashMap<String,String>() //empty filter
                    val request = ClientesFilteredRequest(1, MAX_QUERY_CLIENTES, filters)
                    getClientesFromAPI(request)
                } else {
                    //convert cliente entity to domain
                    updateData()
                }


            }

        })

        dataUpdated.subscribe {
            if (aguaSan?.session?.esAdmin != true) {
                val c = clientes.filter { c -> c.activo == "S" }
                clientes.clear()
                clientes.addAll(c)
            }
            clientesAdapter.notifyDataSetChanged()
            clientesCached.addAll(clientes)
        }
        
        


        slRefresh.setOnRefreshListener {
            //actualizamos todos los datos o solo la lista de clientes? esto puede demorar unos minutos

            val homeViewModel = HomeViewModel(aguaSan!!, aguaSanActivity!!)
            val barriosViewModel = BarriosViewModel(aguaSan!!, aguaSanActivity!!)
            val ciudadesViewModel = CiudadesViewModel(aguaSan!!, aguaSanActivity!!)

            // actualizamos la lista de clientes
            clientes.clear()
            clientesAdapter.notifyDataSetChanged()

            barriosViewModel.deleteAllFromDatabase()
                .andThen(ciudadesViewModel.deleteAllFromDatabase())
                .andThen(homeViewModel.deleteSituacionesVisitasFromDatabase())
                .andThen(homeViewModel.deleteTiposDocumentosFromDatabase())
                .andThen(viewModel.deleteClientesFromDatabase())
                .subscribeBy(onComplete={
                    UtilTools.cargarDatosGenerales(
                        homeViewModel,
                        barriosViewModel,
                        ciudadesViewModel,
                        aguaSanActivity!!
                    )

                    val filters = HashMap<String,String>() //empty filter
                    val request = ClientesFilteredRequest(1, MAX_QUERY_CLIENTES, filters)
                    getClientesFromAPI(request)
                })

        }


    }

    fun getClientesFromAPI(request: ClientesFilteredRequest, replaceClientUpdated: Boolean=false) {
        viewModel.getClientes(request)
            .subscribe(object : AguaSanObserver<List<Cliente>>(aguaSanActivity!!) {
                override fun onNext(t: List<Cliente>) {

                    val newClients = if(!replaceClientUpdated) {
                        t.filter { new -> clientes.firstOrNull { old -> old.codCliente != new.codCliente } != null  }
                    }else {
                        t.filter { new -> clientes.firstOrNull { old -> old.codCliente == new.codCliente } != null  }
                    }
                    if(newClients.isNullOrEmpty()) return

                    newClients.mapIndexed { index, cliente ->
                        cliente.formularios?.map { f -> f.cliente = cliente}
                        cliente.visitas?.map { v -> v.cliente = cliente}
                        cliente.ubicaciones?.map { u -> u.cliente = cliente}
                        viewModel.insertAllFormulariosToDatabase(cliente.formularios ?: listOf())
                            .subscribeBy(onSuccess = {
                                Log.d(TAG, "formularios insertados: ${it.size},, cliente: ${cliente.nombres}," +
                                        "index: $index")
                            })

                        viewModel.insertAllVisitasToDatabase(cliente.visitas ?: listOf())
                            .subscribeBy(onSuccess = {
                                Log.d(TAG, "Visitas insertadas: ${it.size},, cliente: ${cliente.nombres}," +
                                        "index: $index")
                            })

                        viewModel.insertAllUbicacionesToDatabase(cliente.ubicaciones ?: listOf())
                            .subscribeBy(onSuccess = {
                                Log.d(TAG, "Ubicaciones insertadas: ${it.size}, cliente: ${cliente.nombres}," +
                                        "index: $index")
                            })
                    }

                    viewModel.insertAllClientesToDatabase(t)
                        .subscribeBy(onSuccess= {
                            if(request.pagina <= 1) {
                                clientes.clear()
                                clientesCached.clear()
                            }

                            clientes.addAll(t.filter { c -> c.activo == "S" })
                            clientesCached.addAll(clientes)
                            clientesAdapter.notifyDataSetChanged()

                        })


                    slRefresh.isRefreshing = false
                }

                override fun onError(e: Throwable) {
                    super.onError(e)
                    slRefresh.isRefreshing = false
                }

            })


//        viewModel.getClientes()
//            .subscribe(object : AguaSanObserver<List<Cliente>>(aguaSanActivity!!) {
//                override fun onNext(t: List<Cliente>) {
//                    viewModel.insertAllClientesToDatabase(t).subscribeBy(onSuccess = {
//                        clientes.clear()
//                        clientes.addAll(t)
//                        clientesCached.clear()
//                        clientesCached.addAll(clientes)
//                        clientesAdapter.notifyDataSetChanged()
//
//                        slRefresh.isRefreshing = false
//                    })
//
//                }
//
//                override fun onError(e: Throwable) {
//                    super.onError(e)
//                    slRefresh.isRefreshing = false
//                }
//
//            })

    }

    private fun llamarCliente() {
        val i = Intent(Intent.ACTION_CALL, Uri.parse("tel:$currentPhoneNumber"))
        aguaSanActivity!!.startActivity(i)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            val codCliente = data?.getIntExtra("codCliente", 0) ?: 0
            if(codCliente != 0) {
                val filters = hashMapOf(ClientesFilters.codCliente to codCliente.toString())
                val request = ClientesFilteredRequest(1, MAX_QUERY_CLIENTES, filters)
                getClientesFromAPI(request, true)
            }else {
                clientes.clear()
                viewModel.deleteClientesFromDatabase().subscribeBy(onComplete = {
                    val filters = HashMap<String, String>() //empty filters
                    val request = ClientesFilteredRequest(1, MAX_QUERY_CLIENTES, filters)
                    getClientesFromAPI(request, true)
                })
            }


        } else if (requestCode == REQUEST_CODE_VISITA && resultCode == RESULT_OK) {
            val codCliente = data?.getIntExtra("codCliente", 0) ?: 0
            if(codCliente != 0) {
                val filters = hashMapOf(ClientesFilters.codCliente to codCliente.toString())
                val request = ClientesFilteredRequest(1, MAX_QUERY_CLIENTES, filters)
                getClientesFromAPI(request, true)
            }
//            viewModel.getClientesFromDatabase().subscribe(object: AguaSanObserver<List<Cliente>>(aguaSanActivity!!) {
//                override fun onNext(t: List<Cliente>) {
//                    clientes.clear()
//                    clientes.addAll(t)
//                    clientesCached.clear()
//                    clientesCached.addAll(clientes)
//                    clientesAdapter.notifyDataSetChanged()
//                }
//            })
            
        }
    }


    fun updateData() {
        clientes.mapIndexed { index, c ->
            viewModel.getClienteAllData(c.toEntity()).subscribe(object: AguaSanObserver<Cliente>(aguaSanActivity!!){
                override fun onNext(t: Cliente) {
                    c.tipoDocumento = t.tipoDocumento
                    c.ciudad = t.ciudad
                    c.barrio = t.barrio
                    c.formulario = t.formulario
                    c.ubicaciones = t.ubicaciones
                    c.visitas = t.visitas
                    c.formularios = t.formularios

                    if(index >= clientes.size -1) {
                        dataUpdated.onNext(Unit)
                    }
                }

            })

        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSIONS_PHONE_CALL) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                llamarCliente()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_search, menu)
        menu.findItem(R.id.action_filters)?.setOnMenuItemClickListener {
            val builder = AlertDialog.Builder(aguaSanActivity!!, R.style.AppTheme_Dialog)
            builder.setTitle("Filtrar por")

            // add a list
            val options = arrayOf("Código de Cliente",
            "Nombre de Cliente",
            "Apellido de Cliente",
            "Número de documento de Cliente")
            builder.setItems(options) { dialog, which ->
                when(which) {
                    0 -> {
                        filtro = ClientesFilters.codCliente
                    }
                    1 -> {
                        filtro = ClientesFilters.nombres
                    }
                    2 -> {
                        filtro = ClientesFilters.apellidos
                    }
                    3-> {
                        filtro = ClientesFilters.nroDocumento
                    }
                }
                Toast.makeText(requireContext(), "Filtro elegido: $filtro \n " +
                        "Escriba el valor en el campo de búsqueda y presione buscar (lupa)",
                    Toast.LENGTH_LONG).show()
            }

            builder.setNegativeButton("Cancelar", null)

            // create and show the alert dialog
            val dialog = builder.create()
            dialog.show()
            true
        }
        (menu.findItem(R.id.action_search).actionView as SearchView).apply {
            this.queryHint = "Buscar"
            setIconifiedByDefault(false)
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    p0?.let { filter ->
                        val newFilter = if(filter.isNotEmpty()) {
                            filter
                        }else {
                            ""
                        }

                        currentPage = 1
                        val filters = hashMapOf(filtro to newFilter) //TODO
                        val request = ClientesFilteredRequest(currentPage, MAX_QUERY_CLIENTES, filters)
                        getClientesFromAPI(request)

                    }
                    return false
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    p0?.let { s ->
                        val temp = ArrayList<Cliente>()
                        if (s.isNotBlank() || s.isNotEmpty()) {
                            for (item in clientesCached) {
                                if (item.nombres?.contains(s, true) == true ||
                                    item.apellidos?.contains(s, true) == true ||
                                    item.nroDocumento?.contains(s, true) == true ||
                                    item.formulario?.nroFormulario?.toString()
                                        ?.contains(s, true) == true ||
                                            item.codCliente?.toString()?.contains(s, true) == true
                                ) {
                                    temp.add(item)
                                }
                            }
                        } else {
                            temp.addAll(clientesCached)
                        }
                        clientes.clear()
                        clientes.addAll(temp)
                        clientesAdapter?.notifyDataSetChanged()
                    }
                    return false
                }
            })
        }
    }

    companion object {

        private const val TAG = "ListaClientesFragment"

        fun newInstance(b: Bundle? = null, callback: (Cliente) -> Unit = {}): AguaSanFragment {
            val fragment = ListaClientesFragment()
            b?.let { fragment.arguments = it }
            fragment.callback = callback
            return fragment
        }


    }

    override fun loadMoreElements() {
        currentPage++
        val filters = hashMapOf("" to filtro) //TODO
        val request = ClientesFilteredRequest(currentPage, MAX_QUERY_CLIENTES, filters)
        getClientesFromAPI(request)
    }

    private fun getScrollListener(): RecyclerView.OnScrollListener {
        return object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                //recyclerView got scrolled vertically
                if(dy > 0) {
                    //check if we have reached the bottom of the recyclerView or not
                    //to do that we need to know how many items are there in the screen
                    //what is the top item position
                    //an recyclerView size
                    val visibleItemCount = rvClientes.layoutManager?.childCount ?: return
                    val pastVisibleItem = (rvClientes.layoutManager as? LinearLayoutManager)?.findFirstCompletelyVisibleItemPosition() ?: return
                    val total = clientesAdapter.itemCount

                    //if is not loading, we can get the next page data
                   //if(progressBar.visibility != View.VISIBLE) {
                        //check if we reached the bottom or not
                        if((visibleItemCount + pastVisibleItem) >= total) {
                            loadMoreElements()
                        }
                   // }


                }
                super.onScrolled(recyclerView, dx, dy)
            }
        }
    }

}
