package py.com.aguasan.app.dataModels

import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.Visita

class VisitaRepository (var application : AguaSan) {
    private val visitaDAO = application.database!!.visitaDao()

    fun createVisita(visita: Visita): Observable<Visita> {
        return application.apiVisita!!.createVisita(visita)
    }

    fun getVisitas(codCliente: Int): Observable<List<Visita>> {
        return application.apiVisita!!.getVisitas(codCliente)
    }

    fun insertVisita(visita: Visita, cb: () -> Unit = {}): Completable {
        return visitaDAO.insert(visita.toEntity())
    }

    fun getVisitasFromDatabase(codCliente: Int): Observable<List<Visita>> {
        return visitaDAO.getVisitasFromCliente(codCliente).map { visitas -> visitas.map{ entity -> entity.toDomain() } }
    }

}