package py.com.aguasan.app.comm

import io.reactivex.Observable
import py.com.aguasan.app.models.TokenRegistrationReq
import py.com.aguasan.app.models.TokenRegistrationResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface AguaSanFirebaseAdminApi {

    @POST("firebase/registration")
    fun registerToken(
        @Body tokenRegistrationReq : TokenRegistrationReq
    ): Observable<TokenRegistrationResponse>


    companion object {
        fun createForApi(apiURL: String, params: Pair<String?, String?>?) : AguaSanFirebaseAdminApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(AguaSanClient.provideClient(params))
                .baseUrl(apiURL)
                .build()

            return retrofit.create(AguaSanFirebaseAdminApi::class.java)
        }
    }
}