package py.com.aguasan.app.adapters

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Ubicacion
import java.io.File

class UbicacionAdapter(ubicaciones: ArrayList<Ubicacion>, val context: Context, val hideButtons: Boolean=true) :
    androidx.recyclerview.widget.RecyclerView.Adapter<UbicacionAdapter.UbicacionViewHolder>() {
    var items: List<Ubicacion> = emptyList()
    var callbackAddFoto: (Ubicacion) -> Unit = { c -> }
    var callbackMapa: (Ubicacion) -> Unit = { c -> }
    var longCallback: (Ubicacion) -> Unit= {c ->}

    init {
        items = ubicaciones
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): UbicacionAdapter.UbicacionViewHolder {
        val windowManager = p0.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_list_ubicacion_view, p0, false)
        return UbicacionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: UbicacionAdapter.UbicacionViewHolder, position: Int) {
        val item = items[position]
        holder.lugar?.text = item.tipoUbicacion?.tipoUbicacion
        holder.referencia?.text = item.referencia
        holder.btVerEnMapa?.setOnClickListener { callbackMapa(item) }
        holder.btAgregarFoto?.setOnClickListener { callbackAddFoto(item) }

        if(item.pathFoto.isNullOrEmpty() || item.pathFoto.equals("null")) {
            holder.btAgregarFoto?.text = "AGREGAR FOTO"
        }else{
            holder.btAgregarFoto?.text = "CAMBIAR FOTO"
        }
        try {
            val file = File(item.pathFoto)
            val uri = Uri.fromFile(file)
            Glide.with(context)
                .load(uri).error(R.drawable.google_maps_icon)
                .into(holder.fotoUbicacion!!)
        }catch (e: Exception) {
            e.printStackTrace()
        }

        holder.viewItem?.setOnLongClickListener {
            longCallback(item)
            true
        }

        if(hideButtons) {
            holder.btAgregarFoto?.visibility = View.GONE
            holder.btVerEnMapa?.gravity = Gravity.CENTER
            /*holder.btAgregarFoto?.setOnClickListener {
                Toast.makeText(context,
                "Debe editar el cliente para utilizar esta opción", Toast.LENGTH_SHORT).show()
            }*/
        }

        holder.btAgregarFoto?.visibility = View.GONE // por ahora


    }


    class UbicacionViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var lugar: TextView? = null
        var fotoUbicacion: ImageView? = null
        var btVerEnMapa: Button? = null
        var btAgregarFoto: Button? = null
        var referencia: TextView? = null
        var viewItem: View? = null

        init {
            lugar = v.findViewById(R.id.tvUbicacion)
            fotoUbicacion = v.findViewById(R.id.ivFotoUbicacion)
            btVerEnMapa = v.findViewById(R.id.btVerEnMapa)
            btAgregarFoto = v.findViewById(R.id.btAgregarFoto)
            referencia = v.findViewById(R.id.tvReferencia)
            viewItem = v
        }
    }

}

