package py.com.aguasan.app.dataModels

import io.reactivex.Observable
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.models.Evento

class EventosRepository(var application: AguaSan) {

    fun getListEventos(): Observable<List<Evento>> {
        return application.apiEventos!!.getListEventos()
    }

    fun getEvento(nroEvento: Int): Observable<Evento>{
        return application.apiEventos!!.getEvento(nroEvento)
    }

    fun updateEvento(evento: Evento) : Observable<Evento> {
        return application.apiEventos!!.updateEvento(evento)
    }

    fun createEvento(evento: Evento) : Observable<Evento> {
        return application.apiEventos!!.createEvento(evento)
    }
}