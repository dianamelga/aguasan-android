package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.persistence.entities.TipoUbicacion

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface TipoUbicacionDao {

    @Query("SELECT * FROM ubicaciones_tipos")
    fun getAll(): Observable<List<TipoUbicacion>>

    @Query("SELECT * FROM ubicaciones_tipos WHERE cod_tipo_ubicacion = :codTipoUbicacion")
    fun getTipoUbicacion(codTipoUbicacion: Int): Observable<TipoUbicacion>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewTipoUbicacion(tipoUbicacion: TipoUbicacion): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tiposUbicaciones: List<TipoUbicacion>): Completable

    @Update
    fun updateTipoUbicacion(tipoUbicacion: TipoUbicacion): Completable

    @Query("DELETE FROM ubicaciones_tipos")
    fun deleteAll(): Completable
}