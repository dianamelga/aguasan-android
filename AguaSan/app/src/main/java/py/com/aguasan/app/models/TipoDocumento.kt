package py.com.aguasan.app.models

import java.io.Serializable

data class TipoDocumento(
    val codTipoDocumento: String?=null,
    val tipoDocumento: String?=null
): Serializable {

    override fun toString(): String {
        tipoDocumento?.let { return tipoDocumento} ?: return super.toString()
    }

    override fun equals(other: Any?): Boolean {
        return when(other) {
            is TipoDocumento -> {
                codTipoDocumento == other.codTipoDocumento
            }
            else -> false
        }

    }

    fun toEntity(): py.com.aguasan.app.persistence.entities.TipoDocumento {
        return py.com.aguasan.app.persistence.entities.TipoDocumento(
            codTipoDocumento = codTipoDocumento!!,
            tipoDocumento = tipoDocumento!!
        )
    }
}

