package py.com.aguasan.app.views.fragments


import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_lista_ubicaciones.*

import py.com.aguasan.app.R
import py.com.aguasan.app.adapters.UbicacionAdapter
import py.com.aguasan.app.models.Cliente
import py.com.aguasan.app.models.Ubicacion
import py.com.aguasan.app.utils.AguaSanObserver
import py.com.aguasan.app.utils.CameraHelper
import py.com.aguasan.app.utils.toByteArray
import py.com.aguasan.app.utils.toHexString
import py.com.aguasan.app.views.activities.MapsActivity
import py.com.aguasan.app.viewModels.ClientesViewModel
import py.com.aguasan.app.views.activities.CapturaUbicacionActivity
import py.com.aguasan.app.views.activities.ClienteActivity
import java.io.File
import kotlin.collections.ArrayList
import kotlin.experimental.and

class ListaUbicacionesFragment : AguaSanFragment(), Step {
    private lateinit var viewModel: ClientesViewModel
    private lateinit var ubicacionesAdapter: UbicacionAdapter
    private var ubicaciones: ArrayList<Ubicacion> = ArrayList()
    private var photoPath: String?=null
    private var currentUbicacion: Ubicacion?=null

    init {
        resourceId = R.layout.fragment_lista_ubicaciones
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        viewModel = ClientesViewModel(aguaSanActivity?.aguasan!!, aguaSanActivity!!)

        return inflater.inflate(R.layout.fragment_lista_ubicaciones, container, false)
    }

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ubicacionesAdapter = UbicacionAdapter(ubicaciones, aguaSanActivity!!, arguments?.getBoolean(ADD_FOTO, false) ?: false)

        val llm = androidx.recyclerview.widget.LinearLayoutManager(context)
        llm.orientation = RecyclerView.VERTICAL
        rvUbicaciones.layoutManager = llm
        rvUbicaciones.adapter = ubicacionesAdapter

        ubicacionesAdapter.callbackMapa = {
            val i = Intent(aguaSanActivity!!, MapsActivity::class.java)
            i.putExtra(MapsActivity.LATITUDE, it.latitud?.toDouble())
            i.putExtra(MapsActivity.LONGITUDE, it.longitud?.toDouble())
            startActivity(i)
        }

        ubicacionesAdapter.callbackAddFoto = {
            currentUbicacion = it
            launchCamera()
        }

        ubicacionesAdapter.longCallback = {
            if(!permiteIncluir) {
                Toast.makeText(aguaSanActivity!!,
                    "Debe entrar en modo edición para ver opciones",
                    Toast.LENGTH_LONG).show()
            }else if(aguaSan?.session?.esAdmin == true){ //solo si es admin puede eliminar
                currentUbicacion = it
                val builder = AlertDialog.Builder(aguaSanActivity!!, R.style.AppTheme_Dialog)
                builder.setTitle("Opciones")

                // add a list
                val options = arrayOf("Eliminar")
                builder.setItems(options) { dialog, which ->
                    when (which) {
                        0 -> {
                            //TODO permitir eliminar de la bd remota solo si el usuario es ADMIN

                            if (it.codUbicacion == null) {

                                viewModel.deleteUbicacionFromDatabase(it).subscribeBy(onComplete={
                                    cliente?.let { cliente ->
                                        viewModel.getUbicacionesFromDatabase(cliente).subscribe(object: AguaSanObserver<List<Ubicacion>>(aguaSanActivity!!) {
                                            override fun onNext(t: List<Ubicacion>) {
                                                ubicaciones.clear()
                                                ubicaciones.addAll(t.filter{ ubi -> ubi.codUbicacionLocal != it.codUbicacionLocal})
                                                ubicacionesAdapter.notifyDataSetChanged()
                                            }

                                        })

                                    }
                                })

                            }else{

                                viewModel.deleteUbicacion(it.codUbicacion!!).subscribe(object: AguaSanObserver<String>(aguaSanActivity!!) {
                                    override fun onNext(t: String) {
                                        viewModel.deleteUbicacionFromDatabase(it).subscribeBy(onComplete={
                                            cliente?.let { cliente ->
                                                viewModel.getUbicacionesFromDatabase(cliente).subscribe(object: AguaSanObserver<List<Ubicacion>>(aguaSanActivity!!) {
                                                    override fun onNext(t: List<Ubicacion>) {
                                                        ubicaciones.clear()
                                                        ubicaciones.addAll(t.filter{ ubi -> ubi.codUbicacion != it.codUbicacion})
                                                        ubicacionesAdapter.notifyDataSetChanged()
                                                    }

                                                })

                                            }
                                        })
                                    }
                                })
                            }
                        }
                    }
                }

                builder.setNegativeButton("Cancelar", null)

                // create and show the alert dialog
                val dialog = builder.create()
                dialog.show()
            }
        }
        btAddUbicaciones.setOnClickListener {
            val i = Intent(aguaSanActivity!!, CapturaUbicacionActivity::class.java)
            i.putExtra("cliente", (aguaSanActivity!! as ClienteActivity).cliente)
            startActivityForResult(i, REQUEST_CODE)

        }

        if(cliente == null) {
            ubicaciones.clear()
            ubicacionesAdapter.notifyDataSetChanged()

        }else {
            viewModel.getUbicacionesFromDatabase(cliente!!).subscribe(object: AguaSanObserver<List<Ubicacion>>(aguaSanActivity!!){
                override fun onNext(t: List<Ubicacion>) {
                    ubicaciones.clear()
                    ubicaciones.addAll(t)
                    ubicacionesAdapter.notifyDataSetChanged()
                }

            })



        }

        if(permiteIncluir) {
            btAddUbicaciones.visibility = View.VISIBLE
        }else{
            btAddUbicaciones.visibility = View.GONE
        }


    }

    private fun launchCamera() {
        if(CameraHelper.hasPermissions(aguaSanActivity!!)) {
            takePhoto()
        }else {
            requestPermissions(CameraHelper.getPermissionsNeeded(aguaSanActivity!!), CameraHelper.REQUEST_PERMISSIONS)
        }
    }

    private fun takePhoto() {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        val fileUri = aguaSanActivity!!.contentResolver
            .insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(aguaSanActivity!!.packageManager) != null) {
            photoPath = fileUri.toString()
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
            intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION
                        or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            )
            startActivityForResult(intent, TAKE_PHOTO_REQUEST)
        }
    }



    override fun onSelected() {

    }

    override fun verifyStep(): VerificationError? {
        val cliente = (activity as ClienteActivity).cliente
        cliente.ubicaciones = ubicaciones
        return null
    }

    override fun onError(error: VerificationError) {

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CameraHelper.REQUEST_PERMISSIONS) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                launchCamera()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            data?.let {
                val ubicacion = it.getSerializableExtra("ubicacion") as Ubicacion
                ubicaciones.add(ubicacion)
                ubicacionesAdapter.notifyDataSetChanged()
            }
        }else if(requestCode == TAKE_PHOTO_REQUEST && resultCode == RESULT_OK) {
            proccessPhoto()
        }
    }

    private fun proccessPhoto(){
        val cursor = aguaSanActivity!!.contentResolver.query(Uri.parse(photoPath),
            Array(1) {android.provider.MediaStore.Images.ImageColumns.DATA},
            null, null, null)
        cursor?.moveToFirst()
        val photoPath = cursor?.getString(0)
        cursor?.close()


        val file = File(photoPath)
        val uri = Uri.fromFile(file)

        currentUbicacion?.pathFoto = photoPath

        for(ubi in ubicaciones) {
            val hexStringFoto = (file.toByteArray())?.toHexString()
            if(ubi.codUbicacionLocal == currentUbicacion?.codUbicacionLocal) {
                ubi.pathFoto = currentUbicacion?.pathFoto
                ubi.foto =  hexStringFoto  //encode(currentUbicacion?.pathFoto!!)
                ubicacionesAdapter.notifyDataSetChanged()
                break
            }
        }

    }


    private fun encode(filePath: String): String{
        val bytes = File(filePath).readBytes()
        return Base64.encodeToString(bytes, Base64.DEFAULT)
    }

    companion object {
        const val TAKE_PHOTO_REQUEST = 1
        const val REQUEST_CODE = 100
        const val PERMITE_INCLUIR = "permite_incluir"
        const val ADD_FOTO = "agregar_foto"
        var cliente: Cliente? = null
        var permiteIncluir: Boolean = false
        fun newInstance(b: Bundle?): ListaUbicacionesFragment {
            val fragment = ListaUbicacionesFragment()
            fragment.arguments = b
            cliente = null
            permiteIncluir = false
            b?.let {
                it.getSerializable("cliente")?.let{ cli ->
                    this.cliente = cli as Cliente?
                }
                this.permiteIncluir = b.getBoolean(PERMITE_INCLUIR, false)
            }
            return fragment
        }


    }

}
