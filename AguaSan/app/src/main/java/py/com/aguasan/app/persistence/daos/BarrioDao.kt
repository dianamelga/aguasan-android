package py.com.aguasan.app.persistence.daos

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import py.com.aguasan.app.persistence.entities.BARRIO_TABLE_NAME
import py.com.aguasan.app.persistence.entities.Barrio

/**
 * Created by Diana Melgarejo on 7/25/20.
 */
@Dao
interface BarrioDao {

    @Query("SELECT * FROM $BARRIO_TABLE_NAME")
    fun getAll(): Observable<List<Barrio>>

    @Query("SELECT a.* FROM clientes c JOIN barrio a ON c.cod_barrio = a.cod_barrio AND " +
            " c.cod_ciudad = a.cod_ciudad JOIN ciudad b ON c.cod_ciudad = b.cod_ciudad " +
            " WHERE c.cod_cliente = :codCliente")
    fun getBarrioFromCliente(codCliente: Int): Observable<Barrio>

    @Query("SELECT * FROM $BARRIO_TABLE_NAME WHERE cod_ciudad = :codCiudad")
    fun getBarrioFromCiudad(codCiudad: String): Observable<List<Barrio>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(barrios: List<Barrio>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(barrio: Barrio): Completable

    @Update
    fun updateBarrio(barrio: Barrio): Completable

    @Query("DELETE FROM $BARRIO_TABLE_NAME")
    fun deleteAll(): Completable

    @Query("DELETE FROM $BARRIO_TABLE_NAME WHERE cod_barrio = :codBarrio AND cod_ciudad = :codCiudad")
    fun deleteBarrio(codBarrio: String, codCiudad: String): Completable
}