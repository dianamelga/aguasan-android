package py.com.aguasan.app.viewHolders

import android.content.Context
import android.util.Log
import android.widget.LinearLayout
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.expand.Collapse
import com.mindorks.placeholderview.annotations.expand.Expand
import py.com.aguasan.app.R
import py.com.aguasan.app.models.Cliente

@Layout(R.layout.item_drop_list_cliente_view)
class ClienteChildList (val context: Context , val cliente: Cliente) {
    var funVer: (Cliente) -> Unit = {}
    var funEditar: (Cliente) -> Unit = {}
    var funRegVisita: () -> Unit = {}
    var funLlamar: (numero: String) -> Unit = {}

    var btVer: LinearLayout?=null
    var btEditar: LinearLayout?=null
    var btVisita: LinearLayout?=null
    var btLlamar: LinearLayout?=null

    @Resolve
    private fun onResolve() {
        Log.d(TAG, "onResolve")
    }




    companion object {
        const val TAG = "ClienteChildList"
    }
}