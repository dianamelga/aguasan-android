package py.com.aguasan.app.views.activities

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.appcompat.app.AppCompatActivity
import android.transition.Fade
import android.transition.Slide
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.R
import py.com.aguasan.app.utils.NotificationUtils
import java.util.*

open class AguaSanActivity : AppCompatActivity() {
    var aguasan: AguaSan? = null
    var finish = false
    var disposeBag: CompositeDisposable = CompositeDisposable()
    val backPressedEvent: BehaviorSubject<Long> = BehaviorSubject.createDefault(-1)
    var pushDialog: AlertDialog? = null
    var showNotification = true
    private var activityIsShowing = true
    private val progressReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                SHOW_PROGRESS_ACTION -> showProgress()
                DISMISS_PROGRESS_ACTION -> dismissProgress()
                NotificationUtils.PUSH_NOTIFICATION -> if (showNotification) showNotificationDialog(intent.getBundleExtra(NotificationUtils.PUSH_NOTIFICATION))
            }
        }

    }

    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        aguasan = application as AguaSan
        progressDialog = ProgressDialog(this)
        progressDialog?.isIndeterminate = true
        progressDialog?.setCancelable(false)
        progressDialog?.setMessage("Aguarde por favor...")

        val intentFilter = IntentFilter()
        intentFilter.addAction(SHOW_PROGRESS_ACTION)
        intentFilter.addAction(DISMISS_PROGRESS_ACTION)
        intentFilter.addAction(NotificationUtils.PUSH_NOTIFICATION)
        LocalBroadcastManager.getInstance(this).registerReceiver(progressReceiver, intentFilter)
        setupWindowAnimations()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeBag.dispose()
        disposeBag.clear()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(progressReceiver)
    }

    fun addForDisposing(d: Disposable?) {
        d?.let {
            disposeBag.add(it)
        }
    }

    fun showProgress() {
        progressDialog?.let {
            if (!it.isShowing && !isFinishing) {
                it.show()
            }
        }
    }

    fun dismissProgress() {
        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    override fun onBackPressed() {
        backPressedEvent.onNext(Calendar.getInstance().timeInMillis)
        if (finish) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    fun showKeyboard(v: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(v, 0)
    }

    fun hideKeyboard(v: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }

    companion object {
        const val SHOW_PROGRESS_ACTION = "py.com.aguasan.SHOW_PROGRESS"
        const val DISMISS_PROGRESS_ACTION = "py.com.aguasan.DISSMIS_PROGRESS"
    }


    private fun setupWindowAnimations() {
        if (Build.VERSION.SDK_INT >= 21) {
            val fade = Fade()
            fade.duration = 350
            window.enterTransition = fade

            val slide = Slide()
            slide.duration = 350
            window.exitTransition = slide
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                android.R.id.home -> {
                    super.onBackPressed()
                }
                else -> {
                }
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun showNotificationDialog(bundle: Bundle){
        if (pushDialog?.isShowing == true){
            pushDialog?.dismiss()
        }
        if (activityIsShowing){
            val title = bundle.getString(NotificationUtils.TITLE)
            val message = bundle.getString(NotificationUtils.MESSAGE)
            //val token_2fa = bundle.getString(NotificationUtils.TOKEN_2FA) ?: ""
            pushDialog = AlertDialog.Builder(this, R.style.AppTheme_Dialog)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Aceptar"){ dialog, _ ->
                 //   showAprobationProcess(token_2fa)
                    dialog.dismiss()
                }.show()
        }
    }
}