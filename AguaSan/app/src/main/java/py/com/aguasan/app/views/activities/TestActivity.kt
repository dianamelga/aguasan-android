package py.com.aguasan.app.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_test.*
import py.com.aguasan.app.AguaSan
import py.com.aguasan.app.R

class TestActivity : AguaSanActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Configuración"

        etUrl.setText(AguaSan.url)

        swConexionLocal.isChecked = AguaSan.conexionLocal
        etUrl.isEnabled = !AguaSan.conexionLocal

        swConexionLocal.setOnCheckedChangeListener { buttonView, isChecked ->

            etUrl.isEnabled = !isChecked
            AguaSan.conexionLocal = isChecked


        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                android.R.id.home -> {
                    AguaSan.url = etUrl.text.toString()
                    super.onBackPressed()
                }
                else -> {
                }
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }



}
