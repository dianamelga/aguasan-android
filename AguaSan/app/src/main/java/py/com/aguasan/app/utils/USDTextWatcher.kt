package py.com.aguasan.app.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.NumberFormat
import java.util.*

class USDTextWatcher(private val et: EditText) : TextWatcher {
    internal var mEditing: Boolean = false

    init {
        mEditing = false
    }

    override fun afterTextChanged(s: Editable) {
        if (!mEditing) {
            mEditing = true

            val digits = s.toString().replace("\\D".toRegex(), "")
            val nf = NumberFormat.getCurrencyInstance(Locale("en", "US"))
            try {
                val formatted = nf.format(java.lang.Double.parseDouble(digits) / 100)
                s.replace(0, s.length, formatted)
            } catch (nfe: NumberFormatException) {
                s.clear()
            }

            mEditing = false
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    companion object {

        private val TAG = "USDTextWatcher"
    }

}